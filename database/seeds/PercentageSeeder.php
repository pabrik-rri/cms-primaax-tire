<?php

use Illuminate\Database\Seeder;

class PercentageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $percentage             = new \App\Model\Setting\Percentage;
        $percentage->percentage = 2;
        $percentage->save();
    }
}
