<?php

use Illuminate\Database\Seeder;

class ModuleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $module = ["Level","User","Mitra","Courier","Customer","Area","Product","Vendor","Voucher","Unit","Transaction","Setting"];

        for($i=0; $i < sizeof($module); $i++)
        {
            DB::table('sys_modules')->insert(['module' => $module[$i]]);
        }
    }
}
