<?php

use Illuminate\Database\Seeder;

class ClosedSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $closed         = new \App\Model\Setting\Closed;
        $closed->time   = 60; //satuan menit
        $closed->save();
    }
}
