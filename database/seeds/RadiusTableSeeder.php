<?php

use Illuminate\Database\Seeder;

class RadiusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $content = array(
            array(
                'radius' => 1.24,
                'is_radius_reject' => 0
            ),
            array(
                'radius' => 3.105,
                'is_radius_reject' => 1
            )
        );

        foreach ($content as $key => $value) {
            $radius = new \App\Model\Setting\Radius;
            $radius->radius = $value['radius'];
            $radius->is_radius_reject = $value['is_radius_reject'];
            $radius->save();
        }
    }
}
