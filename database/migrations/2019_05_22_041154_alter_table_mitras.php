<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableMitras extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mitras', function (Blueprint $table) {
            $table->string('bank_account',30)->nullable()->after('limit');
            $table->string('bank_name',30)->nullable()->after('limit');
            $table->string('bank_norek',20)->nullable()->after('limit');
            $table->text('bank_logo')->nullable()->after('limit');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mitras', function (Blueprint $table) {
            $table->dropColumn('bank_account');
            $table->dropColumn('bank_name');
            $table->dropColumn('bank_norek');
            $table->dropColumn('bank_logo');
        });
    }
}
