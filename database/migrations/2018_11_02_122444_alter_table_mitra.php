<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableMitra extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mitras', function (Blueprint $table) {
            $table->string('vendor_id',50)->nullable()->after('mitra_id');
        });

        Schema::table('customers', function (Blueprint $table) {
            $table->string('vendor_id',50)->nullable()->after('avatar');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mitras', function (Blueprint $table) {
            $table->dropColumn('vendor_id');
        });

        Schema::table('customers', function (Blueprint $table) {
            $table->dropColumn('vendor_id');
        });
    }
}
