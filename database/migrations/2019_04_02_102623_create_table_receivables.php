<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableReceivables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('receivables', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code', 50);
            $table->datetime('invoice_date');
            $table->string('supplier_name', 100);
            $table->string('customer_name', 100);
            $table->integer('value_of_receivable')->default(0);
            $table->integer('balance_of_receivable')->default(0);
            $table->integer('is_factory')->comment('0 : pabrik , 1 : agen, 2 : toko');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('receivables');
    }
}
