<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableVendors extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('vendors');
        
        Schema::create('vendors', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 50);
            $table->text('address');
            $table->string('phone', 15);
            $table->string('fax', 15)->nullable();
            $table->integer('status')->nullable()->default(1)->comment('1 : Active, 0 : Non Active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendors');
    }
}
