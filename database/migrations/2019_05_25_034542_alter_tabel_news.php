<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTabelNews extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cms_news', function (Blueprint $table) {
            $table->integer('user_created')->unsigned()->nullable()->after('status');
            $table->integer('user_updated')->unsigned()->nullable()->after('user_created');

            $table->foreign('user_created')->references('id')->on('users');
            $table->foreign('user_updated')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cms_news', function (Blueprint $table) {
            $table->dropColumn('user_created');
            $table->dropColumn('user_updated');
        });
    }
}
