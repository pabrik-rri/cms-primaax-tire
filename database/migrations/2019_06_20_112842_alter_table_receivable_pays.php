<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableReceivablePays extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('receivable_pays', function (Blueprint $table) {
            $table->dropForeign('receivable_pays_receivable_id_foreign');

            $table->dropColumn('receivable_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('receivable_pays', function (Blueprint $table) {
            $table->integer('receivable_id')->nullable()->unsigned();
            $table->foreign('receivable_id')->references('id')->on('receivables');
        });
    }
}
