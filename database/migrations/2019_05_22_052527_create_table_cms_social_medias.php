<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCmsSocialMedias extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_social_medias', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 50);
            $table->text('url');
            $table->text('image');
            $table->integer('status')->default(1)->comment('0: Tidak Aktif, 1 : Aktif');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cms_social_medias');
    }
}
