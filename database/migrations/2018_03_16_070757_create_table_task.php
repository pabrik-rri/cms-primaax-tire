<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTask extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('sys_tasks');

        Schema::create('sys_tasks', function (Blueprint $table) {
            $table->increments('tasks_id');
            $table->integer('modules_id')->unsigned();
            $table->string('tasks');
            $table->timestamps();

            $table->foreign('modules_id')->references('modules_id')->on('sys_modules');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sys_tasks');
    }
}
