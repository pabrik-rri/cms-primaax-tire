<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableReceivablePays extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('receivable_pays', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code', 50);
            $table->string('no_receipt', 50);
            $table->string('customer_name', 100);
            $table->integer('receivable_id')->unsigned();
            $table->timestamps();

            $table->foreign('receivable_id')->references('id')->on('receivables');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('receivable_pays');
    }
}
