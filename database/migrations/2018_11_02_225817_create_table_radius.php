<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableRadius extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('radius');

        Schema::create('radius', function (Blueprint $table) {
            $table->increments('id');
            $table->double('radius');
            $table->integer('is_radius_reject')->nullable()->default(0)->comment('1 : Radius Reject, 0 : Radius Normal');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('radius');
    }
}
