<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDebtDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('debt_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('receivable_id')->unsigned();
            $table->integer('debt_id')->unsigned();
            $table->timestamps();

            $table->foreign('receivable_id')->references('id')->on('receivables');
            $table->foreign('debt_id')->references('id')->on('debts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('debt_details');
    }
}
