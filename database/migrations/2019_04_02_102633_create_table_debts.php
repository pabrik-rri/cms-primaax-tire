<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDebts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('debts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code', 50);
            $table->datetime('debt_date');
            $table->integer('supplier_id');
            $table->string('customer_name', 100);
            $table->integer('value_of_debt')->default(0);
            $table->integer('status')->comment('0 : Pembayaran , 1 : Penerimaan');
            $table->integer('payment_method')->comment('0 : Transfer , 1 : COD, 2 : Cek/Giro');
            $table->integer('is_factory')->comment('0 : pabrik , 1 : agen, 2 : toko');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('debts');
    }
}
