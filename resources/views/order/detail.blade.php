@extends('layouts.root')

@section('title','Detail Order')

@section('content')

@php
    switch ($order->status) {
        case '0':
            $status = "<span class='btn btn-warning btn-sm'>Pending</span>";
            break;

        case '1':
            $status = "<span class='btn btn-primary btn-sm'>Diterima</span>";
            break;

        case '2':
            $status = "Persiapan Pengiriman";
            break;

        case '3':
            $status = "Dalam Pengiriman";
            break;

        case '4':
            $status = "<span class='btn btn-danger btn-sm'>Batal</span>";
            break;

        case '5':
            $status = "<span class='btn btn-success btn-sm'>Selesai</span>";
            break;

        case '6':
            $status = "<span class='btn btn-info btn-sm'>Pindahan</span>";
            break;

        default:
            $status = "";
            break;
    }
@endphp

<!-- Page Content -->
<div class="content">

    <nav class="breadcrumb bg-white push">
        <a class="breadcrumb-item" href="javascript:void(0)">Dashboard</a>
        <a class="breadcrumb-item" href="{{ url('order') }}">Order</a>
        <span class="breadcrumb-item active">Detail</span>
    </nav>
    <!-- Material Design -->
    <div class="row">
        <div class="col-md-12">
            <!-- Static Labels -->
            <div class="block">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Informasi Order</h3>
                </div>
                <div class="block-content">
                    <div class="row">
                        <div class="col-sm-6">
                            <label>Kode Order</label>
                            <p>{{ $order->order_code }}</p>
                        </div>
                        <div class="col-sm-6">
                            <label>Tanggal Order</label>
                            <p>{{ date_format(date_create($order->created_at),'d F Y') }}</p>
                        </div>
                        <div class="col-sm-6">
                            <label>Status</label>
                            <p>{!! $status !!}</p>
                        </div>
                        <div class="col-sm-6">
                            <label>Alamat</label>
                            <p>{{ $order->address }}</p>
                        </div>
                        <div class="col-sm-6">
                            <label>Latitude</label>
                            <p>{{ $order->latitude }}</p>
                        </div>
                        <div class="col-sm-6">
                            <label>Longitude</label>
                            <p>{{ $order->longitude }}</p>
                        </div>
                        <div class="col-sm-6">
                            <label>Mitra</label>
                            <p>{{ isset($order->Mitra) ? $order->Mitra->name : "" }}</p>
                        </div>
                        <div class="col-sm-6">
                            <label>Courier</label>
                            <p>{{ isset($order->Courier) ? $order->Courier->name : "" }}</p>
                        </div>
                        <div class="col-sm-6">
                            <label>Customer</label>
                            <p>{{ isset($order->Customer) ? $order->Customer->name : "" }}</p>
                        </div>
                        <div class="col-sm-6">
                            <label>Kode Referral</label>
                            <p>{{ isset($order->Customer) ? $order->Customer->referral_code : "" }}</p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END Static Labels -->

            <div class="block">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Detail Order</h3>
                </div>
                <div class="block-content">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered">
                            <thead>
                                <th width="5%">No</th>
                                <th>Produk</th>
                                <th width="5%">Qty</th>
                                <th class="text-right">Total</th>
                            </thead>
                            <tbody>
                                @foreach($detail as $key => $value)
                                    <tr>
                                        <td>{{ $key+1 }}</td>
                                        <td>{{ $value->Product->name }}</td>
                                        <td>{{ $value->qty }} </td>
                                        <td class="text-right">Rp. {{ number_format($value->qty * $value->price) }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="4" class="text-right">Rp. {{ number_format($total) }}</td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- END column -->
    </div>
    <!-- END row -->
</div>
<!-- END content -->

@endsection
