@extends('layouts.root')

@section('title','Dashboard')

@push('style')
    <link rel="stylesheet" href="{{ asset('assets/js/plugins/getorgchart/getorgchart.css') }}">
@endpush

@section('content')
    <!-- Page Content -->
    <div class="content">
        <nav class="breadcrumb bg-white push">
            <a class="breadcrumb-item" href="javascript:void(0)">Dashboard</a>
            <span class="breadcrumb-item active">Admin</span>
        </nav>
        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title"><small>Peta Jabaran</small></h3>
            </div>
            <div class="block-content">
                <form action="">
                    <div class="form-group">
                        <div class="form-material">
                            <select class="form-control" id="organization_id" name="organization_id">
                                <option value="">Pilih Unit Kerja ...</option>
                                @foreach($organization as $opt)
                                    <option value="{{ $opt->organization_id }}">{{ $opt->organization }}</option>
                                @endforeach
                            </select>
                            <label for="material-select">Unit Kerja</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <button type="button" id="btn-search" class="btn btn-success">Cari</button>
                    </div>
                </form>
                <div id="position"></div>
            </div>
        </div>
    </div>
    <!-- END Page Content -->
@endsection

@push('script')
    <script src="{{ asset('assets/js/plugins/getorgchart/getorgchart.js') }}"></script>
    
    <script>
        $('#btn-search').bind('click', function()
        {
            var url = '{{ url("position-map/getposition")  }}';
            var id  = $('#organization_id').val();

            $.get(url, { organization_id : id }, function(data){
                console.log(data);
            }).done(function(data){
                var orgchart = new getOrgChart(document.getElementById("position"),{			
                    color: "blue",
                    enableEdit: false,
                    enableMove: false,
                    mouseNavEnabled:false,
                    enableDetailsView: false,
                    enableZoomOnNodeDoubleClick: false,
                    dataSource: data
                });
            });
        })
    </script>
@endpush