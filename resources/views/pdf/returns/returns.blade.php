<!DOCTYPE html>
<html>
	<head>
	    <meta charset="utf-8">
			<title>Eksport Retur Barang</title>
		</head>
	<body>
		<center><h3>RETUR PEMBELIAN</h3></center><br/>
		<table width="100%">
			<tr>
				<td>
					Tanggal : {{ date('d F Y', strtotime($return->return_date)) }}
				</td>
				<td align="right">
					Nama Customer : {{ \Auth::User()->name }}
				</td>
			</tr>
			<tr>
				<td>
					No Faktur : {{ $return->code }}
				</td>
			</tr>
		</table>
		<br/>
		<table width="100%" border="1" cellpadding="5" cellspacing="0">
			<tr>
				<td>No</td>
				<td>Nama Barang</td>
				<td>Qty</td>
				<td>Keterangan</td>
			</tr>
			@if(count($return->returnDetails) > 0)
                @php $index = 1 @endphp
                @foreach($return->returnDetails as $key => $result)
                    <tr>
                        <td>{{ $index }}</td>
                        <td>
                            {{ $result->product->name }}
                        </td>
                        <td>
                            {{ $result->qty }}
                        </td>
                        <td>
                            {{ $result->information }}
                        </td>
                    </tr>
                    @php $index++ @endphp
                @endforeach
                <tr>
                	<td colspan="2">Total</td>
                	<td colspan="2">{{ $return->qty }}</td>
                </tr>
            @endif
		</table>
	</body>
</html>