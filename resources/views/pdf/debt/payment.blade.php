<!DOCTYPE html>
<html>
	<head>
	    <meta charset="utf-8">
			<title>Eksport Pembayaran Hutang</title>
		</head>
	<body>
		<center><h3>BUKTI PEMBAYARAN HUTANG</h3></center><br/>
		<table width="100%">
			<tr>
				<td>
					Tanggal : {{ date('d F Y', strtotime($debt->debt_date)) }}
				</td>
				<td align="right">
					Nama Supplier : {{ $upline->name }}
				</td>
			</tr>
			<tr>
				<td>
					No Faktur : {{ $debt->code }}
				</td>
				<td align="right">
					Nilai Pembayaran : {{ number_format($debt->value_of_debt, 0, ',', '.') }}
				</td>
			</tr>
			<tr>
				<td>
					Nama Customer : {{ \Auth::User()->name }}
				</td>
				<td align="right">
					Cara Pembayaran :
					@if($debt->payment_method == 0)
						Transfer
					@elseif($debt->payment_method == 1)
						COD
					@else
						Cek/Giro
					@endif
				</td>
			</tr>
		</table>
		<br/>
		<table width="100%" border="1" cellpadding="5" cellspacing="0">
			<tr>
				<td>No</td>
				<td>Tanggal</td>
				<td>No Pembelian</td>
				<td>Nilai Hutang</td>
				<td>Saldo Hutang</td>
			</tr>
			@if(count($debt->debtDetails) > 0)
                @php $index = 1 @endphp
                @php $total = 0 @endphp
                @foreach($debt->debtDetails as $key => $result)
                	@php $total += $result->receivables->value_of_receivable @endphp
                    <tr>
                        <td>{{ $index }}</td>
                        <td>
                            {{ date('d-M-Y', strtotime($result->receivables->invoice_date)) }}
                        </td>
                        <td>
                            {{ $result->receivables->code }}
                        </td>
                        <td>
                            {{ number_format($result->receivables->value_of_receivable, 0, ',', '.') }}
                        </td>
                        <td>
                            {{ number_format($result->receivables->balance_of_receivable, 0, ',', '.') }}
                        </td>
                    </tr>
                    @php $index++ @endphp
                @endforeach
                <tr>
                	<td colspan="3">Total</td>
                	<td colspan="2">{{ number_format($total, 0, ',', '.') }}</td>
                </tr>
            @endif
		</table>
	</body>
</html>