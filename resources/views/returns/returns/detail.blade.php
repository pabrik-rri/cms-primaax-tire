@extends('layouts.root')

@section('title','Detail Retur Barang')

@section('content')
<div class="container">
    <div class="content">
        <nav class="breadcrumb bg-white push">
            <a class="breadcrumb-item" href="javascript:void(0)">Dashboard</a>
            <span class="breadcrumb-item active">Detail Retur Barang</span>
        </nav>

        @if ($errors->any())
            <div class="alert alert-danger m-t-20">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        @if(Session::has('status'))
            @if(Session::get('status') == '200')
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-check"></i> Berhasil!</h3>
                    <p class="mb-0">{{ Session::get('msg') }}</p>
                </div>
            @elseif(Session::get('status') == 'err')
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-check"></i> Gagal!</h3>
                    <p class="mb-0">{{ Session::get('msg') }}</p>
                </div>
            @endif
        @endif

        <!-- Filter -->
        <div class="block">
            <div class="block-content">
                <div class="col-12 mb-3" style="width: 100% !important;">
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group mb-3">
                                <label for="supplier">Tanggal Retur</label><br/>
                                {{ date('d F Y', strtotime($return->return_date)) }}
                            </div>
                            <div class="form-group mb-3">
                                <label for="supplier">No Faktur</label><br/>
                                {{ $return->code }}
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group mb-3">
                                <label for="supplier">Status</label><br/>
                                {{ $return->status == 0 ? 'Retur' : 'Kembali' }}
                            </div>
                            <div class="form-group mb-3">
                                <label for="supplier">Qty</label><br/>
                                {{ $return->qty }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Table -->
        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">Table</h3>
            </div>
            <div class="block-content pb-3">
                <div class="table-responsive">
                    <table class="table table-vcenter table-stripped">
                        <thead>
                            <tr>
                                <th style="width: 50px;">No</th>
                                <th>Nama Barang</th>
                                <th>Qty</th>
                                <th>Keterangan</th>
                            </tr>
                        </thead>
                        <tbody id="table-value">
                            @csrf
                            @if(count($return->returnDetails) > 0)
                                @php $index = 1 @endphp
                                @foreach($return->returnDetails as $key => $result)
                                    <tr>
                                        <td class="index">{{ $index }}</td>
                                        <td>{{ $result->product->name }}</td>
                                        <td class="qty">{{ $result->qty }}</td>
                                        <td>{{ $result->information }}</td>
                                    </tr>
                                    @php $index++ @endphp
                                @endforeach
                            @endif
                            <input type="hidden" name="date">
                        </tbody>
                    </table>
                </div>
                <a href="{{ url('returns/returns/export-pdf/' . $return->id) }}" class="btn btn-primary">Print</a>
                <a href="{{ url('returns/returns') }}" class="btn btn-danger">Exit</a>
            </div>
        </div>
        <!-- END Table -->
    </div>
</div>

@endsection

@push('script')
    <script>
        $('#btn-export').click(function(){
            $('input[name=date]').val($('#now').val());
            $('#form-products').attr('action', '{{ url("returns/returns/export-pdf/" . $return->id) }}')
            $('#table-value').append('<input type="hidden" name="_method" value="POST">');
            $('#form-products').submit();
        });
    </script>
@endpush