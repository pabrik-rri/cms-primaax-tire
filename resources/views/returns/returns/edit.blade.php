@extends('layouts.root')

@section('title','Retur Barang')

@section('content')
<div class="container">
    <div class="content">
        <nav class="breadcrumb bg-white push">
            <a class="breadcrumb-item" href="javascript:void(0)">Dashboard</a>
            <span class="breadcrumb-item active">Retur Barang</span>
        </nav>

        @if ($errors->any())
            <div class="alert alert-danger m-t-20">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        @if(Session::has('status'))
            @if(Session::get('status') == '200')
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-check"></i> Berhasil!</h3>
                    <p class="mb-0">{{ Session::get('msg') }}</p>
                </div>
            @elseif(Session::get('status') == 'err')
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-check"></i> Gagal!</h3>
                    <p class="mb-0">{{ Session::get('msg') }}</p>
                </div>
            @endif
        @endif

        <!-- Filter -->
        <div class="block">
            <div class="block-content">
                <div class="col-12 mb-3" style="width: 100% !important;">
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group mb-3">
                                <label for="supplier">Tanggal</label>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="text" autocomplete="off" class="form-control js-datepicker" value="{{ date('d-m-Y', strtotime($return->return_date)) }}" id="now" data-week-now="1" data-autoclose="true" data-today-highlight="true" data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy" name="now" placeholder="Pilih tanggal mulai">
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group mb-3">
                                <label for="customer">Nilai Qty</label>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="text" class="form-control js-datepicker" id="qty" value="{{ $return->qty }}"readonly>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Table -->
        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">Table</h3>
            </div>
            <div class="block-content pb-3">
                <form method="post" action="{{ url('returns/returns/update/'. $return->id) }}" id="form-products">
                    <input type="hidden" name="_method" value="PUT">
                    <div class="table-responsive">
                        <table class="table table-vcenter table-stripped">
                            <thead>
                                <tr>
                                    <th style="width: 50px;">No</th>
                                    <th>DO</th>
                                    <th>Nama Barang</th>
                                    <th>Qty</th>
                                    <th>Keterangan</th>
                                </tr>
                            </thead>
                            <tbody id="table-value">
                                @csrf
                                @if(count($return->returnDetails) > 0)
                                    @php $index = 1 @endphp
                                    @foreach($return->returnDetails as $key => $result)
                                        <tr id="tr-{{ $result->id }}">
                                            <td class="index">{{ $index }}</td>
                                            <td><input data-qty="{{ $result->qty }}" data-id="<?php echo $result->id; ?>" type="checkbox" class="form-control" name="do[]"></td>
                                            <td>
                                                {{ $result->product->name }}
                                                <input type=hidden name=name[] value="{{ $result->product_id }}">
                                            </td>
                                            <td class="qty">
                                                {{ $result->qty }}
                                                <input type=hidden name=qty[] value="{{ $result->qty }}">
                                            </td>
                                            <td>
                                                {{ $result->information }}
                                                <input type=hidden name=ket[] value="{{ $result->information }}">
                                            </td>
                                        </tr>
                                        @php $index++ @endphp
                                    @endforeach
                                @endif
                                <input type="hidden" name="date">
                            </tbody>
                        </table>
                    </div>
                </form>
                <button data-toggle="modal" data-target="#add-modal" class="btn btn-warning">Tambah</button>
                <button id="multiple-delete-button" class="btn btn-danger">Hapus</button>
                <button id="btn-save" class="btn btn-success">Save</button>
                <a href="{{ url('returns/returns') }}" class="btn btn-danger">Exit</a>
            </div>
        </div>
        <!-- END Table -->
    </div>
</div>

<div class="modal fade" id="add-modal" tabindex="-1" role="dialog" aria-labelledby="modal-fadein" style="display: none;" aria-hidden="true">
    <div class="modal-dialog hide" role="document" aria-hidden="true">
        <div class="modal-content">
            <div class="block block-themed block-transparent mb-0">
                <div class="block-header bg-primary-dark">
                    <h3 class="block-title">Tambah Barang</h3>
                    <div class="block-options">
                        <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                            <i class="si si-close"></i>
                        </button>
                    </div>
                </div>
                <div class="block-content">
                    <div class="form-inline mb-3">
                        <div class="row">
                            <div class="col-8 form-inline">
                                <label for="" class="control-label">Nama Barang</label>
                                &nbsp;&nbsp;
                                <select name="add-name" class="form-control">
                                    @foreach($products as $key => $result)
                                        <option value={{ $result->id }}>{{ $result->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-4 form-inline text-right">
                                <input type="number" name="add-qty" style="width:100%;" class="form-control" placeholder="qty" value="">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="control-label">Keterangan</label>
                        <textarea name="add-ket" style="width:100%" class="form-control"></textarea>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Batal</button>
                <button id="add-save" type="submit" class="btn btn-success">Simpan</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="delete-modal" tabindex="-1" role="dialog" aria-labelledby="modal-fadein" style="display: none;" aria-hidden="true">
    <div class="modal-dialog hide" role="document" aria-hidden="true">
        <div class="modal-content">
            <div class="block block-themed block-transparent mb-0">
                <div class="block-header bg-primary-dark">
                    <h3 class="block-title">Hapus</h3>
                    <div class="block-options">
                        <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                            <i class="si si-close"></i>
                        </button>
                    </div>
                </div>
                <div class="block-content">
                    @csrf
                    <div class="row">
                        <div class="col-sm-12 modal-text">
                            <p>Anda yakin akan menghapus data ini ?</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-danger btn-modal-delete">Hapus</button>
            </div>
        </div>
    </div>
</div>


@endsection

@push('script')
    <script src="{{ asset('assets/js/plugins/select2/select2.full.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
    <script>
        $('#qty').val('{{ $return->qty }}');
        clear();

        var index = +'{{ count($return->returnDetails) }}' + 1;
        var id = +'{{ count($return->returnDetails) }}' + 1;
        // Add
        $('#add-save').click(function(){
            var html;
            html = '<tr id="tr-' + id + '">';
            html += '<td class="index">' + index + '</td>';
            html += '<td><input data-qty="'+ $('input[name=add-qty]').val() +'" data-id="' + id + '" type="checkbox" class="form-control" name="do[]"></td>';
            html += '<td><input type=hidden name=name[] value="' + $('select[name=add-name] option:selected').val() + '">' + $('select[name=add-name] option:selected').text() + '</td>';
            html += '<td class="qty"><input type=hidden name=qty[] value="' + $('input[name=add-qty]').val() + '">' + $('input[name=add-qty]').val() + '</td>';
            html += '<td><input type=hidden name=ket[] value="' + $('textarea[name=add-ket]').val() + '">' + $('textarea[name=add-ket]').val() + '</td>';
            html += '</tr>';
            $('#table-value').append(html);


            // Sum Total Qty
            var qty = $('#qty').val();
            $('#qty').val(+qty + +$('input[name=add-qty]').val());
            index++; id++;
            $('#add-modal').modal('hide');
            clear();
        });

        $('#multiple-delete-button').click(function(){
            var checkboxes = [];
            var values = $("input[name='do[]']:checked").map(function(){
                $('#qty').val(+$('#qty').val() - +$(this).data('qty'));
                $('#tr-' + $(this).data('id')).remove();
            });

            index = 1;
            $('tbody#table-value > tr > td.index').each(function(){
                $(this).html(index);
                index++;
            });
        });

        $('#btn-save').click(function(){
            $('input[name=date]').val($('#now').val());
            $('#form-products').submit();
        });

        function clear()
        {
            $('select[name=add-name]').val('');
            $('input[name=add-qty]').val('');
            $('textarea[name=add-ket]').val('');
        }

        jQuery(function () {
            // Init page helpers (Select2 plugin)
            Codebase.helpers(['datepicker']);
        });
    </script>
@endpush