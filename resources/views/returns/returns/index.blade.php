@extends('layouts.root')

@section('title','Retur Barang')

@section('content')
<div class="container">
    <div class="content">
        <nav class="breadcrumb bg-white push">
            <a class="breadcrumb-item" href="javascript:void(0)">Dashboard</a>
            <span class="breadcrumb-item active">Retur Barang</span>
        </nav>

        @if ($errors->any())
            <div class="alert alert-danger m-t-20">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        @if(Session::has('status'))
            @if(Session::get('status') == '200')
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-check"></i> Berhasil!</h3>
                    <p class="mb-0">{{ Session::get('msg') }}</p>
                </div>
            @elseif(Session::get('status') == 'err')
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-check"></i> Gagal!</h3>
                    <p class="mb-0">{{ Session::get('msg') }}</p>
                </div>
            @endif
        @endif

        <h2 class="content-heading">
            <a class="btn btn-sm btn-warning float-right" href="{{ url('returns/returns/create') }}">
                <i class="fa fa-plus"></i> &nbsp; Tambah Retur Barang
            </a>
            Retur Barang
        </h2>

        <!-- Filter -->
        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">Pencarian</h3>
            </div>
            <div class="block-content">
                <div class="row">
                    <div class="form-group col-10">
                        <form action="{{ url('returns/returns') }}" class="form-inline" method="GET">
                        <div class="form-inlin mb-3e mr-3"><label>Periode</label></div>
                        <div class="form-inline mb-3 mr-3">
                            <input type="text" autocomplete="off" class="form-control js-datepicker" value="{{ Request::get('start') != '' ? Request::get('start') : date('d-m-Y') }}" id="start" data-week-start="1" data-autoclose="true" data-today-highlight="true" data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy" name="start" style="width: 100%" placeholder="Pilih tanggal mulai">
                        </div>
                        <div class="form-inline mb-3 mr-3"><label>s/d</label></div>
                        <div class="form-inline mb-3 mr-3">
                            <input type="text" autocomplete="off" class="form-control js-datepicker" value="{{ Request::get('finish') != '' ? Request::get('finish') : date('d-m-Y') }}" id="finish" data-week-start="1" data-autoclose="true" data-today-highlight="true" data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy" name="finish" style="width: 100%" placeholder="Pilih tanggal selesai">
                        </div>
                        <div class="form-inline mb-3 mr-3"><label>Status</label></div>
                        <div class="form-inline mb-3 mr-3">
                            <select class="js-select2 form-control" id="status" name="status">
                                <option {{ (Request::get("status") == null ? 'selected' : '') }} value=""></option>
                                <option {{ (Request::get("status") == "0" ? 'selected' : '') }} value=0>Retur</option>
                                <option {{ (Request::get("status") == "1" ? 'selected' : '') }} value=1>Kembali</option>
                            </select>
                        </div>
                        <div class="form-inline mb-3 mr-3">
                            <input type="text" value="{{ Request::get('return_id') }}" name="return_id" placeholder="Input No. Retur" class="form-control">
                        </div>
                        <div class="form-check-inline mb-3">
                            <button class="btn btn-warning" style="width: 100%"> <i class="fa fa-search"></i> Cari</button>
                        </div>
                        </form>
                    </div>
                    <div class="form-group col-2">
                        <form action="{{ url('returns/returns/export') }}" method="get" id="export">
                        <input type="hidden" name="return-id-export">
                        <input type="hidden" name="status-export">
                        <input type="hidden" name="start-export">
                        <input type="hidden" name="finish-export">
                        <button class="text-white btn btn-success" style="width: 100%"> <i class="fa fa-file-excel-o"></i> Eksport</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- Table -->
        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">Table</h3>
            </div>
            <div class="block-content">
                <div class="table-responsive">
                    <table class="table table-vcenter table-stripped">
                        <thead>
                            <tr>
                                <th style="width: 50px;">No</th>
                                <th>Tanggal</th>
                                <th>No Retur</th>
                                <th>Qty</th>
                                <th>Status</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($data) > 0)
                                @foreach($data as $key => $result)
                                    <tr>
                                        <td>{{ ($data->currentpage()-1) * $data->perpage() + $key + 1 }}</td>
                                        <td>{{ date('d-M-Y', strtotime($result->return_date)) }}</td>
                                        <td>{{ $result->code }}</td>
                                        <td>{{ $result->qty }}</td>
                                        <td>{{ $result->status == 0 ? 'Retur' : 'Kembali' }}</td>
                                        <td>
                                            <a href="{{ url('returns/returns/detail/'. $result->id) }}" class="btn btn-sm btn-success" data-toggle="tooltip" title="Lihat">
                                                <i class="fa fa-eye"></i>
                                            </a>
                                            <a href="{{ url('returns/returns/edit/'. $result->id) }}" class="btn btn-sm btn-primary edit-button" data-status="{{ $result->status }}" data-toggle="tooltip" title="Ubah">
                                                <i class="fa fa-pencil"></i>
                                            </a>
                                            <button  data-toggle="tooltip" title="Hapus" data-action="{{ url('returns/returns/delete/'. $result->id) }}" class="btn btn-sm btn-danger btn-xs btn-delete delete-button" data-status="{{ $result->status }}">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td align="center" colspan='5'>Data Tidak Tersedia</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                {{ $data->links() }}
            </div>
        </div>
        <!-- END Table -->
    </div>


</div>
@include('modal_delete')
@endsection

@push('script')
    <!-- DataTables -->
    <script src="{{ asset('assets/js/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
    <script>
        $('.edit-button').click(function(){
            var status = $(this).data('status');
            if (status == 1) {
                alert('Data tidak bisa diubah karena barang sudah diterima');
                return false;
            }
        });

        $('.delete-button').click(function(){
            var status = $(this).data('status');
            if (status == 1) {
                alert('Data tidak bisa dihapus karena barang sudah diterima');
                return false;
            }

            var action = $(this).data('action');

            $('#modalDelete').modal('show');
            $('.modal-title').html('Delete Retur Barang');

            $('#formDelete').attr('action',action);
            $('#formDelete').append("<input type='hidden' name='_method' value='DELETE'>");
        });

        $('#export').submit(function(){
            $('input[name=status-export').val($('select[name=status').val());
            $('input[name=return-id-export').val($('input[name=return_id').val());
            $('input[name=start-export').val($('input[name=start').val());
            $('input[name=finish-export').val($('input[name=finish').val());
        });
        
        jQuery(function () {
            // Init page helpers (Select2 plugin)
            Codebase.helpers(['datepicker']);
        });
    </script>
@endpush