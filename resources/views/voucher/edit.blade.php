@extends('layouts.root')

@section('title','Ubah Voucher')

@section('content')

<!-- Page Content -->
<div class="content">

    <nav class="breadcrumb bg-white push">
        <a class="breadcrumb-item" href="javascript:void(0)">Dashboard</a>
        <a class="breadcrumb-item" href="{{ url('voucher') }}">Voucher</a>
        <span class="breadcrumb-item active">Ubah</span>
    </nav>
    <!-- Material Design -->
    <div class="row">
        <div class="col-md-12">
            <!-- Static Labels -->
            <div class="block">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Ubah Voucher</h3>
                </div>
                <div class="block-content">
                    <form class="form-level" action="{{ url('voucher/update/'. $voucher->id) }}" method="post">
                        <input type="hidden" name="_method" value="PUT">
                        @csrf
                        <div class="form-group">
                            <div class="form-material">
                                <input type="text" autocomplete="off" class="form-control" id="code" name="code" placeholder="Masukan Kode Voucher" value="{{ $voucher->code }}">
                                <label for="material-email">Kode <span class="text text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-material">
                                <input type="number" autocomplete="off" class="form-control" id="value" name="value" placeholder="Masukan Nilai Voucher" value="{{ $voucher->value }}">
                                <label for="material-email">Nilai <span class="text text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-material">
                                <input type="text" autocomplete="off" class="form-control js-datepicker" id="start" name="start" value="{{ date_format(date_create($voucher->start),'d-m-Y') }}" data-week-start="1" data-autoclose="true" data-today-highlight="true" data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy" placeholder="Berlaku Dari">
                                <label for="material-email">Berlaku dari <span class="text text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-material">
                                <input type="text" autocomplete="off" class="form-control js-datepicker" id="finish" name="finish" value="{{ date_format(date_create($voucher->finish),'d-m-Y') }}" data-week-start="1" data-autoclose="true" data-today-highlight="true" data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy" placeholder="Berlaku Sampai">
                                <label for="material-email">Berlaku sampai <span class="text text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-warning">Simpan</button>
                            <button type="reset" class="btn btn-secondary">Reset</button>
                        </div>
                    </form>
                </div>
            </div>
            <!-- END Static Labels -->
        </div>
        <!-- END column -->
    </div>
    <!-- END row -->
</div>
<!-- END content -->

@endsection

@push('script')

    <script src="{{ asset('assets/js/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/jquery-validation/additional-methods.min.js') }}"></script>
    <script>
        jQuery(function () {
            // Init page helpers (Datepicker plugin)
            Codebase.helpers(['datepicker']);
        });
        $('.form-level').validate({
            ignore: [],
            errorClass: 'invalid-feedback animated fadeInDown',
            errorElement: 'div',
            errorPlacement: function(error, e) {
                jQuery(e).parents('.form-group').append(error);
            },
            highlight: function(e) {
                jQuery(e).closest('.form-group').removeClass('is-invalid').addClass('is-invalid');
            },
            success: function(e) {
                jQuery(e).closest('.form-group').removeClass('is-invalid');
                jQuery(e).remove();
            },
            rules: {
                'value': {
                    required: true
                },
                'start': {
                    required: true
                },
                'finish': {
                    required: true
                },
                'code': {
                    required: true,
                    minlength: 3
                }
            },
            messages: {
                'value': {
                    required: 'Nilai Voucher harus diisi',
                },
                'start': {
                    required: 'Tanggal berlaku dari Voucher harus diisi',
                },
                'finish': {
                    required: 'Tanggal berlaku sampai Voucher harus diisi',
                },
                'code': {
                    required: 'Kode Voucher harus diisi',
                    minlength: 'Minimal karakter terdiri dari 3 huruf'
                }
            }
        });
    </script>
@endpush
