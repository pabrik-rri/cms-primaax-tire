@extends('layouts.root')

@section('title','Menu')

@section('content')
<div class="container">
    <div class="content">
        <nav class="breadcrumb bg-white push">
            <a class="breadcrumb-item" href="javascript:void(0)">Dashboard</a>
            <span class="breadcrumb-item active">Menu</span>
        </nav>

        @if ($errors->any())
    	    <div class="alert alert-danger m-t-20">
    	        <ul>
    	            @foreach ($errors->all() as $error)
    	                <li>{{ $error }}</li>
    	            @endforeach
    	        </ul>
    	    </div>
    	@endif

    	@if(Session::has('status'))
    		@if(Session::get('status') == '200')
    	        <div class="alert alert-success alert-dismissible" role="alert">
    	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    	            <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-check"></i> Success!</h3>
    	            <p class="mb-0">{{ Session::get('msg') }}</p>
    	        </div>
            @elseif(Session::get('status') == 'err')
    			<div class="alert alert-danger alert-dismissible" role="alert">
    	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    	            <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-check"></i> Failed!</h3>
    	            <p class="mb-0">{{ Session::get('msg') }}</p>
    	        </div>
            @endif
        @endif

        <h2 class="content-heading">
            <a class="btn btn-sm btn-warning float-right" href="{{ url('system/menu/create') }}">
                <i class="fa fa-plus"></i> &nbsp; Tambah Menu Baru
            </a>
            Menu
        </h2>

        <!-- Table -->
        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">Table</h3>
            </div>
            <div class="block-content">
                <table class="table table-vcenter table-stripped">
                    <thead>
                        <tr>
                            <th style="width: 50px;">No</th>
                            <th>Menu</th>
                            <th>Module</th>
                            <th>URL</th>
                            <th>Icon</th>
                            <th>Sub</th>
                            <th>Parent</th>
                            <th>Position</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(sizeof($menu) > 0)
                            @foreach ($menu as $key => $result)
                                <tr>
                                    <td>{{ ($menu->currentpage()-1) * $menu->perpage() + $key + 1  }}</td>
                                    <td>{{ $result->menu }}</td>
                                    <td>{{ isset($result->getModule) ? $result->getModule->module : "-" }}</td>
                                    <td>{{ $result->menu_url }}</td>
                                    <td>{{ isset($result->menu_icon) ? $result->menu_icon : "-" }}</td>
                                    <td>{{ $result->menu_is_sub == 0 ? "Bukan" : "Ya" }}</td>
                                    <td>{{ isset($result->menu_parent) ? $result->getMenu->menu : "-" }}</td>
                                    <td>{{ $result->menu_position }}</td>
                                    <td>
                                        <a href="{{ url('system/menu/edit/'. $result->menu_id) }}" class="btn btn-sm btn-primary">
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                        <button data-toggle="tooltip" title="Hapus" data-action="{{ url('system/menu/delete/'. $result->menu_id) }}" class="btn btn-sm btn-danger btn-delete">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr> <td colspan="9">Data Menu Tidak Tersedia. </td> </tr>
                        @endif
                    </tbody>
                </table>
            </div>
            {{ $menu->links() }}
        </div>
        <!-- END Table -->
    </div>


</div>

@include('modal_delete')
@endsection

@push('script')
    <script type="text/javascript">
        $('.btn-delete').click(function(){
        	var action = $(this).data('action');

        	$('#modalDelete').modal('show');
        	$('.modal-title').html('Delete Level');

            $('#formDelete').attr('action',action);
            $('#formDelete').append("<input type='hidden' name='_method' value='DELETE'>");
        });
    </script>
@endpush
