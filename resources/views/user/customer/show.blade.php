@extends('layouts.root')

@section('title','Detail Customer')

@section('content')

<!-- Page Content -->
<div class="content">

    <nav class="breadcrumb bg-white push">
        <a class="breadcrumb-item" href="javascript:void(0)">Dashboard</a>
        <a class="breadcrumb-item" href="{{ url('customer') }}">Customer</a>
        <span class="breadcrumb-item active">Detail</span>
    </nav>
    <!-- Material Design -->
    <div class="row">
        <div class="col-md-12">
            <!-- Static Labels -->
            <div class="block">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Detail Customer</h3>
                </div>
                <div class="block-content">
                    <div class="row">
                        <div class="col-sm-6">
                            <label>Nama</label>
                            <p>{{ $user->name }}</p>
                        </div>
                        <div class="col-sm-6">
                            <label>No KTP</label>
                            <p>{{ $user->no_id_card }}</p>
                        </div>
                        <div class="col-sm-6">
                            <label>Tanggal Lahir</label>
                            <p>{{ date_format(date_create($user->birth_date),'d F Y') }}</p>
                        </div>
                        <div class="col-sm-6">
                            <label>Email</label>
                            <p>{{ $user->email }}</p>
                        </div>
                        <div class="col-sm-6">
                            <label>No Telepon</label>
                            <p>{{ $user->mobile }}</p>
                        </div>
                        <div class="col-sm-6">
                            <label>Gender</label>
                            <p>{{ $user->gender == 1 ? "Laki-laki" : "Perempuan" }}</p>
                        </div>
                        <div class="col-sm-6">
                            <label>Propinsi</label>
                            <p>{{ isset($user->City) ? $user->City->Province->province : "" }}</p>
                        </div>
                        <div class="col-sm-6">
                            <label>Kota</label>
                            <p>{{ isset($user->City) ? $user->City->city : "" }}</p>
                        </div>
                        <div class="col-sm-12">
                            <label>Address</label>
                            <p>{{ $user->address }}</p>
                        </div>
                        <div class="col-sm-6">
                            <label>Latitude</label>
                            <p>{{ $user->latitude }}</p>
                        </div>
                        <div class="col-sm-6">
                            <label>Longitude</label>
                            <p>{{ $user->longitude }}</p>
                        </div>
                        <div class="col-sm-12">
                            <label>Kode Pos</label>
                            <p>{{ $user->zip_code }}</p>
                        </div>
                        <div class="col-sm-12">
                            <label>Referral Duta</label>
                            <p>{{ $user->referral_code }}</p>
                        </div>
                        <div class="col-sm-6">
                            <label>Status</label>
                            <p>{!! $user->status == 1 ? "<span class='btn-sm btn-success'>Aktif</span>" : "<span class='btn-sm btn-danger'>Tidak Aktif</span>" !!}</p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END Static Labels -->
        </div>
        <!-- END column -->
    </div>
    <!-- END row -->
</div>
<!-- END content -->

@endsection
