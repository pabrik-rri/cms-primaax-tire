@extends('layouts.root')

@section('title','Detail Mitra')

@section('content')

<!-- Page Content -->
<div class="content">

    <nav class="breadcrumb bg-white push">
        <a class="breadcrumb-item" href="javascript:void(0)">Dashboard</a>
        <a class="breadcrumb-item" href="{{ url('mitra') }}">Mitra</a>
        <span class="breadcrumb-item active">Detail</span>
    </nav>
    <!-- Material Design -->
    <div class="row">
        <div class="col-md-12">
            <!-- Static Labels -->
            <div class="block">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Detail Mitra</h3>
                </div>
                <div class="block-content">
                    <div class="row">
                        <div class="col-sm-6">
                            <label>Nama</label>
                            <p>{{ $user->name }}</p>
                        </div>
                        <div class="col-sm-6">
                            <label>No KTP</label>
                            <p>{{ $user->no_id_card }}</p>
                        </div>
                        <div class="col-sm-6">
                            <label>Tanggal Lahir</label>
                            <p>{{ date_format(date_create($user->birth_date),'d F Y') }}</p>
                        </div>
                        <div class="col-sm-6">
                            <label>Email</label>
                            <p>{{ $user->email }}</p>
                        </div>
                        <div class="col-sm-6">
                            <label>No Telepon</label>
                            <p>{{ $user->mobile }}</p>
                        </div>
                        <div class="col-sm-6">
                            <label>Gender</label>
                            <p>{{ $user->gender == 1 ? "Laki-laki" : "Perempuan" }}</p>
                        </div>
                        <div class="col-sm-6">
                            <label>Propinsi</label>
                            <p>{{ isset($user->City) ? $user->City->Province->province : "" }}</p>
                        </div>
                        <div class="col-sm-6">
                            <label>Kota</label>
                            <p>{{ isset($user->City) ? $user->City->city : "" }}</p>
                        </div>
                        <div class="col-sm-12">
                            <label>Address</label>
                            <p>{{ $user->address }}</p>
                        </div>
                        <div class="col-sm-6">
                            <label>Latitude</label>
                            <p>{{ $user->latitude }}</p>
                        </div>
                        <div class="col-sm-6">
                            <label>Longitude</label>
                            <p>{{ $user->longitude }}</p>
                        </div>
                        <div class="col-sm-12">
                            <label>Kode Pos</label>
                            <p>{{ $user->zip_code }}</p>
                        </div>
                        <div class="col-sm-6">
                            <label>Status</label>
                            <p>{!! $user->status == 1 ? "<span class='btn-sm btn-success'>Aktif</span>" : "<span class='btn-sm btn-danger'>Tidak Aktif</span>" !!}</p>
                        </div>
                        <div class="col-sm-6">
                            <label>Status Toko</label>
                            <p>{{ $user->status_store == 1 ? "Buka" : "Tutup" }}</p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END Static Labels -->
        </div>
        <!-- END column -->
    </div>
    <!-- END row -->
</div>
<!-- END content -->

@endsection

@push('script')

    <script src="{{ asset('assets/js/plugins/select2/select2.full.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/jquery-validation/additional-methods.min.js') }}"></script>
    <script>
        jQuery(function () {
            // Init page helpers (Select2 plugin)
            Codebase.helpers(['select2','datepicker']);

            $('#province').bind('change', function(){
                var url = '{{ url("getCity") }}';
                var province = $(this).val();

                $.get(url, {province : province}, function(data){
                    $('#city_id').html(data);
                });
            });
        });

        $('.form-level').validate({
            ignore: [],
            errorClass: 'invalid-feedback animated fadeInDown',
            errorElement: 'div',
            errorPlacement: function(error, e) {
                jQuery(e).parents('.form-group').append(error);
            },
            highlight: function(e) {
                jQuery(e).closest('.form-group').removeClass('is-invalid').addClass('is-invalid');
            },
            success: function(e) {
                jQuery(e).closest('.form-group').removeClass('is-invalid');
                jQuery(e).remove();
            },
            rules: {
                'name': {
                    required: true
                },
                'email': {
                    required: true,
                    email: true
                },
                'mobile': {
                    required: true,
                },
                'password': {
                    minlength: 5
                },
                'confirm-password': {
                    equalTo: '#password'
                },
                'province': {
                    required: true,
                },
                'city_id': {
                    required: true,
                },
            },
            messages: {
                'name': {
                    required: 'Inputan nama harus diisi',
                },
                'email': {
                    required: 'Inputan email harus diisi',
                },
                'mobile': 'Inputan telepon harus diisi',
                'password': {
                    minlength: 'Isian kata sandi minimal terdiri dari 5 karakter atau lebih'
                },
                'confirm-password': {
                    minlength: 'Isian kata sandi minimal terdiri dari 5 karakter atau lebih',
                    equalTo: 'Kata sandi tidak sama'
                },
                'province': 'Propinsi harus dipilih',
                'city_id': 'Kota harus dipilih'
            }
        });
    </script>
@endpush
