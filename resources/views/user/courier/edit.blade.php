@extends('layouts.root')

@section('title','Ubah Courier Baru')

@section('content')

<!-- Page Content -->
<div class="content">

    <nav class="breadcrumb bg-white push">
        <a class="breadcrumb-item" href="javascript:void(0)">Dashboard</a>
        <a class="breadcrumb-item" href="{{ url('courier') }}">Courier</a>
        <span class="breadcrumb-item active">Ubah</span>
    </nav>
    <!-- Material Design -->
    <div class="row">
        <div class="col-md-12">
            <!-- Static Labels -->
            <div class="block">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Ubah Courier Baru</h3>
                </div>
                <div class="block-content">
                    <form class="form-level" action="{{ url('courier/update/'.$user->id) }}" method="post" enctype=multipart/form-data>
                        @csrf
                        <input type="hidden" name="_method" value="PUT">

                        <div class="form-group">
                            <div class="form-material">
                                <select class="js-select2 form-control" id="mitra_id" name="mitra_id" data-placeholder="Pilih Mitra">
                                    <option></option>
                                    @foreach($mitras as $mitra)
                                        @if($mitra->id == $user->mitra_id)
                                            <option selected value="{{ $mitra->id }}">{{ $mitra->name }}</option>
                                        @else
                                            <option value="{{ $mitra->id }}">{{ $mitra->name }}</option>
                                        @endif
                                    @endforeach
                                </select>
                                <label for="province">Mitra <span class="text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-material">
                                <input type="text" autocomplete="off" class="form-control" id="name" name="name" value="{{ $user->name }}" placeholder="Masukan nama Anda">
                                <label for="material-name">Nama <span class="text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-material">
                                <select class="js-select2 form-control" id="gender" name="gender" data-placeholder="Pilih Jenis Kelamin">
                                    <option value="1" @if($user->gender == 1) "selected" @endif>Laki - laki</option>
                                    <option value="2" @if($user->gender == 2) "selected" @endif>Perempuan</option>
                                </select>
                                <label for="province">Jenis Kelamin <span class="text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-material">
                                <input type="text" autocomplete="off" class="form-control" id="email" name="email" value="{{ $user->email }}" placeholder="Masukan email Anda">
                                <label for="material-email">Email <span class="text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-material">
                                <input type="text" autocomplete="off" class="form-control" id="mobile" name="mobile" value="{{ $user->mobile }}" placeholder="Masukan Telpon Anda">
                                <label for="material-mobile">Telepon <span class="text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-material">
                                <input type="text" class="js-datepicker form-control" id="birth_date" name="birth_date" value="{{ date_format(date_create($user->birth_date),'d-m-Y') }}" data-week-start="1" data-autoclose="true" data-today-highlight="true" data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy">
                                <label for="birth_date">Tanggal Lahir</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-material">
                                <input type="password" autocomplete="off" class="form-control" id="password" name="password" placeholder="Masukan kata sandi Anda">
                                <label for="material-password">Kata Sandi <span class="text-danger">(Kosongkan jika ingin menggunkan kata sandi yang sama)</span></label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-material">
                                <input type="password" autocomplete="off" class="form-control" id="confirm-password" name="confirm-password" placeholder="Masukan konfirmasi kata sandi Anda">
                                <label for="material-conf-password">Konfirmasi Kata Sandi </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-material">
                                <input type="text" autocomplete="off" class="form-control" id="no_id_card" name="no_id_card" value="{{ $user->no_id_card }}" placeholder="Masukan No Ktp Anda">
                                <label for="material-mobile">No Ktp </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-material">
                                <input type="text" autocomplete="off" class="form-control" id="name_store" name="name_store" value="{{ $user->name_store }}" placeholder="Masukan Nama Toko Anda">
                                <label for="material-mobile">Nama Toko </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-material">
                                <select class="js-select2 form-control" id="province" name="province" data-placeholder="Pilih Propinsi">
                                    <option></option>
                                    @foreach($provinces as $province)
                                        @if(isset($user->City))
                                            @if($province->id == $user->City->province_id)
                                                <option selected value="{{ $province->id }}">{{ $province->province }}</option>
                                            @else
                                                <option value="{{ $province->id }}">{{ $province->province }}</option>
                                            @endif
                                        @else
                                            <option value="{{ $province->id }}">{{ $province->province }}</option>
                                        @endif
                                    @endforeach
                                </select>
                                <label for="province">Propinsi <span class="text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-material">
                                <select class="js-select2 form-control" id="city_id" name="city_id" data-placeholder="Pilih Kota">
                                    <option></option>
                                    @foreach($cities as $city)
                                        @if($city->id == $user->city_id)
                                            <option selected value="{{ $city->id }}">{{ $city->city }}</option>
                                        @else
                                            <option value="{{ $city->id }}">{{ $city->city }}</option>
                                        @endif
                                    @endforeach
                                </select>
                                <label for="city_id">Kota <span class="text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-material">
                                <textarea autocomplete="off" class="form-control" id="address" name="address" placeholder="Masukan Alamat Anda">{{ $user->address }}</textarea>
                                <label for="material-mobile">Alamat <span class="text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-material">
                                <input type="text" autocomplete="off" class="form-control" id="latitude" name="latitude" value="{{ $user->latitude }}" placeholder="Masukan Latitude Anda">
                                <label for="material-mobile">Latitude </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-material">
                                <input type="text" autocomplete="off" class="form-control" id="longitude" name="longitude" value="{{ $user->longitude }}" placeholder="Masukan Longitude Anda">
                                <label for="material-mobile">Longitude </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-material">
                                <input type="text" autocomplete="off" class="form-control" id="zip_code" name="zip_code" value="{{ $user->zip_code }}" placeholder="Masukan Kode Pos Anda">
                                <label for="material-mobile">Kode Pos </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <img src="{{ $path.$user->avatar }}" alt="{{ $user->name_store }}" width="250px" height="250px">
                            <div class="form-material">
                                <input type="file" autocomplete="off" class="form-control" id="avatar" name="avatar" placeholder="Masukan Foto Anda">
                                <label for="material-mobile">Foto <span class="text-danger">(Kosongkan jika ingin menggunkan avatar yang sama)</span></label>
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-warning">Simpan</button>
                            <button type="reset" class="btn btn-secondary">Reset</button>
                        </div>
                    </form>
                </div>
            </div>
            <!-- END Static Labels -->
        </div>
        <!-- END column -->
    </div>
    <!-- END row -->
</div>
<!-- END content -->

@endsection

@push('script')

    <script src="{{ asset('assets/js/plugins/select2/select2.full.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/jquery-validation/additional-methods.min.js') }}"></script>
    <script>
        jQuery(function () {
            // Init page helpers (Select2 plugin)
            Codebase.helpers(['select2','datepicker']);

            $('#province').bind('change', function(){
                var url = '{{ url("getCity") }}';
                var province = $(this).val();

                $.get(url, {province : province}, function(data){
                    $('#city_id').html(data);
                });
            });
        });

        $('.form-level').validate({
            ignore: [],
            errorClass: 'invalid-feedback animated fadeInDown',
            errorElement: 'div',
            errorPlacement: function(error, e) {
                jQuery(e).parents('.form-group').append(error);
            },
            highlight: function(e) {
                jQuery(e).closest('.form-group').removeClass('is-invalid').addClass('is-invalid');
            },
            success: function(e) {
                jQuery(e).closest('.form-group').removeClass('is-invalid');
                jQuery(e).remove();
            },
            rules: {
                'name': {
                    required: true
                },
                'email': {
                    required: true,
                    email: true
                },
                'mobile': {
                    required: true,
                },
                'password': {
                    minlength: 5
                },
                'confirm-password': {
                    equalTo: '#password'
                },
                'province': {
                    required: true,
                },
                'city_id': {
                    required: true,
                },
                'mitra_id': {
                    required: true,
                },
            },
            messages: {
                'name': {
                    required: 'Inputan nama harus diisi',
                },
                'email': {
                    required: 'Inputan email harus diisi',
                },
                'mobile': 'Inputan telepon harus diisi',
                'password': {
                    minlength: 'Isian kata sandi minimal terdiri dari 5 karakter atau lebih'
                },
                'confirm-password': {
                    minlength: 'Isian kata sandi minimal terdiri dari 5 karakter atau lebih',
                    equalTo: 'Kata sandi tidak sama'
                },
                'province': 'Propinsi harus dipilih',
                'city_id': 'Kota harus dipilih',
                'mitra_id': 'Mitra harus dipilih'
            }
        });
    </script>
@endpush
