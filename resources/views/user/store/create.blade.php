@extends('layouts.root')

@section('title','Tambah Toko Baru')

@section('content')

<!-- Page Content -->
<div class="content">

    <nav class="breadcrumb bg-white push">
        <a class="breadcrumb-item" href="javascript:void(0)">Dashboard</a>
        <a class="breadcrumb-item" href="{{ url('store') }}">Toko</a>
        <span class="breadcrumb-item active">Tambah</span>
    </nav>
    <!-- Material Design -->
    <div class="row">
        <div class="col-md-12">
            <!-- Static Labels -->
            <div class="block">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Tambah Toko Baru</h3>
                </div>
                <div class="block-content">
                    <form class="form-level" action="{{ url('store') }}" method="post" enctype=multipart/form-data>
                        @csrf
                        <div class="form-group">
                            <div class="form-material">
                                <input type="text" autocomplete="off" class="form-control" id="name" name="name" placeholder="Masukan nama Anda">
                                <label for="material-name">Nama <span class="text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-material">
                                <select class="js-select2 form-control" id="gender" name="gender" data-placeholder="Pilih Jenis Kelamin">
                                    <option value="1">Laki - laki</option>
                                    <option value="2">Perempuan</option>
                                </select>
                                <label for="province">Jenis Kelamin <span class="text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-material">
                                <input type="text" autocomplete="off" class="form-control" id="email" name="email" placeholder="Masukan email Anda">
                                <label for="material-email">Email <span class="text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-material">
                                <input type="text" autocomplete="off" class="form-control" id="mobile" name="mobile" placeholder="Masukan Telpon Anda">
                                <label for="material-mobile">Telepon <span class="text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-material">
                                <input type="text" class="js-datepicker form-control" id="birth_date" name="birth_date" data-week-start="1" data-autoclose="true" data-today-highlight="true" data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy">
                                <label for="birth_date">Tanggal Lahir</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-material">
                                <input type="password" autocomplete="off" class="form-control" id="password" name="password" placeholder="Masukan kata sandi Anda">
                                <label for="material-password">Kata Sandi <span class="text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-material">
                                <input type="password" autocomplete="off" class="form-control" id="confirm-password" name="confirm-password" placeholder="Masukan konfirmasi kata sandi Anda">
                                <label for="material-conf-password">Konfirmasi Kata Sandi <span class="text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-material">
                                <input type="text" autocomplete="off" class="form-control" id="no_id_card" name="no_id_card" placeholder="Masukan No Ktp Anda">
                                <label for="material-mobile">No Ktp </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-material">
                                <input type="text" autocomplete="off" class="form-control" id="name_store" name="name_store" placeholder="Masukan Nama Toko Anda">
                                <label for="material-mobile">Nama Toko </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-material">
                                <select class="js-select2 form-control" id="province" name="province" data-placeholder="Pilih Propinsi">
                                    <option></option>
                                    @foreach($provinces as $province)
                                        <option value="{{ $province->id }}">{{ $province->province }}</option>
                                    @endforeach
                                </select>
                                <label for="province">Propinsi <span class="text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-material">
                                <select class="js-select2 form-control" id="city_id" name="city_id" data-placeholder="Pilih Kota">
                                    <option></option>
                                </select>
                                <label for="city_id">Kota <span class="text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-material">
                                <textarea autocomplete="off" class="form-control" id="address" name="address" placeholder="Masukan Alamat Anda"></textarea>
                                <label for="material-mobile">Alamat <span class="text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-material">
                                <input type="text" autocomplete="off" class="form-control" id="latitude" name="latitude" placeholder="Masukan Latitude Anda">
                                <label for="material-mobile">Latitude </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-material">
                                <input type="text" autocomplete="off" class="form-control" id="longitude" name="longitude" placeholder="Masukan Longitude Anda">
                                <label for="material-mobile">Longitude </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-material">
                                <input type="text" autocomplete="off" class="form-control" id="zip_code" name="zip_code" placeholder="Masukan Kode Pos Anda">
                                <label for="material-mobile">Kode Pos </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-material">
                                <input type="text" autocomplete="off" class="form-control" id="limit" name="limit" placeholder="Masukan Limit Toko">
                                <label for="material-mobile">Limit Toko </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-material">
                                <select class="form-control js-select2" multiple id="vendor_id" name="vendor_id[]" placeholder="Pilih Vendor">
                                    @foreach($vendors as $opt)
                                        <option value="{{ $opt->id }}">{{ $opt->name }}</option>
                                    @endforeach
                                </select>
                                <label for="material-select">Vendor / Supplier</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-material">
                                <input type="file" autocomplete="off" class="form-control" id="avatar" name="avatar" placeholder="Masukan Foto Anda">
                                <label for="material-mobile">Foto </label>
                            </div>
                        </div>
                        @if(\Auth::user()->level_id == 1 || \Auth::user()->level_id == 2)
                            <div class="form-group">
                                <div class="form-material">
                                    <select class="form-control js-select2" multiple id="mitra_id" name="mitra_id" placeholder="Pilih Agen">
                                        @foreach($mitras as $opt)
                                            <option value="{{ $opt->id }}">{{ $opt->name }}</option>
                                        @endforeach
                                    </select>
                                    <label for="material-select">Agen</label>
                                </div>
                            </div>
                        @endif
                        <div class="form-group">
                            <button type="submit" class="btn btn-warning">Simpan</button>
                            <button type="reset" class="btn btn-secondary">Reset</button>
                        </div>
                    </form>
                </div>
            </div>
            <!-- END Static Labels -->
        </div>
        <!-- END column -->
    </div>
    <!-- END row -->
</div>
<!-- END content -->

@endsection

@push('script')

    <script src="{{ asset('assets/js/plugins/select2/select2.full.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/jquery-validation/additional-methods.min.js') }}"></script>
    <script>
        jQuery(function () {
            // Init page helpers (Select2 plugin)
            Codebase.helpers(['select2','datepicker']);

            $('#province').bind('change', function(){
                var url = '{{ url("getCity") }}';
                var province = $(this).val();

                $.get(url, {province : province}, function(data){
                    $('#city_id').html(data);
                });
            });
        });

        $('.form-level').validate({
            ignore: [],
            errorClass: 'invalid-feedback animated fadeInDown',
            errorElement: 'div',
            errorPlacement: function(error, e) {
                jQuery(e).parents('.form-group').append(error);
            },
            highlight: function(e) {
                jQuery(e).closest('.form-group').removeClass('is-invalid').addClass('is-invalid');
            },
            success: function(e) {
                jQuery(e).closest('.form-group').removeClass('is-invalid');
                jQuery(e).remove();
            },
            rules: {
                'name': {
                    required: true
                },
                'email': {
                    required: true,
                    email: true
                },
                'mobile': {
                    required: true,
                },
                'password': {
                    required: true,
                    minlength: 5
                },
                'confirm-password': {
                    required: true,
                    equalTo: '#password'
                },
                'province': {
                    required: true,
                },
                'city': {
                    required: true,
                },
            },
            messages: {
                'name': {
                    required: 'Inputan nama harus diisi',
                },
                'email': {
                    required: 'Inputan email harus diisi',
                },
                'mobile': 'Inputan telepon harus diisi',
                'password': {
                    required: 'Inputan kata sandi harus diisi',
                    minlength: 'Isian kata sandi minimal terdiri dari 5 karakter atau lebih'
                },
                'confirm-password': {
                    required: 'Inputan konfirmasi kata sandi harus diisi',
                    minlength: 'Isian kata sandi minimal terdiri dari 5 karakter atau lebih',
                    equalTo: 'Kata sandi tidak sama'
                },
                'province': 'Propinsi harus dipilih',
                'city_id': 'Kota harus dipilih'
            }
        });
    </script>
@endpush
