@extends('layouts.root')

@section('title','Radius')

@section('content')
<div class="container">
    <div class="content">
        <nav class="breadcrumb bg-white push">
            <a class="breadcrumb-item" href="javascript:void(0)">Dashboard</a>
            <span class="breadcrumb-item active">Radius</span>
        </nav>

        @if ($errors->any())
    	    <div class="alert alert-danger m-t-20">
    	        <ul>
    	            @foreach ($errors->all() as $error)
    	                <li>{{ $error }}</li>
    	            @endforeach
    	        </ul>
    	    </div>
    	@endif

    	@if(Session::has('status'))
            @if(Session::get('status') == '200')
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-check"></i> Berhasil!</h3>
                    <p class="mb-0">{{ Session::get('msg') }}</p>
                </div>
            @elseif(Session::get('status') == 'err')
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-check"></i> Gagal!</h3>
                    <p class="mb-0">{{ Session::get('msg') }}</p>
                </div>
            @endif
        @endif

        <!-- Table -->
        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">Table</h3>
            </div>
            <div class="block-content">
                <div class="table-responsive">
                    <table class="table table-vcenter table-stripped">
                        <thead>
                            <tr>
                                <th style="width: 50px;">No</th>
                                <th>Radius (miles)</th>
                                <th>Radius (km)</th>
                                <th>Tipe</th>
                                <th style="width: 200px;"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($radius) > 0)
                                @foreach($radius as $key => $result)
                                    <tr>
                                        <td>{{ ($radius->currentpage()-1) * $radius->perpage() + $key + 1 }}</td>
                                        <td>{{ $result->radius }} miles</td>
                                        <td>{{ round($result->radius*1.609344, 2) }} km</td>
                                        <td>{{ $result->is_radius_reject == 1 ? "Radius Reject" : "Radius Normal" }}</td>
                                        <td>
                                            <a href="{{ url('radius/edit/'. $result->id) }}" class="btn btn-sm btn-primary" data-toggle="tooltip" title="Ubah">
                                                <i class="fa fa-pencil"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
    						@else
    							<tr>
    								<td colspan='4'>Data Tidak Tersedia</td>
    							</tr>
                            @endif
                        </tbody>
                    </table>
                    <div class="row">
                        <div class="col-sm-12">
                            <span class="text-danger">Note : 1 miles = 1.609344 km</span> <br><br>
                        </div>
                    </div>
                </div>
                {{ $radius->links() }}
            </div>
        </div>
        <!-- END Table -->
    </div>


</div>

@include('modal_delete')
@endsection

@push('script')
    <!-- DataTables -->
    <script src="{{ asset('themes/plugins/datatables/jquery.dataTables.min.js') }} "></script>
    <script src="{{ asset('themes/plugins/datatables/dataTables.bootstrap.js') }} "></script>

    <script type="text/javascript">
        $(document).ready(function() {
            $('#datatable').dataTable({
            	"pageLength": 50,
            	"lengthChange": false,
            	"searching": false
            });
        } );

        $('.btn-delete').click(function(){
        	var action = $(this).data('action');

        	$('#modalDelete').modal('show');
        	$('.modal-title').html('Delete Level');

            $('#formDelete').attr('action',action);
            $('#formDelete').append("<input type='hidden' name='_method' value='DELETE'>");
        });
    </script>
@endpush
