@extends('layouts.root')

@section('title','Coverage Area')

@section('content')
<div class="container">
    <div class="content">
        <nav class="breadcrumb bg-white push">
            <a class="breadcrumb-item" href="javascript:void(0)">Dashboard</a>
            <span class="breadcrumb-item active">Coverage Area</span>
        </nav>

        @if ($errors->any())
    	    <div class="alert alert-danger m-t-20">
    	        <ul>
    	            @foreach ($errors->all() as $error)
    	                <li>{{ $error }}</li>
    	            @endforeach
    	        </ul>
    	    </div>
    	@endif

    	@if(Session::has('status'))
            @if(Session::get('status') == '200')
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-check"></i> Berhasil!</h3>
                    <p class="mb-0">{{ Session::get('msg') }}</p>
                </div>
            @elseif(Session::get('status') == 'err')
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-check"></i> Gagal!</h3>
                    <p class="mb-0">{{ Session::get('msg') }}</p>
                </div>
            @endif
        @endif

        <!-- Filter -->
        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">Pencarian</h3>
            </div>
			<form action="{{ url('coverage_area') }}" class="form-inline" method="GET">
            <div class="block-content">
                <div class="row mb-3">
                    <div class="col-4">
            			<table width="100%" cellpadding="5">
            				<tr>
            					<td class="pt-0">
            						<label for="province">Provinsi</label>
            					</td>
            					<td class="pt-0">
            						<select class="js-select2 form-control" name="province" data-placeholder="Pilih Provinsi" style="width:100%;">
                                        <option value="">Pilih Provinsi</option>
                                        @foreach(json_decode($provinces) as $key)
                                            <option @if(Request::get("province") == $key->province) selected="selected" @endif value="{{ $key->id }}">{{ $key->province }}</option>
                                        @endforeach
                                    </select>
            					</td>
            				</tr>
            				<tr><td colspan="2" style="height: 15px;"></td></tr>
            				<tr>
            					<td class="pt-0">
            						<label for="city">Kota</label>
            					</td>
            					<td class="pt-0">
            						<select class="js-select2 form-control" name="city" data-placeholder="Pilih Kota" style="width:100%;">
                                        <option value="">Pilih Kota</option>
                                    </select>
            					</td>
            				</tr>
            			</table>
            		</div>
            		<div class="col-4">
            			<table width="100%" cellpadding="5">
            				<tr>
            					<td class="pt-0">
            						<label for="agent">Agen</label>
            					</td>
            					<td class="pt-0">
            						<select class="js-select2 form-control" name="mitra" data-placeholder="Pilih Customer" style="width:100%;">
                                        <option value="">Pilih Agen</option>
                                    </select>
            					</td>
            				</tr>
            				<tr><td colspan="2" style="height: 15px;"></td></tr>
            				<tr>
            					<td class="pt-0">
            						<label for="store">Toko</label>
            					</td>
            					<td class="pt-0">
            						<select class="js-select2 form-control" name="store" data-placeholder="Pilih Toko" style="width:100%;">
                                        <option value="">Pilih Toko</option>
                                    </select>
            					</td>
            				</tr>
            			</table>
            		</div>
                    <div class="col-4 align-self-start">
                        <button class="btn btn-warning" style="width: 100%"> <i class="fa fa-eye"></i> Lihat</button>
                    </div>
                </div>
            </div>
			</form>
        </div>

        <!-- Table -->
        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">Coverage Area</h3>
            </div>
            <div class="block-content">
                <div class="table-responsive">
                	<div id="map-canvas" style="height:300px; border: 2px solid #3872ac;"></div>
                </div>
            </div>
        </div>
        <!-- END Table -->
    </div>


</div>

@endsection

@push('script')
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCv200mEGfbuiFnGWWXRvi8UATWwzszGu0&callback=initMap&libraries=places&region=ID" type="text/javascript" async defer></script>
	<script>
		var locations = [];
		var store = JSON.parse('@php echo $stores; @endphp');
		var mitra = JSON.parse('@php echo $mitras; @endphp');
		var select_mitra = JSON.parse('@php echo $select_mitras; @endphp');
		var select_store = JSON.parse('@php echo $select_stores; @endphp');
		var city = JSON.parse('@php echo $cities; @endphp');

		$('select[name=province').change(function(){
			$('select[name=city]').html('<option value="">Pilih Kota</option>');
			if ($(this).val()) {
				var val = $(this).val();
				$.each(city, function(key, value){
					if (value['province_id'] == val) {
						$('select[name=city]').append('<option value="'+value['id']+'">'+value['city']+'</option>');	
					}
				});
			}
		});

		$('select[name=city').change(function(){
			$('select[name=mitra]').html('<option value="">Pilih Agen</option>');
			if ($(this).val()) {
				var val = $(this).val();
				$.each(select_mitra, function(key, value){
					if (value['city_id'] == val) {
						$('select[name=mitra]').append('<option value="'+value['id']+'">'+value['name']+'</option>');
					}
				});
			}
		});

		$('select[name=mitra').change(function(){
			$('select[name=store]').html('<option value="">Pilih Toko</option>');
			if ($(this).val()) {
				var val = $(this).val();
				$.each(select_store, function(key, value){
					if (value['mitra_id'] == val) {
						$('select[name=store]').append('<option value="'+value['id']+'">'+value['name']+'</option>');	
					}
				});
			}
		});

		/**
		 * Trigger Change
		 */
		var selected_province = '@php echo Request::get("province") @endphp';
		if (selected_province)
			$('select[name=province]').val(selected_province).trigger('change');

		var selected_city = '@php echo Request::get("city") @endphp';
		if (selected_city)
			$('select[name=city]').val(selected_city).trigger('change');

		var selected_mitra = '@php echo Request::get("mitra") @endphp';
		if (selected_mitra)
			$('select[name=mitra]').val(selected_mitra).trigger('change');

		var selected_store = '@php echo Request::get("store") @endphp';
		if (selected_store)
			$('select[name=store]').val(selected_store).trigger('change');

		$.each(mitra, function(key, value){
			locations.push(
				[value['name_store'], value['latitude'], value['longitude']]
			);
		});

		$.each(store, function(key, value){
			locations.push(
				[value['name_store'], value['latitude'], value['longitude']]
			);
		});

		var map;
		function initMap()
		{
			map = new google.maps.Map(document.getElementById('map-canvas'), {
				mapTypeId: google.maps.MapTypeId.ROADMAP
			});

			//create empty LatLngBounds object
			var bounds = new google.maps.LatLngBounds();
			var infowindow = new google.maps.InfoWindow();

			for (i = 0; i < locations.length; i++) {  
				var marker = new google.maps.Marker({
					position: new google.maps.LatLng(locations[i][1], locations[i][2]),
					map: map
				});

				//extend the bounds to include each marker's position
				bounds.extend(marker.position);

				google.maps.event.addListener(marker, 'click', (function(marker, i) {
					return function() {
						infowindow.setContent(locations[i][0]);
						infowindow.open(map, marker);
					}
				})(marker, i));
			}

			//now fit the map to the newly inclusive bounds
			map.fitBounds(bounds);

			//(optional) restore the zoom level after the map is done scaling
			// var listener = google.maps.event.addListener(map, "idle", function () {
			//     map.setZoom(5);
			//     google.maps.event.removeListener(listener);
			// });
		}
	</script>
@endpush
