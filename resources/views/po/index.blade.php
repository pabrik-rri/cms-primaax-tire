@extends('layouts.root')

@section('title','PO')

@section('content')
<div class="container">
    <div class="content">
        <nav class="breadcrumb bg-white push">
            <a class="breadcrumb-item" href="javascript:void(0)">Dashboard</a>
            <span class="breadcrumb-item active">PO</span>
        </nav>

        @if ($errors->any())
    	    <div class="alert alert-danger m-t-20">
    	        <ul>
    	            @foreach ($errors->all() as $error)
    	                <li>{{ $error }}</li>
    	            @endforeach
    	        </ul>
    	    </div>
    	@endif

    	@if(Session::has('status'))
            @if(Session::get('status') == '200')
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-check"></i> Berhasil!</h3>
                    <p class="mb-0">{{ Session::get('msg') }}</p>
                </div>
            @elseif(Session::get('status') == 'err')
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-check"></i> Gagal!</h3>
                    <p class="mb-0">{{ Session::get('msg') }}</p>
                </div>
            @endif
        @endif

        <h2 class="content-heading">
            PO
        </h2>

        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">Filter</h3>
            </div>
            <div class="block-content">
                <form action="{{ url('pre_order') }}" class="form-inline" method="GET">
                    <div class="form-group" style="width: 25%">
                        <label for="">Tanggal Mulai</label>
                        <input type="text" autocomplete="off" class="form-control js-datepicker" value="{{ Request::get('start') != "" ? Request::get('start') : date('d-m-Y') }}" id="start" data-week-start="1" data-autoclose="true" data-today-highlight="true" data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy" name="start" style="width: 100%" placeholder="Pilih tanggal mulai">
                    </div>
                    <div class="form-group" style="margin-left:10px; width: 25%">
                        <label for="">Tanggal Selesai</label>
                        <input type="text" autocomplete="off" class="form-control js-datepicker" value="{{ Request::get('finish') != "" ? Request::get('finish') : date('d-m-Y') }}" id="finish" data-week-start="1" data-autoclose="true" data-today-highlight="true" data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy" name="finish" style="width: 100%" placeholder="Pilih tanggal selesai">
                    </div>
                    <div class="form-group" style="margin-left:10px; width: 25%">
                        <label for="">Status PO</label>
                        <select name="status" id="status" class="form-control" style="width: 100%">
                            <option value="">Semua Status</option>
                            <option value="0" @if(Request::get("status") == '0') selected @endif>Pending</option>
                            <option value="1" @if(Request::get("status") == '1') selected @endif>Diterima</option>
                            <option value="2" @if(Request::get("status") == '2') selected @endif>Persiapan pengiriman</option>
                            <option value="3" @if(Request::get("status") == '3') selected @endif>Dalam pengiriman</option>
                            <option value="3" @if(Request::get("status") == '4') selected @endif>Batal</option>
                            <option value="3" @if(Request::get("status") == '5') selected @endif>Selesai</option>
                        </select>
                    </div>
                    <div class="form-group" style="margin:20px 0 0 5px; width: 21.5%">
                        <input type="hidden" name="_type" value="export">
                        <button class="btn btn-success" style="width: 100%" value="export"> <i class="fa fa-file-excel-o"></i> Export</button>
                    </div>
                </form> <br>
            </div>
        </div>

        <!-- filter -->
        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">Pencarian</h3>
            </div>
            <div class="block-content">
                <form action="{{ url('pre_order') }}" class="form-inline" method="GET">
                    <div class="form-group" style="margin-bottom: 10px; width: 25%">
                        <label for="">Tanggal Mulai</label>
                        <input type="text" autocomplete="off" class="form-control js-datepicker" value="{{ Request::get('start') != "" ? Request::get('start') : date('d-m-Y') }}" id="start" data-week-start="1" data-autoclose="true" data-today-highlight="true" data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy" name="start" style="width: 100%" placeholder="Pilih tanggal mulai">
                    </div>
                    <div class="form-group" style="margin-left:10px; margin-bottom: 10px; width: 25%">
                        <label for="">Tanggal Selesai</label>
                        <input type="text" autocomplete="off" class="form-control js-datepicker" value="{{ Request::get('finish') != "" ? Request::get('finish') : date('d-m-Y') }}" id="finish" data-week-start="1" data-autoclose="true" data-today-highlight="true" data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy" name="finish" style="width: 100%" placeholder="Pilih tanggal selesai">
                    </div>
                    <div class="form-group" style="margin-left:10px; margin-bottom: 10px; width: 47%">
                        <label for="">Status PO</label>
                        <select name="status" id="status" class="form-control" style="width: 100%">
                            <option value="">Semua Status</option>
                            <option value="0" @if(Request::get("status") == '0') selected @endif>Pending</option>
                            <option value="1" @if(Request::get("status") == '1') selected @endif>Diterima</option>
                            <option value="2" @if(Request::get("status") == '2') selected @endif>Persiapan pengiriman</option>
                            <option value="3" @if(Request::get("status") == '3') selected @endif>Dalam pengiriman</option>
                            <option value="3" @if(Request::get("status") == '4') selected @endif>Batal</option>
                            <option value="3" @if(Request::get("status") == '5') selected @endif>Selesai</option>
                        </select>
                    </div>
                    <div class="form-group" style="width: 25%">
                        <label for="">Berdasarkan</label>
                        <select name="key" id="key" class="form-control" style="width: 100%">
                            <option value="">Cari Berdasarkan</option>
                            <option value="orders.order_code" @if(Request::get("key") == 'orders.order_code') selected @endif>Kode</option>
                            <option value="mitra.name" @if(Request::get("key") == 'mitra.name') selected @endif>Mitra</option>
                        </select>
                    </div>
                    <div class="form-group" style="margin-left:10px; width: 51%">
                        <label for="">Pencarian</label>
                        <input type="text" autocomplete="off" class="form-control" id="value" name="value" value="{{ Request::get('value') }}" style="width: 100%" placeholder="Masukan data pencarian">
                    </div>
                    <div class="form-group" style="margin:20px 0 0 10px; width: 21.5%">
                        <button class="btn btn-warning" style="width: 100%"> <i class="fa fa-search"></i> Cari</button>
                    </div>
                </form> <br>
            </div>
        </div>

        <!-- Table -->
        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">Table</h3>
            </div>
            <div class="block-content">
                <div class="table-responsive">
                    <table class="table table-vcenter table-stripped">
                        <thead>
                            <tr>
                                <th style="width: 50px;">No</th>
                                <th>Kode</th>
                                <th width="20%">Tanggal</th>
                                <th>Status</th>
                                <th>Alamat</th>
                                <th>Agen / Toko</th>
                                <th>Total</th>
                                <th style="width: 200px;"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($order) > 0)
                                @foreach($order as $key => $result)
                                    <tr>
                                        <td>{{ ($order->currentpage()-1) * $order->perpage() + $key + 1 }}</td>
                                        <td>{{ $result->order_code }}</td>
                                        <td>{{ date_format(date_create($result->order_date),'d M Y H:i:s') }}</td>
                                        <td>{{ $result->order_status }}</td>
                                        <td>{{ $result->order_address }}</td>
                                        <td>{{ $result->mitra }}</td>
                                        <td>Rp. {{ number_format($result->total_price) }}</td>
                                        <td>
                                            <a href="{{ url('pre_order/show/'. $result->id) }}" class="btn btn-sm btn-primary" data-toggle="tooltip" title="Detail">
                                                <i class="fa fa-search"></i>
                                            </a>
                                            @if(in_array($result->status, [0,1,2,3]))
                                                <button data-toggle="tooltip" title="Batal" data-action="{{ url('pre_order/cancel/'. $result->id) }}" class="btn btn-sm btn-warning btn-xs btn-cancel">
                                                    <i class="fa fa-ban"></i>
                                                </button>
                                            @endif
                                            <button  data-toggle="tooltip" title="Hapus" data-action="{{ url('pre_order/delete/'. $result->id) }}" class="btn btn-sm btn-danger btn-xs btn-delete">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach
    						@else
    							<tr>
    								<td colspan='8'>Data Tidak Tersedia</td>
    							</tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                {{ $order->appends([ 'key' => Request::get('key'), 'value' => Request::get('value'), 'start' => Request::get('start'),'finish' => Request::get('finish'),'status' => Request::get('status')])->links() }}
            </div>
        </div>
        <!-- END Table -->
    </div>


</div>

@include('modal_delete')
@include('modal_cancel')
@endsection

@push('script')
    <!-- DataTables -->
    <script src="{{ asset('themes/plugins/datatables/jquery.dataTables.min.js') }} "></script>
    <script src="{{ asset('themes/plugins/datatables/dataTables.bootstrap.js') }} "></script>
    <script src="{{ asset('assets/js/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/jquery-validation/additional-methods.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
    <script>
        jQuery(function () {
            // Init page helpers (Select2 plugin)
            Codebase.helpers(['datepicker']);
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#datatable').dataTable({
            	"pageLength": 50,
            	"lengthChange": false,
            	"searching": false
            });
        } );

        $('.btn-delete').click(function(){
            var action = $(this).data('action');

            $('#modalDelete').modal('show');

            $('#formDelete').attr('action',action);
            $('#formDelete').append("<input type='hidden' name='_method' value='DELETE'>");
        });

        $('.btn-cancel').click(function(){
        	var action = $(this).data('action');

        	$('#modalCancel').modal('show');

            $('#formCancel').attr('action',action);
            $('#formCancel').append("<input type='hidden' name='_method' value='DELETE'>");
        });
    </script>
@endpush
