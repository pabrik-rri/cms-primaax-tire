@extends('layouts.root')

@section('title','Detail Order')

@section('content')

@php
    switch ($order->status) {
        case '0':
            $status = "<span class='btn btn-warning btn-sm'>Pending</span>";
            break;

        case '1':
            $status = "<span class='btn btn-primary btn-sm'>Diterima</span>";
            break;

        case '2':
            $status = "Persiapan Pengiriman";
            break;

        case '3':
            $status = "Dalam Pengiriman";
            break;

        case '4':
            $status = "<span class='btn btn-danger btn-sm'>Batal</span>";
            break;

        case '5':
            $status = "<span class='btn btn-success btn-sm'>Selesai</span>";
            break;

        case '6':
            $status = "<span class='btn btn-info btn-sm'>Pindahan</span>";
            break;

        default:
            $status = "";
            break;
    }
@endphp

<!-- Page Content -->
<div class="content">

    <nav class="breadcrumb bg-white push">
        <a class="breadcrumb-item" href="javascript:void(0)">Dashboard</a>
        <a class="breadcrumb-item" href="{{ url('order') }}">Order</a>
        <span class="breadcrumb-item active">Detail</span>
    </nav>

     @if ($errors->any())
        <div class="alert alert-danger m-t-20">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    @if(Session::has('status'))
        @if(Session::get('status') == '200')
            <div class="alert alert-success alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-check"></i> Berhasil!</h3>
                <p class="mb-0">{{ Session::get('msg') }}</p>
            </div>
        @elseif(Session::get('status') == 'err')
            <div class="alert alert-danger alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-check"></i> Gagal!</h3>
                <p class="mb-0">{{ Session::get('msg') }}</p>
            </div>
        @endif
    @endif


    <!-- Material Design -->
    <div class="row">
        <div class="col-md-12">
            <!-- Static Labels -->
            <div class="block">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Informasi Order</h3>
                    @if($order->status == 0)
                        <a href="{{ url('pre_order/accept/'. $order->id) }}" class="btn btn-md btn-success" style="margin-right:10px;">Terima</a>
                        <a href="{{ url('pre_order/reject/'. $order->id) }}" class="btn btn-md btn-danger">Batal</a>
                    @elseif($order->status == 1)
                        <a href="{{ url('pre_order/send/'. $order->id) }}" class="btn btn-md btn-success" style="margin-right:10px;">Kirim</a>
                    @elseif($order->status == 3)
                        <a href="{{ url('pre_order/finish/'. $order->id) }}" class="btn btn-md btn-success" style="margin-right:10px;">Selesai</a>
                    @endif
                </div>
                <div class="block-content">
                    <div class="row">
                        <div class="col-sm-6">
                            <label>Kode Order</label>
                            <p>{{ $order->order_code }}</p>
                        </div>
                        <div class="col-sm-6">
                            <label>Tanggal Order</label>
                            <p>{{ date_format(date_create($order->created_at),'d F Y') }}</p>
                        </div>
                        <div class="col-sm-6">
                            <label>Status</label>
                            <p>{!! $status !!}</p>
                        </div>
                        <div class="col-sm-6">
                            <label>Alamat</label>
                            <p>{{ $order->address }}</p>
                        </div>
                        <div class="col-sm-6">
                            <label>Latitude</label>
                            <p>{{ $order->latitude }}</p>
                        </div>
                        <div class="col-sm-6">
                            <label>Longitude</label>
                            <p>{{ $order->longitude }}</p>
                        </div>
                        <div class="col-sm-6">
                            <label>Mitra</label>
                            <p>{{ isset($order->Mitra) ? $order->Mitra->name : "" }}</p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END Static Labels -->

            <div class="block">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Detail Order</h3>
                </div>
                <div class="block-content">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered">
                            <thead>
                                <th width="5%">No</th>
                                <th>Produk</th>
                                <th width="5%">Qty</th>
                                <th class="text-right">Harga</th>
                                <th class="text-right">Total</th>
                                <th>Status</th>
                                <th></th>
                            </thead>
                            <tbody>
                                @foreach($detail as $key => $value)
                                    <tr>
                                        <td>{{ $key+1 }}</td>
                                        <td>{{ $value->Product->name }}</td>
                                        <td>{{ $value->qty }} </td>
                                        <td class="text-right">Rp. {{ number_format($value->price) }}</td>
                                        <td class="text-right">Rp. {{ number_format($value->qty * $value->price) }}</td>
                                        <td>
                                            @if($value->is_send == 0)
                                                <button class="btn btn-sm btn-danger btn-send" data-toggle="tooltip" title="Klik disini jika produk ini sudah selesai dikirimkan" data-action="{{ url('pre_order/finish_item/'. $value->id) }}">
                                                    Belum selesai
                                                </button>
                                            @else
                                                <button class="btn btn-sm btn-success">Selesai</button>
                                            @endif
                                        </td>
                                        <td>
                                            @if($order->status == 5 || $value->is_send == 1)
                                                -
                                            @else       
                                                <button class="btn btn-sm btn-primary btn-edit" data-toggle="tooltip" title="Ubah Qty" data-action="{{ url('pre_order/edit_item/'. $value->id) }}" data-qty="{{ $value->qty }}"><i class="fa fa-pencil"></i></button>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th colspan="5" class="text-right">Rp. {{ number_format($total) }}</th>
                                    <th colspan="2"></th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>

             <!-- Static Labels -->
             <div class="block">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Data Vendor</h3>
                </div>
                <div class="block-content">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                        <th>No</th>
                                        <th>Vendor</th>
                                        <th>Alamat</th>
                                        <th>Telepon</th>
                                        <th>Fax</th>
                                    </thead>
                                    <tbody>
                                        @if(sizeof($vendors) > 0)
                                            @foreach($vendors as $key => $value)
                                                <tr>
                                                    <td>{{ $key+1 }}</td>
                                                    <td>{{ $value->name }}</td>
                                                    <td>{{ $value->address }}</td>
                                                    <td>{{ $value->phone }}</td>
                                                    <td>{{ $value->fax }}</td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="5">Data Vendor tidak tersedia</td>
                                            </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END Static Labels -->
        </div>
        <!-- END column -->
    </div>
    <!-- END row -->
</div>
<!-- END content -->

@include('modal_send')
@include('modal_edit')
@endsection


@push('script')
    <script>
        $('.btn-send').click(function(){
        	var action = $(this).data('action');

        	$('#modalSend').modal('show');
            $('#formSend').attr('action',action);
            $('#formSend').append("<input type='hidden' name='mitra_id' value='{{ $order->mitra_id }}'>");
        });
        
        $('.btn-edit').click(function(){
        	var action = $(this).data('action');
        	var qty = $(this).data('qty');

        	$('#modalEdit').modal('show');
            $('#formEdit').attr('action',action);
            $('#formEdit .po-qty').val(qty);
        });
    </script>
@endpush