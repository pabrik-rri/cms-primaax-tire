@extends('layouts.root')

@section('title','Pembayaran Hutang')

@section('content')
<div class="container">
    <div class="content">
        <nav class="breadcrumb bg-white push">
            <a class="breadcrumb-item" href="javascript:void(0)">Dashboard</a>
            <span class="breadcrumb-item active">Pembayaran Hutang</span>
        </nav>

        @if ($errors->any())
            <div class="alert alert-danger m-t-20">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        @if(Session::has('status'))
            @if(Session::get('status') == '200')
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-check"></i> Berhasil!</h3>
                    <p class="mb-0">{{ Session::get('msg') }}</p>
                </div>
            @elseif(Session::get('status') == 'err')
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-check"></i> Gagal!</h3>
                    <p class="mb-0">{{ Session::get('msg') }}</p>
                </div>
            @endif
        @endif

        <!-- Filter -->
        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">Pencarian</h3>
            </div>
            <form class="form-inline" method="POST" action="{{ url('debt/payment') }}" id="form-save">
            @csrf
            <input type="hidden" name="_method" value="POST">
            <div class="block-content">
                <div id="header" class="col-12 mb-3" style="width: 100% !important;">
                    <div class="row">
                        <div class="col-4">
                            <div class="form-group mb-3">
                                <label for="supplier">Tanggal</label>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="text" autocomplete="off" class="form-control js-datepicker" value="{{ Request::get('now') != '' ? Request::get('now') : date('d-m-Y') }}" id="now" data-week-now="1" data-autoclose="true" data-today-highlight="true" data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy" name="now" placeholder="Pilih tanggal mulai">
                            </div>
                            <div class="form-group mb-3">
                                <label for="supplier">Nama Customer</label>
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                <input id="customer" type="text" class="form-control" value="{{ \Auth::user()->name }}" disabled>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="form-group mb-3">
                                <label for="customer">Nama Supplier</label>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="hidden" name="supplier_id" value="{{ $upline->id }}">
                                <input type="text" name="supplier" class="form-control" value="{{ $upline->name }}" readonly>
                            </div>
                            <div class="form-group mb-3">
                                <label for="customer">Nilai Pembayaran</label>
                                &nbsp;&nbsp;
                                <input type="text" name="payment" class="form-control priceFormatPerfix">
                            </div>
                            <div class="form-group">
                                <label for="customer">Cara Pembayaran</label>
                                &nbsp;&nbsp;&nbsp;
                                <select class="js-select2 form-control" id="customer" name="payment_method" data-placeholder="Pilih Customer">
                                    <option value=0>Transfer</option>
                                    <option value=1>COD</option>
                                    <option value=2>Cek / Giro</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="form-group mb-3">
                                <label for="customer">Status</label>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="text" class="form-control js-datepicker" value="Pembayaran" disabled>
                            </div>
                            <div class="form-group mb-3">
                                <button class="btn btn-success" id="calculate">Hitung</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </form>
        </div>

        <!-- Table -->
        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">Table</h3>
            </div>
            <div class="block-content pb-3">
                <div class="table-responsive">
                    <table class="table table-vcenter table-stripped">
                        <thead>
                            <tr>
                                <th style="width: 50px;">No</th>
                                <th>DO</th>
                                <th>Tanggal Invoice</th>
                                <th>No. Pembelian</th>
                                <th>Nilai Hutang</th>
                                <th>Saldo Hutang</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($receivables) > 0)
                                @php $i = 1 @endphp
                                @foreach($receivables as $key => $result)
                                    <tr class="tr-{{ $result->id }}">
                                        <td>{{ $i }}</td>
                                        <td><input id="do-{{ $result->id }}" data-id="{{ $result->id }}" type="checkbox" class="form-control" name="do[]" onclick="return false;"></td>
                                        <td>{{ date('d-M-Y', strtotime($result->invoice_date)) }}</td>
                                        <td>{{ $result->code }}</td>
                                        <td>Rp. {{ number_format($result->value_of_receivable, 0, ',', '.') }}</td>
                                        <td class="balance">Rp. {{ number_format($result->balance_of_receivable, 0, ',', '.') }}</td>
                                    </tr>
                                    @php $i++ @endphp
                                @endforeach
                            @else
                                <tr>
                                    <td align="center" colspan='7'>Data Tidak Tersedia</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                <a href="{{ url('debt/payment') }}" class="btn btn-danger">Exit</a>
                <button class="btn btn-success" id="save">Simpan</button>
            </div>
        </div>
        <!-- END Table -->
    </div>
</div>

<div class="modal fade" id="print-modal" tabindex="-1" role="dialog" aria-labelledby="modal-fadein" style="display: none;" aria-hidden="true">
    <div class="modal-dialog hide" role="document" aria-hidden="true">
        <form id="print" action="{{ url('debt/payment/export-pdf/' . Session::get('print')) }}" method="GET" target="_blank">
        <input type="hidden" name="_method" value="GET">
        <div class="modal-content">
            <div class="block block-themed block-transparent mb-0">
                <div class="block-header bg-primary-dark">
                    <h3 class="block-title">Pembayaran Hutang</h3>
                    <div class="block-options">
                        <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                            <i class="si si-close"></i>
                        </button>
                    </div>
                </div>
                <div class="block-content">
                    @csrf
                    <div class="row">
                        <div class="col-sm-12 modal-text">
                            <p>Pembayaran Hutang berhasil. Data ingin dicetak</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Tidak</button>
                <button type="submit" class="btn btn-primary btn-modal-delete">Ya</button>
            </div>
        </div>
        </form>
    </div>
</div>

@endsection

@push('script')
    <!-- DataTables -->
    <script src="{{ asset('assets/js/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/jquery-priceformat/jquery.priceformat.min.js') }}"></script>
    <script>
        var receivables = '{{ $receivables }}';
        receivables = JSON.parse(receivables.replace(/&quot;/g,'"'));

        clear();

        $("#print").submit(function(){
            window.location.href = "{{ url('debt/payment')}}";
        });

        $("#calculate").click(function(){
            var payment = parseInt(priceclean($('input[name=payment]').val()));
            if (payment != 0 && !isNaN(payment)) {
                $.each(receivables, function(idx, obj) {
                    if (payment >= obj['balance_of_receivable']) {
                        payment -= obj['balance_of_receivable'];
                        $('tr.tr-' + obj['id'] + ' > td.balance').each(function(){
                            $(this).html('-');
                        });
                        $('#do-' + obj['id']).prop('checked', true);
                    } else {
                        if (payment && payment > 0)
                            $('#do-' + obj['id']).prop('checked', true);
                        else
                            $('#do-' + obj['id']).prop('checked', false);

                        if (payment < 0)
                            payment = -obj['balance_of_receivable'];
                        else
                            payment -= obj['balance_of_receivable'];

                        $('tr.tr-' + obj['id'] + ' > td.balance').each(function(){
                            $(this).html('Rp. ' + currencyFormatDE(-payment));
                        });
                    }
                });
            } else {
                $.each(receivables, function(idx, obj) {
                    $('#do-' + obj['id']).prop('checked', false);

                    $('tr.tr-' + obj['id'] + ' > td.balance').each(function(){
                        $(this).html('Rp. ' + currencyFormatDE(obj['balance_of_receivable']));
                    });
                });
            }
            return false;
        });

        $("#save").click(function(){
            // var array = [];
            // $('input[name^="do"]').map(function() {
            //     if ($(this).prop('checked') == true) {
            //         array.push($(this).data('id'));
            //     }
            // }).get();
            $('#form-save').submit();
        });

        function clear()
        {
            $('input[name=payment]').val('');
            $.each(receivables, function(idx, obj) {
                $('#do-' + obj['id']).prop('checked', false);

                $('tr.tr-' + obj['id'] + ' > td.balance').each(function(){
                    $(this).html('Rp. ' + currencyFormatDE(obj['balance_of_receivable']));
                });
            });
        }

        function currencyFormatDE (num) {
            const formatter = new Intl.NumberFormat('de-DE', {
                minimumFractionDigits: 0
            });
            return formatter.format(num);
        }

        function priceclean(price){
            var price = price;
            price = price.replace("Rp.","");
            price = price.replace(".","");
            price = price.replace(".","");
            price = price.replace(".","");
            price = price.replace(".","");
            price = price.replace(".","");
            price = price.replace(".","");
            price = price.replace(".","");
            price = price.replace(".","");
            price = price.replace(".","");
            price = price.replace(".","");
            price = price.replace(".","");
            price = price.replace(".","");
            price = price.replace(".","");
            price = price.replace(".","");
            price = price.replace(".","");
            price = price.replace("%","");
            price = price.replace(",",".");
            price = eval(price.trim());
            return price;
        }

        jQuery(function () {
            // Init page helpers (Select2 plugin)
            Codebase.helpers(['datepicker']);

            $("body").on("focus", "#header", function(){
                $('.priceFormatPerfix').priceFormat({
                    prefix: 'Rp. ',
                    centsSeparator: '',
                    thousandsSeparator: '.',
                    centsLimit: 0
                });
            });
        });
    </script>

    @if(Session::has('status'))
        @if(!empty(Session::get('print')))
            <script>
                $('#print-modal').modal('show');
            </script>
        @endif
    @endif
@endpush