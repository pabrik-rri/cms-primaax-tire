@extends('layouts.root')

@section('title','Detail Pembayaran Hutang')

@section('content')
<div class="container">
    <div class="content">
        <nav class="breadcrumb bg-white push">
            <a class="breadcrumb-item" href="javascript:void(0)">Dashboard</a>
            <span class="breadcrumb-item active">Detail Pembayaran Hutang</span>
        </nav>

        @if ($errors->any())
            <div class="alert alert-danger m-t-20">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        @if(Session::has('status'))
            @if(Session::get('status') == '200')
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-check"></i> Berhasil!</h3>
                    <p class="mb-0">{{ Session::get('msg') }}</p>
                </div>
            @elseif(Session::get('status') == 'err')
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-check"></i> Gagal!</h3>
                    <p class="mb-0">{{ Session::get('msg') }}</p>
                </div>
            @endif
        @endif

        <!-- Filter -->
        <div class="block">
            <div class="block-content">
                <div class="col-12 mb-3" style="width: 100% !important;">
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group mb-3">
                                <label for="supplier">Tanggal</label><br/>
                                {{ date('d F Y', strtotime($debt->debt_date)) }}
                            </div>
                            <div class="form-group mb-3">
                                <label for="supplier">No Faktur</label><br/>
                                {{ $debt->code }}
                            </div>
                            <div class="form-group mb-3">
                                <label for="supplier">Nama Customer</label><br/>
                                {{ \Auth::User()->name }}
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group mb-3">
                                <label for="supplier">Nama Supplier</label><br/>
                                {{ $upline->name }}
                            </div>
                            <div class="form-group mb-3">
                                <label for="supplier">Nilai Pembayaran</label><br/>
                                Rp. {{ number_format($debt->value_of_debt, 0, ',', '.') }}
                            </div>
                            <div class="form-group mb-3">
                                <label for="supplier">Cara Pembayaran</label><br/>
								@if($debt->payment_method == 0)
									Transfer
								@elseif($debt->payment_method == 1)
									COD
								@else
									Cek/Giro
								@endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Table -->
        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">Table</h3>
            </div>
            <div class="block-content pb-3">
                <div class="table-responsive">
                    <table class="table table-vcenter table-stripped">
                        <thead>
                            <tr>
                                <td>No</td>
								<td>Tanggal</td>
								<td>No Pembelian</td>
								<td>Nilai Hutang</td>
								<td>Saldo Hutang</td>
                            </tr>
                        </thead>
                        <tbody id="table-value">
                            @if(count($debt->debtDetails) > 0)
				                @php $index = 1 @endphp
				                @php $total = 0 @endphp
				                @foreach($debt->debtDetails as $key => $result)
				                	@php $total += $result->receivables->value_of_receivable @endphp
				                    <tr>
				                        <td>{{ $index }}</td>
				                        <td>
				                            {{ date('d-M-Y', strtotime($result->receivables->invoice_date)) }}
				                        </td>
				                        <td>
				                            {{ $result->receivables->code }}
				                        </td>
				                        <td>
				                            Rp. {{ number_format($result->receivables->value_of_receivable, 0, ',', '.') }}
				                        </td>
				                        <td>
				                            Rp. {{ number_format($result->balance, 0, ',', '.') }}
				                        </td>
				                    </tr>
				                    @php $index++ @endphp
				                @endforeach
				            @endif
                        </tbody>
                    </table>
                </div>
                <a href="{{ url('debt/payment/export-pdf/' . $debt->id) }}" class="btn btn-primary">Print</a>
                <a href="{{ url('debt/payment') }}" class="btn btn-danger">Exit</a>
            </div>
        </div>
        <!-- END Table -->
    </div>
</div>

@endsection