@extends('layouts.root')

@section('title','Kontak kami')

@section('content')
<div class="container">
    <div class="content">
        <nav class="breadcrumb bg-white push">
            <a class="breadcrumb-item" href="javascript:void(0)">Dashboard</a>
            <span class="breadcrumb-item active">Kontak kami</span>
        </nav>

        @if ($errors->any())
    	    <div class="alert alert-danger m-t-20">
    	        <ul>
    	            @foreach ($errors->all() as $error)
    	                <li>{{ $error }}</li>
    	            @endforeach
    	        </ul>
    	    </div>
    	@endif

    	@if(Session::has('status'))
            @if(Session::get('status') == '200')
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-check"></i> Berhasil!</h3>
                    <p class="mb-0">{{ Session::get('msg') }}</p>
                </div>
            @elseif(Session::get('status') == 'err')
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-check"></i> Gagal!</h3>
                    <p class="mb-0">{{ Session::get('msg') }}</p>
                </div>
            @endif
        @endif

        <h2 class="content-heading">
            Kontak kami
        </h2>

        <!-- Table -->
        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">Table</h3>
            </div>
            <div class="block-content">
                <div class="table-responsive">
                    <table class="table table-vcenter table-stripped">
                        <thead>
                            <tr>
                                <th style="width: 50px;">No</th>
                                <th>Nama</th>
                                <th>Email</th>
                                <th>Tanggal</th>
                                <th>Pesan</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($contacts) > 0)
                                @foreach($contacts as $key => $result)
                                    <tr>
                                        <td>{{ ($contacts->currentpage()-1) * $contacts->perpage() + $key + 1 }}</td>
                                        <td>{{ $result->first_name }} {{ $result->last_name }}</td>
                                        <td>{{ $result->email }}</td>
                                        <td>{{ date('d F Y H:i:s', strtotime($result->created_at)) }}</td>
                                        <td>{{ substr($result->message, 0, 100) }}</td>
                                        <td>
                                            <a href="{{ url('contact/detail/'. $result->id) }}" class="btn btn-sm btn-primary" data-toggle="tooltip" title="Ubah">
                                                <i class="fa fa-search"></i>
                                            </a>
                                            <button  data-toggle="tooltip" title="Hapus" data-action="{{ url('contact/delete/'. $result->id) }}" class="btn btn-sm btn-danger btn-xs btn-delete">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach
    						@else
    							<tr>
    								<td colspan='6'>Data Tidak Tersedia</td>
    							</tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                {{ $contacts->links() }}
            </div>
        </div>
        <!-- END Table -->
    </div>


</div>

@include('modal_delete')
@endsection

@push('script')
    <script type="text/javascript">
        $('.btn-delete').click(function(){
        	var action = $(this).data('action');

        	$('#modalDelete').modal('show');
        	$('.modal-title').html('Delete Tentang');

            $('#formDelete').attr('action',action);
            $('#formDelete').append("<input type='hidden' name='_method' value='DELETE'>");
        });
    </script>
@endpush
