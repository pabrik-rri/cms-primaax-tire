@extends('layouts.root')

@section('title','Detil Kontak kami')

@section('content')

<!-- Page Content -->
<div class="content">

    <nav class="breadcrumb bg-white push">
        <a class="breadcrumb-item" href="javascript:void(0)">Dashboard</a>
        <a class="breadcrumb-item" href="{{ url('contact') }}">Kontak kami</a>
        <span class="breadcrumb-item active">Detil</span>
    </nav>
    <!-- Material Design -->
    <div class="row">
        <div class="col-md-12">
            <!-- Static Labels -->
            <div class="block">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Detil Kontak kami</h3>
                </div>
                <div class="block-content">
                    <div class="row">
                        <div class="col-sm-6">
                            <label for=""><strong>Nama</strong></label> <br>
                            {{ $contact->first_name }} {{ $contact->last_name }}<br><br>
                        </div>
                        <div class="col-sm-6">
                            <label for=""><strong>Email</strong></label> <br>
                            {{ $contact->email }}<br><br>
                        </div>
                        <div class="col-sm-12">
                            <label for=""><strong>Tanggal kirim</strong></label> <br>
                            {{ date('d F Y H:i:s', strtotime($contact->created_at)) }} <br><br>
                        </div>
                        <div class="col-sm-12">
                            <label for=""><strong>Pesan</strong></label> <br>
                            {{ $contact->message }} <br><br>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END Static Labels -->
        </div>
        <!-- END column -->
    </div>
    <!-- END row -->
</div>
<!-- END content -->

@endsection
