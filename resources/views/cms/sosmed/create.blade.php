@extends('layouts.root')

@section('title','Tambah Media Sosial Baru')

@section('content')

<!-- Page Content -->
<div class="content">

    <nav class="breadcrumb bg-white push">
        <a class="breadcrumb-item" href="javascript:void(0)">Dashboard</a>
        <a class="breadcrumb-item" href="{{ url('sosmed') }}">Media Sosial</a>
        <span class="breadcrumb-item active">Tambah</span>
    </nav>
    <!-- Material Design -->
    <div class="row">
        <div class="col-md-12">
            <!-- Static Labels -->
            <div class="block">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Tambah Media Sosial Baru</h3>
                </div>
                <div class="block-content">
                    <form class="form-level" action="{{ url('sosmed') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <div class="form-material">
                                <input type="text" autocomplete="off" class="form-control" id="name" name="name" placeholder="Masukan nama media sosial">
                                <label for="material-email">Nama Media Sosial <span class="text text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-material">
                                <input type="text" autocomplete="off" class="form-control" id="url" name="url" placeholder="Masukan link media sosial Anda">
                                <label for="material-email">Link Media Sosial (<i>copy link</i> dengan http / https nya) <span class="text text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-material">
                                <input type="file" autocomplete="off" class="form-control" id="image" name="image">
                                <label for="material-email">Image <span class="text text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-warning">Simpan</button>
                            <button type="reset" class="btn btn-secondary">Reset</button>
                        </div>
                    </form>
                </div>
            </div>
            <!-- END Static Labels -->
        </div>
        <!-- END column -->
    </div>
    <!-- END row -->
</div>
<!-- END content -->

@endsection

@push('script')

    <script src="{{ asset('assets/js/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/jquery-validation/additional-methods.min.js') }}"></script>
    <script>
        $('.form-level').validate({
            ignore: [],
            errorClass: 'invalid-feedback animated fadeInDown',
            errorElement: 'div',
            errorPlacement: function(error, e) {
                jQuery(e).parents('.form-group').append(error);
            },
            highlight: function(e) {
                jQuery(e).closest('.form-group').removeClass('is-invalid').addClass('is-invalid');
            },
            success: function(e) {
                jQuery(e).closest('.form-group').removeClass('is-invalid');
                jQuery(e).remove();
            },
            rules: {
                'image': 'required',
                'url': 'required',
                'name': {
                    required: true,
                    minlength: 3
                }
            },
            messages: {
                'image' : 'Gambar media sosial harus di isi',
                'url' : 'Link media sosial harus di isi',
                'name': {
                    required: 'Nama media sosial harus diisi',
                    minlength: 'Minimal karakter terdiri dari 3 huruf'
                }
            }
        });
    </script>
@endpush
