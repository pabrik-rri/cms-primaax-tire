@extends('layouts.root')

@section('title','Slider')

@section('content')
<div class="container">
    <div class="content">
        <nav class="breadcrumb bg-white push">
            <a class="breadcrumb-item" href="javascript:void(0)">Dashboard</a>
            <span class="breadcrumb-item active">Galeri</span>
        </nav>

        @if ($errors->any())
    	    <div class="alert alert-danger m-t-20">
    	        <ul>
    	            @foreach ($errors->all() as $error)
    	                <li>{{ $error }}</li>
    	            @endforeach
    	        </ul>
    	    </div>
    	@endif

    	@if(Session::has('status'))
            @if(Session::get('status') == '200')
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-check"></i> Berhasil!</h3>
                    <p class="mb-0">{{ Session::get('msg') }}</p>
                </div>
            @elseif(Session::get('status') == 'err')
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-check"></i> Gagal!</h3>
                    <p class="mb-0">{{ Session::get('msg') }}</p>
                </div>
            @endif
        @endif

        <h2 class="content-heading">
            <a class="btn btn-sm btn-warning float-right" href="{{ url('gallery/create') }}">
                <i class="fa fa-plus"></i> &nbsp; Tambah Galeri Baru
            </a>
            Galeri
        </h2>

        <!-- Table -->
        <div class="block">
            <div class="block-content">
                
                <div class="row">
                    @if(count($gallery) > 0)
                        @foreach($gallery as $key => $result)
                            <div class="col-md-3 text-center">
                                <img src="{{ $path.$result->image }}" alt="{{ $result->title }}" width="100%" style="margin-bottom: 15px;"> <br>
                                <a href="{{ url('gallery/edit/'. $result->id) }}" class="btn btn-sm btn-primary" data-toggle="tooltip" title="Ubah">
                                    <i class="fa fa-pencil"></i>
                                </a>
                                <button  data-toggle="tooltip" title="Hapus" data-action="{{ url('gallery/delete/'. $result->id) }}" class="btn btn-sm btn-danger btn-xs btn-delete">
                                    <i class="fa fa-trash"></i>
                                </button>
                            </div>
                        @endforeach
                    @else
                        <div class="col-sm-12">
                            <p class="text-center">Data Galeri tidak tersedia.</p>
                        </div>
                    @endif
                </div>

                <br>
                {{ $gallery->links() }}

                <br>
            </div>
        </div>
        <!-- END Table -->
    </div>


</div>

@include('modal_delete')
@endsection

@push('script')
    <!-- DataTables -->
    <script src="{{ asset('themes/plugins/datatables/jquery.dataTables.min.js') }} "></script>
    <script src="{{ asset('themes/plugins/datatables/dataTables.bootstrap.js') }} "></script>

    <script type="text/javascript">
        $(document).ready(function() {
            $('#datatable').dataTable({
            	"pageLength": 50,
            	"lengthChange": false,
            	"searching": false
            });
        } );

        $('.btn-delete').click(function(){
        	var action = $(this).data('action');

        	$('#modalDelete').modal('show');
        	$('.modal-title').html('Delete Level');

            $('#formDelete').attr('action',action);
            $('#formDelete').append("<input type='hidden' name='_method' value='DELETE'>");
        });
    </script>
@endpush
