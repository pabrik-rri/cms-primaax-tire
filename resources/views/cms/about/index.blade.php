@extends('layouts.root')

@section('title','Tentang')

@section('content')
<div class="container">
    <div class="content">
        <nav class="breadcrumb bg-white push">
            <a class="breadcrumb-item" href="javascript:void(0)">Dashboard</a>
            <span class="breadcrumb-item active">Tentang</span>
        </nav>

        @if ($errors->any())
    	    <div class="alert alert-danger m-t-20">
    	        <ul>
    	            @foreach ($errors->all() as $error)
    	                <li>{{ $error }}</li>
    	            @endforeach
    	        </ul>
    	    </div>
    	@endif

    	@if(Session::has('status'))
            @if(Session::get('status') == '200')
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-check"></i> Berhasil!</h3>
                    <p class="mb-0">{{ Session::get('msg') }}</p>
                </div>
            @elseif(Session::get('status') == 'err')
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-check"></i> Gagal!</h3>
                    <p class="mb-0">{{ Session::get('msg') }}</p>
                </div>
            @endif
        @endif

        <h2 class="content-heading">
            <a class="btn btn-sm btn-warning float-right" href="{{ url('about/create') }}">
                <i class="fa fa-plus"></i> &nbsp; Tambah Tentang Baru
            </a>
            Tentang
        </h2>

        <!-- Table -->
        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">Table</h3>
            </div>
            <div class="block-content">
                <div class="table-responsive">
                    <table class="table table-vcenter table-stripped">
                        <thead>
                            <tr>
                                <th style="width: 50px;">No</th>
                                <th>Judul</th>
                                <th>Tanggal</th>
                                <th>Deskripsi</th>
                                <th style="width: 200px;"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($about) > 0)
                                @foreach($about as $key => $result)
                                    <tr>
                                        <td>{{ ($about->currentpage()-1) * $about->perpage() + $key + 1 }}</td>
                                        <td>{{ $result->title }}</td>
                                        <td>{{ date('d F Y H:i:s', strtotime($result->created_at)) }}</td>
                                        <td>{{ substr($result->description, 0, 100) }} ...</td>
                                        <td>
                                            <a href="{{ url('about/edit/'. $result->id) }}" class="btn btn-sm btn-primary" data-toggle="tooltip" title="Ubah">
                                                <i class="fa fa-pencil"></i>
                                            </a>
                                            <button  data-toggle="tooltip" title="Hapus" data-action="{{ url('about/delete/'. $result->id) }}" class="btn btn-sm btn-danger btn-xs btn-delete">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach
    						@else
    							<tr>
    								<td colspan='5'>Data Tidak Tersedia</td>
    							</tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                {{ $about->links() }}
            </div>
        </div>
        <!-- END Table -->
    </div>


</div>

@include('modal_delete')
@endsection

@push('script')
    <script type="text/javascript">
        $('.btn-delete').click(function(){
        	var action = $(this).data('action');

        	$('#modalDelete').modal('show');
        	$('.modal-title').html('Delete Tentang');

            $('#formDelete').attr('action',action);
            $('#formDelete').append("<input type='hidden' name='_method' value='DELETE'>");
        });
    </script>
@endpush
