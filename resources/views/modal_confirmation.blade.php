<div class="modal fade" id="modalConfirmation" tabindex="-1" role="dialog" aria-labelledby="modal-fadein" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-popin hide" role="document" aria-hidden="true">
        <form id="formConfirm" class="form">
            <div class="modal-content">
                <div class="block block-themed block-transparent mb-0">
                    <div class="block-header bg-primary-dark">
                        <h3 id="modal-confirmation-title" class="block-title">Ubah Qty</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="si si-close"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content" id="bodyModalConfirm">
                        @csrf
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Konfirmasi</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                </div>
            </div>
        </form>
    </div>
</div>
