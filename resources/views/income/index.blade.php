@extends('layouts.root')

@section('title','Income')

@section('content')
<div class="container">
    <div class="content">
        <nav class="breadcrumb bg-white push">
            <a class="breadcrumb-item" href="javascript:void(0)">Dashboard</a>
            <span class="breadcrumb-item active">Income</span>
        </nav>

        @if ($errors->any())
    	    <div class="alert alert-danger m-t-20">
    	        <ul>
    	            @foreach ($errors->all() as $error)
    	                <li>{{ $error }}</li>
    	            @endforeach
    	        </ul>
    	    </div>
    	@endif

    	@if(Session::has('status'))
            @if(Session::get('status') == '200')
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-check"></i> Berhasil!</h3>
                    <p class="mb-0">{{ Session::get('msg') }}</p>
                </div>
            @elseif(Session::get('status') == 'err')
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-check"></i> Gagal!</h3>
                    <p class="mb-0">{{ Session::get('msg') }}</p>
                </div>
            @endif
        @endif

        <h2 class="content-heading">
            Income
        </h2>

        <!-- filter -->
        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">Pencarian</h3>
            </div>
            <div class="block-content">
                <form action="{{ url('income') }}" class="form-inline" method="GET">
                    <div class="form-group" style="margin-bottom: 10px; width: 25%">
                        <label for="">Tanggal Mulai</label>
                        <input type="text" autocomplete="off" class="form-control js-datepicker" value="{{ Request::get('start') != "" ? Request::get('start') : date('d-m-Y') }}" id="start" data-week-start="1" data-autoclose="true" data-today-highlight="true" data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy" name="start" style="width: 100%" placeholder="Pilih tanggal mulai">
                    </div>
                    <div class="form-group" style="margin-left:10px; margin-bottom: 10px; width: 25%">
                        <label for="">Tanggal Selesai</label>
                        <input type="text" autocomplete="off" class="form-control js-datepicker" value="{{ Request::get('finish') != "" ? Request::get('finish') : date('d-m-Y') }}" id="finish" data-week-start="1" data-autoclose="true" data-today-highlight="true" data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy" name="finish" style="width: 100%" placeholder="Pilih tanggal selesai">
                    </div>
                    <div class="form-group" style="margin-left:10px; margin-bottom: 10px; width: 25%">
                        <label for="">Agen</label>
                        <select name="mitra_id" id="mitra_id" class="form-control js-select2" style="width: 100%">
                            <option value="">Semua Agen</option>
                            @foreach($mitra as $opt)
                                @if($opt->id == Request::get("mitra_id"))
                                    <option value="{{ $opt->id }}" selected>{{ $opt->name }}</option>
                                @else
                                    <option value="{{ $opt->id }}">{{ $opt->name }}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group" style="margin:10px 0 0 5px; width: 21.5%">
                        <button class="btn btn-warning" style="width: 100%"> <i class="fa fa-search"></i> Cari</button>
                    </div>
                </form> <br>
            </div>
        </div>

        <!-- Table -->
        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">Data Pendapatan : {{ Request::get('start') == null ? date('d M Y') : date_format(date_create(Request::get('start')), 'd M Y') }} s/d {{ Request::get('finish') == null ? date('d M Y') : date_format(date_create(Request::get('finish')), 'd M Y') }}</h3>
            </div>
            <div class="block-content">
                <div class="table-responsive">
                    <table class="table table-vcenter table-stripped">
                        <thead>
                            <tr>
                                <th style="width: 50px;">No</th>
                                <th>Agen</th>
                                <th>Pendapatan</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($income) > 0)
                                @foreach($income as $key => $result)
                                    <tr>
                                        <td>{{ ($income->currentpage()-1) * $income->perpage() + $key + 1 }}</td>
                                        <td>{{ strtoupper($result->mitra) }}</td>
                                        <td>Rp. {{ number_format($result->income) }}</td>
                                    </tr>
                                @endforeach
    						@else
    							<tr>
    								<td colspan='4'>Data Tidak Tersedia</td>
    							</tr>
                            @endif
                        </tbody>
                        <tfoot>
                            <tr>
                                <th colspan="2" class="text-right">Total</th>
                                <th>Rp. {{ number_format($total->income) }}</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                {{ $income->appends([ 'start' => Request::get('start'),'finish' => Request::get('finish'),'mitra_id' => Request::get('mitra_id')])->links() }}
            </div>
        </div>
        <!-- END Table -->
    </div>


</div>

@include('modal_delete')
@include('modal_cancel')
@endsection

@push('script')
    <!-- DataTables -->
    <script src="{{ asset('themes/plugins/datatables/jquery.dataTables.min.js') }} "></script>
    <script src="{{ asset('themes/plugins/datatables/dataTables.bootstrap.js') }} "></script>
    <script src="{{ asset('assets/js/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/jquery-validation/additional-methods.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
    <script>
        jQuery(function () {
            // Init page helpers (Select2 plugin)
            Codebase.helpers(['datepicker']);
        });
    </script>
@endpush
