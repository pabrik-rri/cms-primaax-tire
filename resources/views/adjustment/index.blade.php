@extends('layouts.root')

@section('title','Stock Opname')

@section('content')
<div class="container">
    <div class="content">
        <nav class="breadcrumb bg-white push">
            <a class="breadcrumb-item" href="javascript:void(0)">Dashboard</a>
            <span class="breadcrumb-item active">Stock Opname</span>
        </nav>

        @if ($errors->any())
    	    <div class="alert alert-danger m-t-20">
    	        <ul>
    	            @foreach ($errors->all() as $error)
    	                <li>{{ $error }}</li>
    	            @endforeach
    	        </ul>
    	    </div>
    	@endif

    	@if(Session::has('status'))
            @if(Session::get('status') == '200')
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-check"></i> Berhasil!</h3>
                    <p class="mb-0">{{ Session::get('msg') }}</p>
                </div>
            @elseif(Session::get('status') == 'err')
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-check"></i> Gagal!</h3>
                    <p class="mb-0">{{ Session::get('msg') }}</p>
                </div>
            @endif
        @endif

        <h2 class="content-heading">
            Stock Opname
        </h2>

        <!-- filter -->
        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">Pencarian</h3>
            </div>
            <div class="block-content">
                <form action="{{ url('adjustment') }}" class="form-inline" method="GET">
                    <div class="form-group" style="margin-left:10px; margin-bottom: 10px; width: 75%">
                        <label for="">Agen</label>
                        <select name="mitra_id" id="mitra_id" class="form-control js-select2" style="width: 100%">
                            <option value="">Semua Agen</option>
                            @foreach($mitras as $opt)
                                @if($opt->id == Request::get("mitra_id"))
                                    <option value="{{ $opt->id }}" selected>{{ ucfirst($opt->name) }}</option>
                                @else
                                    <option value="{{ $opt->id }}">{{ ucfirst($opt->name) }}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group" style="margin:10px 0 0 5px; width: 21.5%">
                        <button class="btn btn-warning" style="width: 100%"> <i class="fa fa-search"></i> Cari</button>
                    </div>
                </form> <br>
            </div>
        </div>

        <!-- Table -->
        @for($i = 0; $i < sizeof($data); $i++)
            <div class="block">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Agen - <strong>{{ ucfirst($data[$i]['mitras']['name']) }}</strong></h3>
                </div>
                <div class="block-content">
                    <div class="table-responsive">
                        <table class="table table-vcenter table-stripped">
                            <thead>
                                <tr>
                                    <th>Kategori Produk</th>
                                    <th>Produk</th>
                                    <th>Price</th>
                                    <th>Min Qty</th>
                                    <th>Qty</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @for($j = 0; $j < sizeof($data[$i]['product']); $j++)
                                    @for($k = 0; $k < sizeof($data[$i]['product'][$j]['products']); $k++)
                                        <tr>                                                
                                            <td><strong><i> ~ {{ $data[$i]['product'][$j]['categories'] }} ~</i></strong></td>
                                            <td>{{ $data[$i]['product'][$j]['products'][$k]['name'] }}</td>
                                            <td>Rp. {{ number_format($data[$i]['product'][$j]['products'][$k]['price']) }}</td>
                                            <td>{{ $data[$i]['product'][$j]['products'][$k]['min_qty'] }}</td>
                                            <td>{{ $data[$i]['product'][$j]['products'][$k]['qty'] }}</td>
                                            <td>
                                                <button data-href="{{ url('adjustment/history/'.$data[$i]['product'][$j]['products'][$k]['id']) }}" data-id="{{ $data[$i]['product'][$j]['products'][$k]['id'] }}" class="btn btn-sm btn-danger btn-history" data-toggle="tooltip" title="History Adjustment">
                                                    <i class="fa fa-search"></i>
                                                </button>
                                                <button data-href="{{ url('adjustment/update/'.$data[$i]['product'][$j]['products'][$k]['id']) }}" data-id="{{ $data[$i]['product'][$j]['products'][$k]['id'] }}" class="btn btn-sm btn-primary btn-adjustment" data-toggle="tooltip" title="Adjustment">
                                                    <i class="fa fa-pencil"></i>
                                                </button>
                                            </td>
                                        </tr>
                                    @endfor  
                                @endfor  
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        @endfor
        <!-- END Table -->
    </div>


</div>

@include('modal_delete')
@include('modal_cancel')
@include('modal_adjustment')
@include('modal_history')
@endsection

@push('script')
    <!-- DataTables -->
    <script src="{{ asset('themes/plugins/datatables/jquery.dataTables.min.js') }} "></script>
    <script src="{{ asset('themes/plugins/datatables/dataTables.bootstrap.js') }} "></script>
    <script src="{{ asset('assets/js/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/jquery-validation/additional-methods.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
    <script>
        jQuery(function () {
            // Init page helpers (Select2 plugin)
            Codebase.helpers(['datepicker']);
        });
    </script>

    <script>
        $('.btn-adjustment').click(function(){
            var action = $(this).data('href');
            
            //get qty old
            var id = $(this).data('id');
            var url = '{{ url("adjustment/getQty/") }}' + '/' + id;

            $.get(url, function(data){
                console.log(data);

                //add value
                $('#qty_old').val(data.qty);
            });

        	$('#modalAdjustment').modal('show');
            $('#formAdjustment').attr('action',action);
        });

        $('.btn-history').click(function(){
            var action = $(this).data('href');

            $.get(action, function(data){
                //add value
                console.log(data);
                $('#bodyAdjustment').html(data);
            });

        	$('#modalHistory').modal('show');
        });
    </script>
@endpush
