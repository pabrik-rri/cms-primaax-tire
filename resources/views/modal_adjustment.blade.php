<div class="modal fade" id="modalAdjustment" tabindex="-1" role="dialog" aria-labelledby="modal-fadein" style="display: none;" aria-hidden="true">
    <div class="modal-dialog hide" role="document" aria-hidden="true">
        <form id="formAdjustment" method="POST">
            <div class="modal-content">
                <div class="block block-themed block-transparent mb-0">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title">Form Adjustment</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="si si-close"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content">
                        @csrf
                        <div class="form-group">
                            <label for="" class="control-label">Qty (Sekarang)</label>
                            <input type="text" name="qty_old" id="qty_old" class="form-control" readonly>
                        </div>
                        <div class="form-group">
                            <label for="" class="control-label">Perubahan QTY <strong class="text-danger">*</strong></label>
                            <input type="text" class="form-control" name="qty" id="qty" placeholder="Masukan QTY perubahan" autofocus required>
                        </div>
                        <div class="form-group">
                            <label for="" class="control-label">Alasan merubah QTY <strong class="text-danger">*</strong></label>
                            <textarea name="information" id="information" cols="30" rows="10" class="form-control" placeholder="Masukan Alasan" required></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary btn-modal-delete">Simpan</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                </div>
            </div>
        </form>
    </div>
</div>
