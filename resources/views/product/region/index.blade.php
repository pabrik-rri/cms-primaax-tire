@extends('layouts.root')

@section('title','Harga / Region')

@section('content')
<div class="container">
    <div class="content">
        <nav class="breadcrumb bg-white push">
            <a class="breadcrumb-item" href="javascript:void(0)">Dashboard</a>
            <span class="breadcrumb-item active">Harga / Region</span>
        </nav>

        @if ($errors->any())
    	    <div class="alert alert-danger m-t-20">
    	        <ul>
    	            @foreach ($errors->all() as $error)
    	                <li>{{ $error }}</li>
    	            @endforeach
    	        </ul>
    	    </div>
    	@endif

    	@if(Session::has('status'))
            @if(Session::get('status') == '200')
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-check"></i> Berhasil!</h3>
                    <p class="mb-0">{{ Session::get('msg') }}</p>
                </div>
            @elseif(Session::get('status') == 'err')
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-check"></i> Gagal!</h3>
                    <p class="mb-0">{{ Session::get('msg') }}</p>
                </div>
            @endif
        @endif

        <h2 class="content-heading">
            <a class="btn btn-sm btn-warning float-right" href="{{ url('product_region/create') }}">
                <i class="fa fa-plus"></i> &nbsp; Tambah Harga / Region Baru
            </a>
            Harga / Region
        </h2>

        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">Filter</h3>
            </div>
            <div class="block-content">
                <form action="">
                    <div class="row">
                        <div class="form-group col-sm-5">
                            <div class="form-material">
                                <select class="js-select2 form-control" id="province" name="province" data-placeholder="Pilih Propinsi">
                                    <option></option>
                                    @foreach($provinces as $province)
                                        <option value="{{ $province->id }}" @if(Request::get('province') == $province->id) selected @endif >{{ $province->province }}</option>
                                    @endforeach
                                </select>
                                <label for="province">Propinsi</label>
                            </div>
                        </div>
                        <div class="form-group col-sm-5">
                            <div class="form-material">
                                <select class="js-select2 form-control" disabled id="city_id" name="city_id" data-placeholder="Pilih Kota">
                                    <option></option>
                                    @foreach($cities as $city)
                                        <option value="{{ $city->id }}" @if(Request::get('city_id') == $city->id) selected @endif >{{ $city->city }}</option>
                                    @endforeach
                                </select>
                                <label for="city_id">Kota</label>
                            </div>
                        </div>
                        <div class="form-group col-sm-2">
                            <button class="btn btn-success btn-block" style="margin-top: 20px;"><i class="fa fa-search"></i> Cari</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <!-- Table -->
        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">Table</h3>
            </div>
            <div class="block-content">
                <div class="table-responsive">
                    <table class="table table-vcenter table-stripped">
                        <thead>
                            <tr>
                                <th style="width: 50px;">No</th>
                                <th>Produk</th>
                                <th>Kota</th>
                                <th>Harga</th>
                                <th>Harga (Warehouse)</th>
                                <th style="width: 200px;"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($region) > 0)
                                @foreach($region as $key => $result)
                                    <tr>
                                        <td>{{ ($region->currentpage()-1) * $region->perpage() + $key + 1 }}</td>
                                        <td>{{ $result->Product->name }}</td>
                                        <td>{{ $result->City->city }}</td>
                                        <td>Rp. {{ number_format($result->price) }}</td>
                                        <td>Rp. {{ number_format($result->price_warehouse) }}</td>
                                        <td>
                                            <a href="{{ url('product_region/edit/'. $result->id) }}" class="btn btn-sm btn-primary" data-toggle="tooltip" title="Ubah">
                                                <i class="fa fa-pencil"></i>
                                            </a>
                                            <button  data-toggle="tooltip" title="Hapus" data-action="{{ url('product_region/delete/'. $result->id) }}" class="btn btn-sm btn-danger btn-xs btn-delete">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach
    						@else
    							<tr>
    								<td colspan='4'>Data Tidak Tersedia</td>
    							</tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                {{ $region->appends([ 'city_id' => Request::get('city_id')])->links() }}
            </div>
        </div>
        <!-- END Table -->
    </div>


</div>

@include('modal_delete')
@endsection

@push('script')
<script src="{{ asset('assets/js/plugins/select2/select2.full.min.js') }}"></script>
<script src="{{ asset('assets/js/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('assets/js/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
<script src="{{ asset('assets/js/plugins/jquery-validation/additional-methods.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#datatable').dataTable({
        	"pageLength": 50,
        	"lengthChange": false,
        	"searching": false
        });
    } );

    jQuery(function () {
        // Init page helpers (Select2 plugin)
        Codebase.helpers(['select2','datepicker']);

        $('#province').bind('change', function(){
            var url = '{{ url("getCity") }}';
            var province = $(this).val();

            $.get(url, {province : province}, function(data){
                $('#city_id').html(data);
                $('#city_id').attr('disabled', false);
            });
        });
    });

    $('.btn-delete').click(function(){
    	var action = $(this).data('action');

    	$('#modalDelete').modal('show');
    	$('.modal-title').html('Delete Level');

        $('#formDelete').attr('action',action);
        $('#formDelete').append("<input type='hidden' name='_method' value='DELETE'>");
    });
</script>
@endpush
