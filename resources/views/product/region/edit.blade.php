@extends('layouts.root')

@section('title','Ubah Harga / Region')

@section('content')

<!-- Page Content -->
<div class="content">

    <nav class="breadcrumb bg-white push">
        <a class="breadcrumb-item" href="javascript:void(0)">Dashboard</a>
        <a class="breadcrumb-item" href="{{ url('unit') }}">Harga / Region</a>
        <span class="breadcrumb-item active">Ubah</span>
    </nav>
    <!-- Material Design -->
    <div class="row">
        <div class="col-md-12">
            <!-- Static Labels -->
            <div class="block">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Ubah Harga / Region</h3>
                </div>
                <form class="form-level" action="{{ url('product_region/update/'.$region->id) }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="_method" value="PUT">
                    <div class="block-content">
                        <div class="row">
                            <div class="form-group col-sm-12">
                                <div class="form-material">
                                    <select class="js-select2 form-control" id="province" name="province" data-placeholder="Pilih Propinsi">
                                        <option></option>
                                        @foreach($province as $opt)
                                            <option value="{{ $opt->id }}" @if($opt->id == $region->City->province_id) selected @endif>{{ $opt->province }}</option>
                                        @endforeach
                                    </select>
                                    <label for="province">Propinsi</label>
                                </div>
                            </div>
                            <div class="form-group col-sm-12">
                                <div class="form-material">
                                    <select class="js-select2 form-control" id="city_id" name="city_id" data-placeholder="Pilih Kota">
                                        <option></option>
                                        @foreach($city as $opt)
                                            <option value="{{ $opt->id }}" @if($opt->id == $region->city_id) selected @endif>{{ $opt->city }}</option>
                                        @endforeach
                                    </select>
                                    <label for="city_id">Kota <span class="text-danger">*</span></label>
                                </div>
                            </div>
                            <div class="form-group col-sm-12">
                                <div class="form-material">
                                    <select class="js-select2 form-control" id="product_category" name="product_category" data-placeholder="Pilih Kategori Produk">
                                        <option></option>
                                        @foreach($product_category as $opt)
                                            <option value="{{ $opt->id }}" @if($opt->id == $region->Product->product_category_id) selected @endif>{{ $opt->name }}</option>
                                        @endforeach
                                    </select>
                                    <label for="province">Kategori Produk </label>
                                </div>
                            </div>
                            <div class="form-group col-sm-12">
                                <div class="form-material">
                                    <select class="form-control js-select2" id="product_id" name="product_id" data-placeholder="Pilih Produk">
                                        <option>Pilih Produk</option>
                                        @foreach($product as $opt)
                                            <option value="{{ $opt->id }}" @if($opt->id == $region->product_id) selected @endif>{{ $opt->code }} - {{ $opt->name }}</option>
                                        @endforeach
                                    </select>
                                    <label for="city_id">Produk <span class="text-danger">*</span></label>
                                </div>
                            </div>
                            <div class="form-group col-sm-12">
                                <div class="form-material">
                                    <input type="text" class="form-control" name="price" placeholder="Masukan harga region tersebut" value="{{ $region->price }}">
                                    <label for="price">Harga / Region <span class="text-danger">*</span></label>
                                </div>
                            </div>
                            <div class="form-group col-sm-12">
                                <div class="form-material">
                                    <input type="text" class="form-control" name="price_warehouse" placeholder="Masukan harga region (warehouse) tersebut" value="{{ $region->price_warehouse }}">
                                    <label for="price">Harga / Region (Warehouse) <span class="text-danger">*</span></label>
                                </div>
                            </div>
                            <div class="form-group col-sm-1"></div>
                            <div class="form-group col-sm-12">
                                <button type="submit" class="btn btn-warning">Simpan</button>
                                <button type="reset" class="btn btn-secondary">Reset</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <!-- END Static Labels -->
        </div>
        <!-- END column -->
    </div>
    <!-- END row -->
</div>
<!-- END content -->

@endsection

@push('script')

    <script src="{{ asset('assets/js/plugins/select2/select2.full.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/jquery-validation/additional-methods.min.js') }}"></script>
    <script>
        jQuery(function () {
            // Init page helpers (Select2 plugin)
            Codebase.helpers(['select2']);

            $('#province').bind('change', function(){
                var url = '{{ url("getCity") }}';
                var province = $(this).val();

                $.get(url, {province : province}, function(data){
                    $('#city_id').html(data);
                });
            });

            $('#product_category').bind('change', function(){
                var url = '{{ url("getProduct") }}';
                var product_category = $(this).val();

                $.get(url, {product_category : product_category}, function(data){
                    $('#product_id').html(data);
                });
            });
        });

        $('.form-level').validate({
            ignore: [],
            errorClass: 'invalid-feedback animated fadeInDown',
            errorElement: 'div',
            errorPlacement: function(error, e) {
                jQuery(e).parents('.form-group').append(error);
            },
            highlight: function(e) {
                jQuery(e).closest('.form-group').removeClass('is-invalid').addClass('is-invalid');
            },
            success: function(e) {
                jQuery(e).closest('.form-group').removeClass('is-invalid');
                jQuery(e).remove();
            },
            rules: {
                'city_id': 'required',
                'product_id': 'required',
                'price': 'required',
                'price_warehouse': 'required',
            },
            messages: {
                'city_id': 'Kota harus dipilih',
                'product_id': 'Produk harus dipilih',
                'price': 'Harga harus diisi',
                'price_warehouse': 'Harga Warehouse harus diisi',
            }
        });
    </script>
@endpush
