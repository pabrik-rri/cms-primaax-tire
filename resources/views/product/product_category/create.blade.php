@extends('layouts.root')

@section('title','Tambah Product Kategori Baru')

@section('content')

<!-- Page Content -->
<div class="content">

    <nav class="breadcrumb bg-white push">
        <a class="breadcrumb-item" href="javascript:void(0)">Dashboard</a>
        <a class="breadcrumb-item" href="{{ url('product_category') }}">Product Kategori</a>
        <span class="breadcrumb-item active">Tambah</span>
    </nav>
    <!-- Material Design -->
    <div class="row">
        <div class="col-md-12">
            <!-- Static Labels -->
            <div class="block">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Tambah Product Kategori Baru</h3>
                </div>
                <div class="block-content">
                    <form class="form-level" action="{{ url('product_category') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <div class="form-material">
                                <select class="js-select2 form-control" id="category_id" name="category_id" data-placeholder="Pilih Kategori">
                                    <option value="">Pilih Kategori</option>
                                    @foreach($category as $opt)
                                        <option value="{{ $opt->id }}">{{ $opt->name }}</option>
                                    @endforeach
                                </select>
                                <label for="province">Kategory <span class="text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-material">
                                <input type="text" autocomplete="off" class="form-control" id="name" name="name" placeholder="Masukan Nama Product Kategori">
                                <label for="material-email">Produk Kategori <span class="text text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-material">
                                <input type="file" autocomplete="off" class="form-control" id="image" name="image" placeholder="Masukan Nama Produk Kategori">
                                <label for="material-email">Image</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-warning">Simpan</button>
                            <button type="reset" class="btn btn-secondary">Reset</button>
                        </div>
                    </form>
                </div>
            </div>
            <!-- END Static Labels -->
        </div>
        <!-- END column -->
    </div>
    <!-- END row -->
</div>
<!-- END content -->

@endsection

@push('script')

    <script src="{{ asset('assets/js/plugins/select2/select2.full.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/jquery-validation/additional-methods.min.js') }}"></script>
    <script>
        jQuery(function () {
            // Init page helpers (Select2 plugin)
            Codebase.helpers(['select2']);
        });
        $('.form-level').validate({
            ignore: [],
            errorClass: 'invalid-feedback animated fadeInDown',
            errorElement: 'div',
            errorPlacement: function(error, e) {
                jQuery(e).parents('.form-group').append(error);
            },
            highlight: function(e) {
                jQuery(e).closest('.form-group').removeClass('is-invalid').addClass('is-invalid');
            },
            success: function(e) {
                jQuery(e).closest('.form-group').removeClass('is-invalid');
                jQuery(e).remove();
            },
            rules: {
                'category_id' : 'required',
                'name': {
                    required: true,
                    minlength: 3
                }
            },
            messages: {
                'category_id' : 'Kategori harus dipilih',
                'name': {
                    required: 'Nama Produk Kategori harus diisi',
                    minlength: 'Minimal karakter terdiri dari 3 huruf'
                }
            }
        });
    </script>
@endpush
