@extends('layouts.root')

@section('title','Ubah Produk Kategori')

@section('content')

<!-- Page Content -->
<div class="content">

    <nav class="breadcrumb bg-white push">
        <a class="breadcrumb-item" href="javascript:void(0)">Dashboard</a>
        <a class="breadcrumb-item" href="{{ url('product_category') }}">Produk Kategori</a>
        <span class="breadcrumb-item active">Ubah</span>
    </nav>
    <!-- Material Design -->
    <div class="row">
        <div class="col-md-12">
            <!-- Static Labels -->
            <div class="block">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Ubah Produk Kategori</h3>
                </div>
                <div class="block-content">
                    <form class="form-level" action="{{ url('product_category/update/'. $product_category->id) }}" method="post" enctype="multipart/form-data">
                        <input type="hidden" name="_method" value="PUT">
                        @csrf
                        <div class="form-group">
                            <div class="form-material">
                                <select class="js-select2 form-control" id="category_id" name="category_id" data-placeholder="Pilih Kategori">
                                    <option value="">Pilih Kategori</option>
                                    @foreach($category as $opt)
                                        <option value="{{ $opt->id }}" @if($product_category->category_id == $opt->id) selected @endif>{{ $opt->name }}</option>
                                    @endforeach
                                </select>
                                <label for="province">Kategory <span class="text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-material">
                                <input type="text" autocomplete="off" class="form-control" id="name" name="name" value="{{ $product_category->name }}" placeholder="Masukan Nama Product Kategori">
                                <label for="material-email">Produk Kategori <span class="text text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-material">
                                <input type="file" autocomplete="off" class="form-control" id="image" name="image" placeholder="Masukan Nama Produk Kategori">
                                <label for="material-email">Image <span class="text text-danger">(Kosongkan jika tidak ingin mengganti gambar)</span></label>
                            </div>
                            <br>
                            <img src="{{ $path.$product_category->image }}" alt="{{ $product_category->name }}" width="250">
                            <br>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-warning">Simpan</button>
                            <button type="reset" class="btn btn-secondary">Reset</button>
                        </div>
                    </form>
                </div>
            </div>
            <!-- END Static Labels -->
        </div>
        <!-- END column -->
    </div>
    <!-- END row -->
</div>
<!-- END content -->

@endsection

@push('script')

    <script src="{{ asset('assets/js/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/jquery-validation/additional-methods.min.js') }}"></script>
    <script>
        $('.form-level').validate({
            ignore: [],
            errorClass: 'invalid-feedback animated fadeInDown',
            errorElement: 'div',
            errorPlacement: function(error, e) {
                jQuery(e).parents('.form-group').append(error);
            },
            highlight: function(e) {
                jQuery(e).closest('.form-group').removeClass('is-invalid').addClass('is-invalid');
            },
            success: function(e) {
                jQuery(e).closest('.form-group').removeClass('is-invalid');
                jQuery(e).remove();
            },
            rules: {
                'name': {
                    required: true,
                    minlength: 3
                }
            },
            messages: {
                'name': {
                    required: 'Nama Produk Kategori harus diisi',
                    minlength: 'Minimal karakter terdiri dari 3 huruf'
                }
            }
        });
    </script>
@endpush
