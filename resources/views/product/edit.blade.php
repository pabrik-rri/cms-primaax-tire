@extends('layouts.root')

@section('title','Ubah Produk')

@section('content')

<!-- Page Content -->
<div class="content">

    <nav class="breadcrumb bg-white push">
        <a class="breadcrumb-item" href="javascript:void(0)">Dashboard</a>
        <a class="breadcrumb-item" href="{{ url('product') }}">Produk</a>
        <span class="breadcrumb-item active">Ubah</span>
    </nav>
    <!-- Material Design -->
    <div class="row">
        <div class="col-md-12">
            <!-- Static Labels -->
            <div class="block">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Ubah Produk</h3>
                </div>
                <div class="block-content">
                    <form class="form-level" action="{{ url('product/update/'. $product->id) }}" method="post" enctype="multipart/form-data">
                        <input type="hidden" name="_method" value="PUT">
                        @csrf
                        <div class="form-group">
                            <div class="form-material">
                                <select class="js-select2 form-control" id="product_category_id" name="product_category_id" data-placeholder="Pilih Produk">
                                    <option value="">Pilih Produk</option>
                                    @foreach($product_category as $opt)
                                        <option value="{{ $opt->id }}" @if($product->product_category_id == $opt->id) selected @endif >{{ $opt->name }}</option>
                                    @endforeach
                                </select>
                                <label for="province">Kategori Produk <span class="text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-material">
                                <input type="text" autocomplete="off" class="form-control" id="code" name="code" value="{{ $product->code }}" placeholder="Masukan Kode Produk">
                                <label for="material-email">Kode Produk <span class="text text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-material">
                                <input type="text" autocomplete="off" class="form-control" id="name" name="name" value="{{ $product->name }}" placeholder="Masukan Nama Produk">
                                <label for="material-email">Nama Produk <span class="text text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-material">
                                <select class="js-select2 form-control" id="unit_id" name="unit_id" data-placeholder="Pilih Unit">
                                    <option value="">Pilih Unit</option>
                                    @foreach($unit as $opt)
                                        <option value="{{ $opt->id }}" @if($product->unit_id == $opt->id) selected @endif>{{ $opt->name }}</option>
                                    @endforeach
                                </select>
                                <label for="province">Unit <span class="text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-material">
                                <textarea name="description" id="description" cols="30" rows="5" class="form-control">{{ $product->description }}</textarea>
                                <label for="material-email">Deskripsi Produk <span class="text text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-material">
                                <input type="text" autocomplete="off" class="form-control" id="price_market" name="price_market" value="{{ $product->price_market }}" placeholder="Masukan Harga Produk">
                                <label for="material-email">Harga<span class="text text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-material">
                                <input type="text" autocomplete="off" class="form-control" id="price_warehouse" name="price_warehouse" value="{{ $product->price_warehouse }}" placeholder="Masukan Harga Produk Gudang">
                                <label for="material-email">Harga Gudang<span class="text text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-material">
                                <input type="text" autocomplete="off" class="form-control" id="qty" name="qty" placeholder="Masukan Qty" value="{{ $product->qty }}" >
                                <label for="material-email">Qty </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-material">
                                <input type="text" autocomplete="off" class="form-control" id="min_qty" name="min_qty" placeholder="Masukan Minimal Qty" value="{{ $product->min_qty }}" >
                                <label for="material-email">Minimal Qty </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Baru</label>
                            <div class="radio-list">
                                <label class="radio-inline p-0">
                                    <div class="radio radio-info">
                                        <input type="radio" name="is_new" value="1" {{ $product->is_new == 1 ? "checked=checked" : "" }}>
                                        <label>Ya</label>
                                    </div>
                                </label>
                                <label class="radio-inline p-0">
                                    <div class="radio radio-info">
                                        <input type="radio" name="is_new" value="0" {{ $product->is_new == 0 ? "checked=checked" : "" }}>
                                        <label>Bukan</label>
                                    </div>
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-material">
                                <input type="file" autocomplete="off" class="form-control" id="image" name="image" placeholder="Masukan Gambar Produk">
                                <label for="material-email">Image <span class="text text-danger">(Kosongkan jika tidak ingin mengganti gambar)</span></label>
                            </div>
                            <br>
                            <img src="{{ $path.$product->image }}" alt="{{ $product->name }}" width="250">
                            <br>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-warning">Simpan</button>
                            <button type="reset" class="btn btn-secondary">Reset</button>
                        </div>
                    </form>
                </div>
            </div>
            <!-- END Static Labels -->
        </div>
        <!-- END column -->
    </div>
    <!-- END row -->
</div>
<!-- END content -->

@endsection

@push('script')

    <script src="{{ asset('assets/js/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/jquery-validation/additional-methods.min.js') }}"></script>
    <script>
        $('.form-level').validate({
            ignore: [],
            errorClass: 'invalid-feedback animated fadeInDown',
            errorElement: 'div',
            errorPlacement: function(error, e) {
                jQuery(e).parents('.form-group').append(error);
            },
            highlight: function(e) {
                jQuery(e).closest('.form-group').removeClass('is-invalid').addClass('is-invalid');
            },
            success: function(e) {
                jQuery(e).closest('.form-group').removeClass('is-invalid');
                jQuery(e).remove();
            },
            rules: {
                'product_category_id' : 'required',
                'unit_id' : 'required',
                'code' : 'required',
                'price_market' : 'required',
                'price_warehouse' : 'required',
                'description' : 'required',
                'name': {
                    required: true,
                    minlength: 3
                }
            },
            messages: {
                'product_category_id' : 'Kategori Produk harus dipilih',
                'unit_id' : 'Unit harus dipilih',
                'code' : 'Kode Produk harus diisi',
                'price_market' : 'Harga Produk harus diisi',
                'price_warehouse' : 'Harga Produk Gudang harus diisi',
                'description' : 'Deskripsi Produk harus diisi',
                'name': {
                    required: 'Nama Produk Kategori harus diisi',
                    minlength: 'Minimal karakter terdiri dari 3 huruf'
                }
            }
        });
    </script>
@endpush
