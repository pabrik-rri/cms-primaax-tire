@extends('layouts.root')

@section('title','Saldo Awal Piutang')

@section('content')
<div class="container">
    <div class="content">
        <nav class="breadcrumb bg-white push">
            <a class="breadcrumb-item" href="javascript:void(0)">Dashboard</a>
            <span class="breadcrumb-item active">Saldo Awal Piutang</span>
        </nav>

        @if ($errors->any())
            <div class="alert alert-danger m-t-20">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        @if(Session::has('status'))
            @if(Session::get('status') == '200')
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-check"></i> Berhasil!</h3>
                    <p class="mb-0">{{ Session::get('msg') }}</p>
                </div>
            @elseif(Session::get('status') == 'err')
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-check"></i> Gagal!</h3>
                    <p class="mb-0">{{ Session::get('msg') }}</p>
                </div>
            @endif
        @endif

        <h2 class="content-heading">
            <a class="btn btn-sm btn-warning float-right" href="{{ url('receivables/balance/create') }}">
                <i class="fa fa-plus"></i> &nbsp; Tambah Saldo Awal Piutang
            </a>
            Saldo Awal Piutang
        </h2>

        <!-- Filter -->
        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">Pencarian</h3>
            </div>
            <div class="block-content">
                <div class="row">
                    <div class="form-group col-8">
                        <form action="{{ url('receivables/balance') }}" class="form-inline" method="GET">
                        <div class="form-inline">
                        	<div class="row">
                        		<div class="col-6 mb-3 form-inline">
                        			<div class="form-group mb-3">
	                    				<label for="supplier">Nama Supplier</label>
	                    				&nbsp;&nbsp;&nbsp;&nbsp;
	                    				<input id="supplier" type="text" class="form-control js-datepicker" value="{{ \Auth::user()->name }}" disabled>
	                    			</div>
	                    			<div class="form-group">
			                        	<label for="customer">Nama Customer</label>
			                        	&nbsp;&nbsp;
			                            <select class="js-select2 form-control" id="customer" name="customer" data-placeholder="Pilih Customer">
                                            <option value="">Pilih Customer</option>
                                            @foreach($customer as $key)
                                                <option @if(Request::get("customer") == $key->name) selected="selected" @endif value="{{ $key->name }}">{{ $key->name }}</option>
                                            @endforeach
                                        </select>
			                        </div>
                        		</div>
		                        <div class="col-6 text-left">
		                            <button class="btn btn-warning" style="width: 50%"> <i class="fa fa-eye"></i> Lihat</button>
		                        </div>
                        	</div>
                        </div>
                        </form>
                    </div>
                    <div class="form-group col-2">
                        <button class="text-white btn btn-success btn-import" data-toggle="tooltip" data-action="{{ url('receivables/balance/import') }}" style="width: 100%"> <i class="fa fa-cloud-upload"></i> Import</button>
                    </div>
                    <div class="form-group col-2">
                        <form action="{{ url('receivables/balance/export') }}" method="get" id="export">
                        <input type="hidden" name="customer-export">
                        <button class="text-white btn btn-success" style="width: 100%"> <i class="fa fa-file-excel-o"></i> Eksport</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- Table -->
        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">Table</h3>
            </div>
            <div class="block-content">
                <div class="table-responsive">
                    <table class="table table-vcenter table-stripped">
                        <thead>
                            <tr>
                                <th style="width: 50px;">No</th>
                                <th>DO</th>
                                <th>Nama Customer</th>
                                <th>Tanggal Invoice</th>
                                <th>No. Pembelian</th>
                                <th>Nilai Piutang</th>
                                <th>Saldo Piutang</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($balance) > 0)
                                @foreach($balance as $key => $result)
                                    <tr>
                                        <td>{{ ($balance->currentpage()-1) * $balance->perpage() + $key + 1 }}</td>
                                        <td><input data-id="<?php echo $result->id; ?>" type="checkbox" class="form-control" name="do[]"></td>
                                        <td>{{ $result->customer_name }}</td>
                                        <td>{{ date('d-M-Y', strtotime($result->invoice_date)) }}</td>
                                        <td>{{ $result->code }}</td>
                                        <td>Rp. {{ number_format($result->value_of_receivable) }}</td>
                                        <td>Rp. {{ number_format($result->balance_of_receivable) }}</td>
                                        <td>
                                            <button  data-toggle="tooltip" title="Hapus" data-action="{{ url('receivables/balance/delete/'. $result->id) }}" class="btn btn-sm btn-danger btn-xs btn-delete">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td align="center" colspan='7'>Data Tidak Tersedia</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                {{ $balance->links() }}
                <button data-action="{{ url('receivables/balance/multiple-delete') }}" class="multiple-delete-button btn btn-danger">Hapus</button>
                <a href="{{ url('') }}" class="btn btn-danger">Exit</a>
            </div>
        </div>
        <!-- END Table -->
    </div>
</div>

@include('modal_import')
@include('modal_delete')
@endsection

@push('script')
    <!-- DataTables -->
    <script>
        $('#export').submit(function(){
            $('input[name=customer-export').val($('select[name=customer').val());
        });

        $('.btn-import').click(function(){
            var action = $(this).data('action');
            $('#modalImport').modal('show');
            
            $('#formImport').attr('action',action);
            $('.template-excel').attr('href', '{{ asset("assets/excel/receivables-template.xlsx") }}');
            $('#formImport').append("<input type='hidden' name='_method' value='POST'>");
        });

        $('.btn-delete').click(function(){
            var action = $(this).data('action');

            $('#modalDelete').modal('show');
            $('.modal-title').html('Delete Level');

            $('#formDelete').attr('action',action);
            $('#formDelete').append("<input type='hidden' name='_method' value='DELETE'>");
        });

        $('.multiple-delete-button').click(function(){
            var action = $(this).data('action');
            var checkboxes = [];

            $('#modalDelete').modal('show');
            $('.modal-title').html('Delete Level');

            $('#formDelete').attr('action',action);
            $('#formDelete').append("<input type='hidden' name='deleted-id'>");
            $('#formDelete').append("<input type='hidden' name='_method' value='DELETE'>");

            var values = $("input[name='do[]']:checked").map(function(){
                checkboxes.push($(this).data('id'));
            }).get();

            $('input[name=deleted-id]').val(checkboxes);
        });

        $('#multiple-delete-form').submit(function(){
            var checkboxes = [];
            var values = $("input[name='do[]']:checked").map(function(){
                checkboxes.push($(this).data('id'));
            }).get();

            $('input[name=deleted-id]').val(checkboxes);
        });
    </script>
@endpush