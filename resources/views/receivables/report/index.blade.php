@extends('layouts.root')

@section('title','Laporan Piutang')

@section('content')

@php 
    $total_value    = 0; 
    $total_balance  = 0; 
@endphp

<div class="container">
    <div class="content">
        <nav class="breadcrumb bg-white push">
            <a class="breadcrumb-item" href="javascript:void(0)">Dashboard</a>
            <span class="breadcrumb-item active">Laporan Piutang</span>
        </nav>

        @if ($errors->any())
            <div class="alert alert-danger m-t-20">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        @if(Session::has('status'))
            @if(Session::get('status') == '200')
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-check"></i> Berhasil!</h3>
                    <p class="mb-0">{{ Session::get('msg') }}</p>
                </div>
            @elseif(Session::get('status') == 'err')
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-check"></i> Gagal!</h3>
                    <p class="mb-0">{{ Session::get('msg') }}</p>
                </div>
            @endif
        @endif

        <h2 class="content-heading">
            Laporan Piutang
        </h2>

        <!-- Filter -->
        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">Pencarian</h3>
            </div>
            <div class="block-content">
                <div class="row">
                    <div class="form-group col-10">
                        <form action="{{ url('receivables/report') }}" class="form-inline" method="GET">
                            <div class="form-inline mr-3"><label>Periode</label></div>
                            <div class="form-inline mr-3">
                                <input type="text" autocomplete="off" class="form-control js-datepicker" value="{{ Request::get('start') != '' ? Request::get('start') : date('d-m-Y') }}" id="start" data-week-start="1" data-autoclose="true" data-today-highlight="true" data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy" name="start" style="width: 100%" placeholder="Pilih tanggal mulai">
                            </div>
                            <div class="form-inline mr-3"><label>s/d</label></div>
                            <div class="form-inline mr-3">
                                <input type="text" autocomplete="off" class="form-control js-datepicker" value="{{ Request::get('finish') != '' ? Request::get('finish') : date('d-m-Y') }}" id="finish" data-week-start="1" data-autoclose="true" data-today-highlight="true" data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy" name="finish" style="width: 100%" placeholder="Pilih tanggal selesai">
                            </div>
                            <div class="form-inline mr-3"><label>Nama</label></div>
                            <div class="form-inline mr-3">
                                <input type="text" value="{{ Request::get('name') }}" name="name" placeholder="Input nama konsumen" class="form-control">
                            </div>
                            <div class="form-check-inline">
                                <button class="btn btn-warning" style="width: 100%"> <i class="fa fa-search"></i> Cari</button>
                            </div>
                        </form>
                    </div>
                    <div class="form-group col-2">
                        <form action="{{ url('receivables/report/export') }}" method="get" id="export">
                        <input type="hidden" name="customer-export">
                        <input type="hidden" name="start-export">
                        <input type="hidden" name="finish-export">
                        <button class="text-white btn btn-success" style="width: 100%"> <i class="fa fa-file-excel-o"></i> Eksport</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- Table -->
        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">Table</h3>
            </div>
            <div class="block-content">
                <div class="table-responsive">
                    <table class="table table-vcenter table-stripped">
                        <thead>
                            <tr>
                                <th style="width: 50px;">No</th>
                                <th>Nama Customer</th>
                                <th>Tanggal Invoice</th>
                                <th>No. Pembelian</th>
                                <th>Nilai Piutang</th>
                                <th>Saldo Piutang</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($balance) > 0)
                                @foreach($balance as $key => $result)
                                    @php 
                                        $total_value = $total_value + $result->value_of_receivable; 
                                        $total_balance = $total_balance + $result->balance_of_receivable; 
                                    @endphp
                                    <tr>
                                        <td>{{ ($balance->currentpage()-1) * $balance->perpage() + $key + 1 }}</td>
                                        <td>{{ $result->customer_name }}</td>
                                        <td>{{ date('d M Y', strtotime($result->invoice_date)) }}</td>
                                        <td>{{ $result->code }}</td>
                                        <td>Rp. {{ number_format($result->value_of_receivable) }}</td>
                                        <td>Rp. {{ number_format($result->balance_of_receivable) }}</td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td align="center" colspan='7'>Data Tidak Tersedia</td>
                                </tr>
                            @endif
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="4" class="text-right"><strong>Total</strong></td>
                                <td>Rp. {{ number_format($total_value) }}</td>
                                <td>Rp. {{ number_format($total_balance) }}</td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                {{ $balance->links() }}
            </div>
        </div>
        <!-- END Table -->
    </div>
</div>

@include('modal_import')
@include('modal_delete')
@endsection

@push('script')
    <!-- DataTables -->
    <script src="{{ asset('assets/js/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
    <script>
        
        jQuery(function () {
            // Init page helpers (Select2 plugin)
            Codebase.helpers(['datepicker']);
        });

        $('#export').submit(function(){
            $('input[name=customer-export').val($('select[name=customer').val());
            $('input[name=start-export').val($('input[name=start').val());
            $('input[name=finish-export').val($('input[name=finish').val());
        });
    </script>
@endpush