@extends('layouts.root')

@section('title','Tambah Produk Baru')

@section('content')

<!-- Page Content -->
<div class="content">

    <nav class="breadcrumb bg-white push">
        <a class="breadcrumb-item" href="javascript:void(0)">Dashboard</a>
        <a class="breadcrumb-item" href="{{ url('receivables/balance') }}">Saldo Awal Piutang</a>
        <span class="breadcrumb-item active">Tambah</span>
    </nav>
    <!-- Material Design -->
    <div class="row">
        <div class="col-md-12">
            <!-- Static Labels -->
            <div class="block">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Tambah Saldo Awal Piutang</h3>
                </div>
                <div class="block-content">
                    <form class="form-level" action="{{ url('receivables/balance') }}" method="post">
                        @csrf
                        <div class="form-group">
                            <div class="form-material">
                                <input type="text" autocomplete="off" class="form-control js-datepicker" id="date" data-week-start="1" data-autoclose="true" data-today-highlight="true" data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy" name="date" style="width: 100%" placeholder="Pilih Tanggal Transaksi">
                                <label>Tanggal Invoice <span class="text text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-material">
                                <input type="text" autocomplete="off" class="form-control" disabled value="{{ \Auth::user()->name }}">
                                <label>Nama Supplier <span class="text text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-material">
                                <select class="js-select2 form-control" id="customer" name="customer" data-placeholder="Pilih Customer">
                                    <option value="">Pilih Customer</option>
                                    @foreach($customer as $key)
                                        <option value="{{ $key->name }}">{{ $key->name }}</option>
                                    @endforeach
                                </select>
                                <label>Nama Customer <span class="text text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-material">
                                <input type="text" autocomplete="off" class="form-control" id="receivables" name="receivables" placeholder="Masukan Nilai Piutang">
                                <input type="hidden" name="rec_value">
                                <label for="material-email">Nilai Piutang<span class="text text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-material">
                                <input type="text" autocomplete="off" class="form-control" id="balance" name="balance" placeholder="Masukan Saldo Piutang">
                                <label for="material-email">Saldo Piutang<span class="text text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-warning">Simpan</button>
                            <button type="reset" class="btn btn-secondary">Reset</button>
                        </div>
                    </form>
                </div>
            </div>
            <!-- END Static Labels -->
        </div>
        <!-- END column -->
    </div>
    <!-- END row -->
</div>
<!-- END content -->

@endsection

@push('script')

    <script src="{{ asset('assets/js/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/select2/select2.full.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/jquery-validation/additional-methods.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.min.js"></script>
    <script>
        jQuery(function () {
            Codebase.helpers(['datepicker']);
        });

        $(document).ready(function($){
            // Format mata uang.
            $( '#receivables' ).mask('0.000.000.000', {reverse: true});
            $( '#balance' ).mask('0.000.000.000', {reverse: true});
        })

        $('.form-level').validate({
            ignore: [],
            errorClass: 'invalid-feedback animated fadeInDown',
            errorElement: 'div',
            errorPlacement: function(error, e) {
                jQuery(e).parents('.form-group').append(error);
            },
            highlight: function(e) {
                jQuery(e).closest('.form-group').removeClass('is-invalid').addClass('is-invalid');
            },
            success: function(e) {
                jQuery(e).closest('.form-group').removeClass('is-invalid');
                jQuery(e).remove();
            },
            rules: {
                'date' : 'required',
                'customer': 'required',
                'receivables' : 'required',
                'balance' : 'required',
            },
            messages: {
                'date' : 'Tanggal Transaksi harus diisi',
                'name': 'Nama Customer harus diisi',
                'receivables' : 'Nilai Piutang harus diisi',
                'balance' : 'Saldo Piutang harus diisi',
            }
        });
    </script>
@endpush
