@extends('layouts.root')

@section('title','Penerimaan Piutang')

@section('content')
<div class="container">
    <div class="content">
        <nav class="breadcrumb bg-white push">
            <a class="breadcrumb-item" href="javascript:void(0)">Dashboard</a>
            <span class="breadcrumb-item active">Penerimaan Piutang</span>
        </nav>

        @if ($errors->any())
            <div class="alert alert-danger m-t-20">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        @if(Session::has('status'))
            @if(Session::get('status') == '200')
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-check"></i> Berhasil!</h3>
                    <p class="mb-0">{{ Session::get('msg') }}</p>
                </div>
            @elseif(Session::get('status') == 'err')
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-check"></i> Gagal!</h3>
                    <p class="mb-0">{{ Session::get('msg') }}</p>
                </div>
            @endif
        @endif

        <!-- <h2 class="content-heading">
            <a class="btn btn-sm btn-warning float-right" href="{{ url('receivables/receipt/create') }}">
                <i class="fa fa-plus"></i> &nbsp; Tambah Penerimaan Piutang
            </a>
            Penerimaan Piutang
        </h2> -->

        <!-- Filter -->
        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">Pencarian</h3>
            </div>
            <div class="block-content">
                <div class="row">
                    <div class="form-group col-10">
                        <form action="{{ url('receivables/receipt') }}" class="form-inline" method="GET">
                        <div class="form-inline mr-3"><label>Periode</label></div>
                        <div class="form-inline mr-3">
                            <input type="text" autocomplete="off" class="form-control js-datepicker" value="{{ Request::get('start') != '' ? Request::get('start') : date('d-m-Y') }}" id="start" data-week-start="1" data-autoclose="true" data-today-highlight="true" data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy" name="start" style="width: 100%" placeholder="Pilih tanggal mulai">
                        </div>
                        <div class="form-inline mr-3"><label>s/d</label></div>
                        <div class="form-inline mr-3">
                            <input type="text" autocomplete="off" class="form-control js-datepicker" value="{{ Request::get('finish') != '' ? Request::get('finish') : date('d-m-Y') }}" id="finish" data-week-start="1" data-autoclose="true" data-today-highlight="true" data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy" name="finish" style="width: 100%" placeholder="Pilih tanggal selesai">
                        </div>
                        <div class="form-inline mr-3"><label>Nama</label></div>
                        <div class="form-inline mr-3">
                            <input type="text" value="{{ Request::get('name') }}" name="name" placeholder="Input nama konsumen" class="form-control">
                        </div>
                        <div class="form-check-inline">
                            <button class="btn btn-warning" style="width: 100%"> <i class="fa fa-search"></i> Cari</button>
                        </div>
                        </form>
                    </div>
                    <div class="form-group col-2">
                        <form action="{{ url('receivables/receipt/export') }}" method="get" id="export">
                        <input type="hidden" name="customer-export">
                        <input type="hidden" name="start-export">
                        <input type="hidden" name="finish-export">
                        <button class="text-white btn btn-success" style="width: 100%"> <i class="fa fa-file-excel-o"></i> Eksport</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- Table -->
        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">Table</h3>
            </div>
            <div class="block-content">
                <div class="table-responsive">
                    <table class="table table-vcenter table-stripped">
                        <thead>
                            <tr>
                                <th style="width: 50px;">No</th>
                                <th>Nama Konsumen</th>
                                <th>No Pembayaran</th>
                                <th>No Penerimaan</th>
                                <th>Nilai Penerimaan</th>
                                <th><center>Detail</center></th>
                                <th><center>Konfirmasi</center></th>
                                <!-- <th>Aksi</th> -->
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($receipts) > 0)
                                @foreach($receipts as $key => $result)
                                    <tr>
                                        <td>{{ ($receipts->currentpage()-1) * $receipts->perpage() + $key + 1 }}</td>
                                        <td>{{ $result->customer_name }}</td>
                                        <td>{{ $result->code }}</td>
                                        <td>{{ (!empty($result->no_receipt) ? $result->no_receipt : '') }}</td>
                                        <td>Rp. {{ number_format($result->value_of_debt, 0, ',', '.') }}</td>
                                        <td align="center">
                                            @foreach($result->debtdetails as &$val)
                                                @php $val->receivable = $val->receivables;  @endphp
                                            @endforeach
                                            <button 
                                                class="btn btn-info btn-detail"
                                                data-name="{{ $result->customer_name }}"
                                                data-detail="{{ json_encode($result->debtdetails) }}"
                                            >
                                                Detail
                                            </button>
                                        </td>
                                        <td align="center">
                                            @if(!$result->no_receipt)
                                            <button class="btn btn-success btn-confirmation" data-action="{{ url('receivables/receipt/repayment/'. $result->id) }}">Konfirmasi</button>
                                            @endif
                                        </td>
                                        <!-- <td>
                                            @if(!$result->no_receipt)
                                            <a href="{{ url('receivables/receipt/edit/'. $result->id) }}" class="btn btn-sm btn-primary" data-toggle="tooltip" title="Ubah">
                                                <i class="fa fa-pencil"></i>
                                            </a>
                                            @endif
                                        </td> -->
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td align="center" colspan='7'>Data Tidak Tersedia</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                {{ $receipts->links() }}
            </div>
        </div>
        <!-- END Table -->
    </div>


</div>
@include('modal_detail')
@include('modal_confirmation')
@endsection

@push('script')
    <!-- DataTables -->
    <script src="{{ asset('assets/js/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/jquery-priceformat/jquery.priceformat.min.js') }}"></script>
    <script>
        function currencyFormatDE (num) {
            const formatter = new Intl.NumberFormat('de-DE', {
                minimumFractionDigits: 0
            });
            return formatter.format(num);
        }

        $('.btn-detail').click(function(){
            var detail = $(this).data('detail');
            var html = '<table class="table table-bordered table-striped">';
            html    += '<thead>';
            html    += '    <th>Nama Customer</th>';
            html    += '    <th>Tanggal Invoice</th>';
            html    += '    <th>No. Pembelian</th>';
            html    += '    <th>Nilai Piutang</th>';
            html    += '    <th>Saldo Piutang</th>';
            html    += '</thead>';
            $.each(detail, function(index, value){
                html    += '<tbody id="bodyAdjustment">';
                html    += '    <th>'+value.receivable.customer_name+'</th>';
                html    += '    <th>'+value.receivable.invoice_date+'</th>';
                html    += '    <th>'+value.receivable.code+'</th>';
                html    += '    <th>Rp. '+currencyFormatDE(value.receivable.value_of_receivable)+'</th>';
                html    += '    <th>Rp. '+currencyFormatDE(value.balance)+'</th>';
                html    += '</tbody>';
            });
            html    += '</table>';
            $('#modalDetail').modal('show');

            $('#bodyModalDetail').html(html);
        });

        $('.btn-confirmation').click(function(){
            var action = $(this).data('action');

            $('#modalConfirmation').modal('show');
            $('#modal-confirmation-title').html('Konfirmasi');

            $('#formConfirm').attr('action', action);
            $('#formConfirm').append("<input type='hidden' name='_method' value='GET'>");

            var html = '<div class="form-group">';
            html    += '    <label for="" class="control-label">Nilai Penerimaan</label>';
            html    += '    <input type="text" name="receipt_value" style="width:100%" class="form-control priceFormatPerfix">';
            html    += '</div>';

            $('#bodyModalConfirm').html('');
            $('#bodyModalConfirm').append(html);
        });

        $('#export').submit(function(){
            $('input[name=customer-export').val($('input[name=name').val());
            $('input[name=start-export').val($('input[name=start').val());
            $('input[name=finish-export').val($('input[name=finish').val());
        });
        
        jQuery(function () {
            // Init page helpers (Select2 plugin)
            Codebase.helpers(['datepicker']);

            $("body").on("focus", "#modalConfirmation", function(){
                $('.priceFormatPerfix').priceFormat({
                    prefix: 'Rp. ',
                    centsSeparator: '',
                    thousandsSeparator: '.',
                    centsLimit: 0
                });
            });
        });
    </script>
@endpush