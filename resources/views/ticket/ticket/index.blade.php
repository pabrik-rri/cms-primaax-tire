@extends('layouts.root')

@section('title','Laporan Keluhan')

@section('content')
<div class="container">
    <div class="content">
        <nav class="breadcrumb bg-white push">
            <a class="breadcrumb-item" href="javascript:void(0)">Dashboard</a>
            <span class="breadcrumb-item active">Laporan Keluhan</span>
        </nav>

        @if ($errors->any())
            <div class="alert alert-danger m-t-20">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        @if(Session::has('status'))
            @if(Session::get('status') == '200')
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-check"></i> Berhasil!</h3>
                    <p class="mb-0">{{ Session::get('msg') }}</p>
                </div>
            @elseif(Session::get('status') == 'err')
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-check"></i> Gagal!</h3>
                    <p class="mb-0">{{ Session::get('msg') }}</p>
                </div>
            @endif
        @endif

        <!-- Filter -->
        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">Pencarian</h3>
            </div>
            <div class="block-content">
                <div class="row">
                    <div class="form-group col-8">
                        <form action="{{ url('ticket') }}" class="form-inline" method="GET">
                        <div class="form-inline mr-3"><label>Periode</label></div>
                        <div class="form-inline mr-3">
                            <input type="text" autocomplete="off" class="form-control js-datepicker" value="{{ Request::get('start') != '' ? Request::get('start') : date('d-m-Y') }}" id="start" data-week-start="1" data-autoclose="true" data-today-highlight="true" data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy" name="start" style="width: 100%" placeholder="Pilih tanggal mulai">
                        </div>
                        <div class="form-inline mr-3"><label>s/d</label></div>
                        <div class="form-inline mr-3">
                            <input type="text" autocomplete="off" class="form-control js-datepicker" value="{{ Request::get('finish') != '' ? Request::get('finish') : date('d-m-Y') }}" id="finish" data-week-start="1" data-autoclose="true" data-today-highlight="true" data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy" name="finish" style="width: 100%" placeholder="Pilih tanggal selesai">
                        </div>
                        <div class="form-check-inline">
                            <button class="btn btn-warning" style="width: 100%"> <i class="fa fa-search"></i> Cari</button>
                        </div>
                        </form>
                    </div>
                    <div class="form-group col-4">
                        <form action="{{ url('ticket/export') }}" method="get" id="export">
                        <input type="hidden" name="start-export">
                        <input type="hidden" name="finish-export">
                        <button class="text-white btn btn-success" style="width: 100%"> <i class="fa fa-file-excel-o"></i> Eksport</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- Table -->
        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">Table</h3>
            </div>
            <div class="block-content">
                <div class="table-responsive">
                    <table class="table table-vcenter table-stripped">
                        <thead>
                            <tr>
                                <th style="width: 50px;">No</th>
                                <th>Tanggal</th>
                                <th>Nama Konsumen</th>
                                <th>Nama Agen</th>
                                <th>Nama Toko</th>
                                <th>Keluhan</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($ticket) > 0)
                                @foreach($ticket as $key => $result)
                                    <tr>
                                        <td>{{ ($ticket->currentpage()-1) * $ticket->perpage() + $key + 1 }}</td>
                                        <td>{{ date('d-M-Y', strtotime($result->created_at)) }}</td>
                                        <td>{{ $result->Customer->name }}</td>
                                        <td>{{ isset($result->Mitra) ? $result->Mitra->name : ""}}</td>
                                        <td>{{ isset($result->Store) ? $result->Store->name : ""}}</td>
                                        <td>{{ $result->complaint }}</td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td align="center" colspan='6'>Data Tidak Tersedia</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                {{ $ticket->links() }}
            </div>
        </div>
        <!-- END Table -->
    </div>


</div>

@endsection

@push('script')
    <!-- DataTables -->
    <script src="{{ asset('assets/js/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
    <script>
        $('#export').submit(function(){
            $('input[name=start-export').val($('input[name=start').val());
            $('input[name=finish-export').val($('input[name=finish').val());
        });
        
        jQuery(function () {
            // Init page helpers (Select2 plugin)
            Codebase.helpers(['datepicker']);
        });
    </script>
@endpush