<?php

namespace App\Model\Order;

use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
    protected $table = 'order_details';

    protected $fillable = [
    	'order_id',
    	'product_id',
    	'qty',
    	'price',
    	'product_name',
		'note',
		'is_send'
	];

	public function Product()
    {
        return $this->belongsTo('\App\Model\Product\Product', 'product_id', 'id');
    }
}
