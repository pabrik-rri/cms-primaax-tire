<?php

namespace App\Model\Order;

use Illuminate\Database\Eloquent\Model;
use Session;

use DB;

class Order extends Model
{
    protected $table = 'orders';

    protected $fillable = [
    	'order_code',
        'user_id',
    	'mitra_id',
    	'payment',
    	'status_payment',
    	'status',
    	'address',
        'address_note',
        'longitude',
        'latitude',
        'ongkir',
        'voucher_id',
        'courier_id',
        'discount',
        'review',
        'star',
        'delivery_date'
    ];


    public static function getAutoNumber( $type )
    {
        $result = self::orderBy('id', 'desc')->first();

        if ($result) {
            $lastNumber = (int) substr($result->order_code, strlen($result->order_code) - 4, 4);
            $newNumber = $lastNumber + 1;

            if (strlen($newNumber) == 1) {
                $newNumber = '000'.$newNumber;
            } elseif (strlen($newNumber) == 2) {
                $newNumber = '00'.$newNumber;
            } elseif (strlen($newNumber) == 3) {
                $newNumber = '0'.$newNumber;
            } else {
                $newNumber = $newNumber;
            }

            $currMonth = (int)date('m', strtotime($result->order_code));
            $currYear = (int)date('y', strtotime($result->order_code));
            $nowMonth = (int)date('m');
            $nowYear = (int)date('y');

            if ( ($currMonth < $nowMonth && $currYear == $nowYear) || ($currMonth == $nowMonth && $currYear < $nowYear) ) {
                $newNumber = '0001';
            } else {
                $newNumber = $newNumber;
            }

            $newCode = $type.$newNumber;
        } else {
            $newCode = $type.'0001';
        }

        return $newCode;
    }

    public static function getByDistance($lat, $lng, $distance, $except = array())
    {
      $results = \DB::table('stores')
                    ->select([
                        'id',
                        \DB::raw('( 3959 * acos( cos( radians(' . $lat . ') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians(' . $lng . ') ) + sin( radians(' . $lat .') ) * sin( radians(latitude) ) ) ) AS distance')
                    ])
                    ->where('is_courier', 0)
                    ->where('status', 1)
                    ->having('distance','<',$distance)
                    ->orderBy('distance')->limit(1);


      if(sizeof($except) > 0)
      {
          $results = $results->whereNotIn('id', $except);

          app('session')->put('mitra_id', $except);
      }

      $data = $results->get();

      if(sizeof($data) > 0)
      {
        return $data[0]->id;
      }

      return false;
    }

    public function getAll($limit = '', $params, $history)
    {
        $result = Order::select(
                    'orders.id',
                    'orders.order_code',
                    'orders.created_at as order_date',
                    'orders.address as order_address',
                    'orders.address_note as address_note',
                    'orders.latitude as latitude',
                    'orders.longitude as longitude',
                    'orders.star as stars',
                    'orders.review as reviews',
                    'orders.note as note',
                    'orders.discount as discount',
                    'orders.ongkir as ongkir',
                    'orders.status as status',
                    \DB::raw('
                      CASE WHEN orders.status = \'0\' THEN "Pending"
                      WHEN orders.status = \'1\' THEN "Diterima"
                      WHEN orders.status = \'2\' THEN "Persiapan Pengiriman"
                      WHEN orders.status = \'3\' THEN "Dalam Pengiriman"
                      WHEN orders.status = \'4\' THEN "Batal"
                      WHEN orders.status = \'5\' THEN "Selesai"
                      WHEN orders.status = \'6\' THEN "Pindahan" ELSE 0 END as order_status'),
                    \DB::raw('SUM(order_details.price * order_details.qty) - orders.discount as total_price'),
                    'mitra.name as mitra',
                    'customer.name as customer',
                    'customer.referral_code as customer_referral',
                    'courier.name as courier'
                )
                ->join('stores as mitra', 'orders.mitra_id','=','mitra.id')
                ->join('customers as customer', 'orders.user_id','=','customer.id')
                ->leftJoin('stores as courier', 'orders.courier_id','=','courier.id')
                ->join('order_details','orders.id', '=' ,'order_details.order_id')
                ->join('products','order_details.product_id', '=' ,'products.id')
                ->orderBy('orders.created_at','DESC')
                ->groupBy('orders.id');

            //filter
            if(isset($params['start']) && isset($params['finish'])) {
                $start = date_format(date_create($params['start']), 'Y-m-d');
                $finish = date_format(date_create($params['finish']), 'Y-m-d');

                $result->whereBetween(\DB::raw('DATE(orders.created_at)'),[$start, $finish]);
            }

            if(isset($params['status'])) {
                $result->where('orders.status', $params['status']);
            }

            if($history == 1) {
                $result->whereIn('orders.status', [4,5]);
            } else {
                $result->whereNotIn('orders.status', [4,5]);
            }

            //search
            if(isset($params['key']) && isset($params['value'])) {
                $result->where($params['key'], 'like','%'. $params['value'] .'%');
            }

        if($limit == "") {
            return $result->get();
        }

        return $result->paginate($limit);
    }

    public function getPO($limit = '', $params)
    {
        $result = Order::select(
                    'orders.id',
                    'orders.order_code',
                    'orders.created_at as order_date',
                    'orders.address as order_address',
                    'orders.address_note as address_note',
                    'orders.latitude as latitude',
                    'orders.longitude as longitude',
                    'orders.star as stars',
                    'orders.review as reviews',
                    'orders.note as note',
                    'orders.discount as discount',
                    'orders.ongkir as ongkir',
                    'orders.status as status',
                    \DB::raw('
                      CASE WHEN orders.status = \'0\' THEN "Pending"
                      WHEN orders.status = \'1\' THEN "Diterima"
                      WHEN orders.status = \'2\' THEN "Persiapan Pengiriman"
                      WHEN orders.status = \'3\' THEN "Dalam Pengiriman"
                      WHEN orders.status = \'4\' THEN "Batal"
                      WHEN orders.status = \'5\' THEN "Selesai"
                      WHEN orders.status = \'6\' THEN "Pindahan" ELSE 0 END as order_status'),
                    \DB::raw('SUM(order_details.price * order_details.qty) - orders.discount as total_price'),
                    'mitra.name as mitra'
                )
                ->join('mitras as mitra', 'orders.mitra_id','=','mitra.id')
                ->join('order_details','orders.id', '=' ,'order_details.order_id')
                ->join('products','order_details.product_id', '=' ,'products.id')
                ->orderBy('orders.created_at','DESC')
                ->where('is_po', 1)
                ->groupBy('orders.id');

            //filter
            if(isset($params['start']) && isset($params['finish'])) {
                $start = date_format(date_create($params['start']), 'Y-m-d');
                $finish = date_format(date_create($params['finish']), 'Y-m-d');

                $result->whereBetween(\DB::raw('DATE(orders.created_at)'),[$start, $finish]);
            }

            if(isset($params['status'])) {
                $result->where('orders.status', $params['status']);
            }

            //search
            if(isset($params['key']) && isset($params['value'])) {
                $result->where($params['key'], 'like','%'. $params['value'] .'%');
            }

        if($limit == "") {
            return $result->get();
        }

        return $result->paginate($limit);
    }

    public function getDataHome($id, $role = '')
    {
        $cond   = "mitra_id";

        if($role = "customer") { $cond = "user_id"; }

        $results = Order::select([
                        '*',
                        DB::raw('SUM(order_details.price * order_details.qty) as total_price')
                    ])
                    ->whereNotIn('status', [4,5])
                    ->where($cond, $id)
                    ->join('order_details','orders.id', '=' ,'order_details.order_id')
                    ->join('products','order_details.product_id', '=' ,'products.id')
                    ->orderBy('orders.created_at', 'DESC')
                    ->groupBy('orders.id');

        return $results->get();
    }

    public function getIncomes($id, $start, $finish)
    {
        $results = Order::select([
                        '*',
                        'orders.created_at as order_date',
                        DB::raw('SUM(order_details.price * order_details.qty) as subtotal')
                    ])
                    ->where('status',5)
                    ->where('mitra_id', $id)
                    ->whereBetween(DB::raw('DATE(orders.created_at)'), [$start, $finish])
                    ->join('order_details','orders.id', '=' ,'order_details.order_id')
                    ->join('products','order_details.product_id', '=' ,'products.id')
                    ->orderBy('orders.created_at', 'DESC')
                    ->groupBy('orders.id');

        $total = Order::select([
                        DB::raw('SUM(order_details.price * order_details.qty) as total')
                    ])
                    ->where('status',5)
                    ->where('mitra_id', $id)
                    ->whereBetween(DB::raw('DATE(orders.created_at)'), [$start, $finish])
                    ->join('order_details','orders.id', '=' ,'order_details.order_id')
                    ->join('products','order_details.product_id', '=' ,'products.id')
                    ->orderBy('orders.created_at', 'DESC');

        $data['data'] = $results->get();
        $data['total'] = $total->first();

        return $data;
    }

    public function getDataOrder($id, $role = '')
    {
        $cond   = "mitra_id";

        if($role = "customer") { $cond = "user_id"; }

        $results = Order::select([
                        '*',
                        DB::raw('SUM(order_details.price * order_details.qty) as total_price')
                    ])
                    ->whereNotIn('status', [4,5])
                    ->where($cond, $id)
                    ->join('order_details','orders.id', '=' ,'order_details.order_id')
                    ->join('products','order_details.product_id', '=' ,'products.id')
                    ->orderBy('orders.created_at', 'DESC')
                    ->groupBy('orders.id');

        return $results->get();
    }

    public function getDataHistory($id, $role = '')
    {
        $cond   = "mitra_id";

        if($role = "customer") { $cond = "user_id"; }

        $results = Order::select([
                        '*',
                        DB::raw('SUM(order_details.price * order_details.qty) as total_price')
                    ])
                    ->whereIn('status', [4,5])
                    ->where($cond, $id)
                    ->join('order_details','orders.id', '=' ,'order_details.order_id')
                    ->join('products','order_details.product_id', '=' ,'products.id')
                    ->orderBy('orders.created_at', 'DESC')
                    ->groupBy('orders.id');

        return $results->get();
    }

    public function getIncome($id)
    {
        return $this->select([
                        \DB::raw('SUM(order_details.price * order_details.qty) as income_today')
                      ])
                      ->join('order_details','orders.id', '=' ,'order_details.order_id')
                      ->where('orders.mitra_id', $id)
                      ->whereDate('orders.created_at','=', date('Y-m-d'))
                      ->orderBy('orders.created_at','DESC')
                      ->groupBy('orders.id')
                      ->first();
    }

    public function getSold($id)
    {
        return $this->select([
                        \DB::raw('SUM(order_details.price * order_details.qty) as income_today')
                      ])
                      ->join('order_details','orders.id', '=' ,'order_details.order_id')
                      ->where('orders.mitra_id', $id)
                      ->whereDate('orders.created_at','=', date('Y-m-d'))
                      ->orderBy('orders.created_at','DESC')
                      ->groupBy('orders.id')
                      ->first();
    }

    public function getIncomeMitra($limit = '', $params)
    {
        $result = $this->select([
                    'mitras.name as mitra',
                    \DB::raw('SUM(order_details.price * order_details.qty) as income')
                ])
                ->join('mitras','orders.mitra_id', '=' ,'mitras.id')
                ->join('order_details','orders.id', '=' ,'order_details.order_id')
                ->where('orders.status', 5)
                ->orderBy('mitras.name','ASC')
                ->groupBy('orders.mitra_id');
        
            $total = $this->select([
                \DB::raw('SUM(order_details.price * order_details.qty) as income')
            ])
                ->join('mitras','orders.mitra_id', '=' ,'mitras.id')
                ->join('order_details','orders.id', '=' ,'order_details.order_id')
                ->where('orders.status', 5)
                ->orderBy('mitras.name','ASC');

            //filter
            if(isset($params['start']) && isset($params['finish'])) {
                $start = date_format(date_create($params['start']), 'Y-m-d');
                $finish = date_format(date_create($params['finish']), 'Y-m-d');

                $result->whereBetween(\DB::raw('DATE(orders.created_at)'),[$start, $finish]);
                $total->whereBetween(\DB::raw('DATE(orders.created_at)'),[$start, $finish]);
            } else {
                $start = date('Y-m-d');
                $finish = date('Y-m-d');

                $result->whereBetween(\DB::raw('DATE(orders.created_at)'),[$start, $finish]);
                $total->whereBetween(\DB::raw('DATE(orders.created_at)'),[$start, $finish]);
            }

            if(isset($params['mitra_id'])) {
                $result->where('orders.mitra_id', $params['mitra_id']); 
                $total->where('orders.mitra_id', $params['mitra_id']); 
            }

        if($limit == "") {
            $data['incomes'] =  $result->get();
        }

        $data['incomes'] = $result->paginate($limit);
        $data['total'] = $total->first();

        return $data;
    }


    public function Product()
    {
        return $this->belongsTo('\App\Model\Product\Product', 'product_id', 'id');
    }

    public function Customer()
    {
        return $this->belongsTo('\App\Model\User\Customer', 'user_id', 'id');
    }

    public function Mitra()
    {
        return $this->belongsTo('\App\Model\User\Mitra', 'mitra_id', 'id');
    }

    public function Courier()
    {
        return $this->belongsTo('\App\Model\User\Mitra', 'courier_id', 'id');
    }

}
