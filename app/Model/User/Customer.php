<?php

namespace App\Model\User;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $table = 'customers';

    protected $fillable = [
    	'name',
    	'no_id_card',
    	'gender',
    	'birth_date',
    	'email',
        'address',
        'longitude',
        'latitude',
    	'city_id',
    	'mobile',
    	'zip_code',
    	'name_store',
    	'password',
    	'status',
        'avatar',
        'token_fcm',
        'referral_code'
    ];

    public function scopeCourier( $query ){
        $data = $query->where('is_courier', 1);
        return $data;
    }
	
	public function City()
    {
        return $this->belongsTo(\App\Model\Master\City::class, 'city_id', 'id');
    }
}
