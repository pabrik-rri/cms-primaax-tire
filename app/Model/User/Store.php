<?php

namespace App\Model\User;

use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
    protected $table = 'stores';

    protected $fillable = [
    	'name',
    	'no_id_card',
    	'gender',
    	'birth_date',
    	'email',
        'address',
        'longitude',
        'latitude',
    	'city_id',
    	'mobile',
    	'zip_code',
    	'name_store',
    	'password',
    	'status',
        'is_courier',
        'mitra_id',
        'avatar',
        'token_fcm'
	];
	
	public function City()
    {
        return $this->belongsTo(\App\Model\Master\City::class, 'city_id', 'id');
    }
}
