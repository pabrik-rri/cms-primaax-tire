<?php

namespace App\Model\Debt;

use Illuminate\Database\Eloquent\Model;

class Debts extends Model
{
    protected $table = 'debts';

    public function DebtDetails()
    {
        return $this->hasMany('\App\Model\Debt\DebtDetails', 'debt_id', 'id');
    }
}