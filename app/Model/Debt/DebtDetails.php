<?php

namespace App\Model\Debt;

use Illuminate\Database\Eloquent\Model;

class DebtDetails extends Model
{
    protected $table = 'debt_details';

    public function Receivables()
    {
        return $this->belongsTo('\App\Model\Receivables\Receivables', 'receivable_id', 'id');
    }
}