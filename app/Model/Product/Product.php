<?php

namespace App\Model\Product;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public $table = 'products';

    protected $primaryKey = 'id';

    protected $dates = ['created_at', 'updated_at'];

    protected $fillable = ['code', 'description', 'min_qty', 'name', 'price_market', 'price_warehouse', 'product_category_id', 'qty', 'status', 'unit_id','is_new'];

    public function ProductCategory()
    {
        return $this->belongsTo( ProductCategory::class );
    }

    public function Unit()
    {
        return $this->belongsTo( Unit::class );
    }
}
