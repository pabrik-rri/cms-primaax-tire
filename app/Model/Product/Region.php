<?php

namespace App\Model\Product;

use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
    public $table = 'product_regions';

    public function Product()
    {
        return $this->belongsTo(\App\Model\Product\Product::class, 'product_id', 'id');
    }

    public function City()
    {
        return $this->belongsTo(\App\Model\Master\City::class, 'city_id', 'id');
    }
}
