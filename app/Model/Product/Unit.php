<?php

namespace App\Model\Product;

use Illuminate\Database\Eloquent\Model;

class Unit extends Model
{
    protected $table = 'units';
}
