<?php

namespace App\Model\Product;

use Illuminate\Database\Eloquent\Model;

use DB;
use App\Model\Product\Category;

class ProductCategory extends Model
{
    public $table = 'product_categories';

    protected $dates = ['created_at', 'updated_at'];

    protected $fillable = ['name', 'status', 'category_id','images'];

    public function Category()
    {
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }
}
