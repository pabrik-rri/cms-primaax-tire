<?php

namespace App\Model\Product;

use Illuminate\Database\Eloquent\Model;

use DB;

class ProductMitra extends Model
{
    public $table = 'product_mitras';

    public function Product()
    {
        return $this->belongsTo(\App\Models\Product::class, 'product_id', 'id');
    }
}
