<?php

namespace App\Model\Cms;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    public $table = 'cms_news';

    public function userCreated()
    {
        return $this->belongsTo('\App\User', 'user_created', 'id');
    }

    public function userUpdated()
    {
        return $this->belongsTo('\App\User', 'user_updated', 'id');
    }
}
