<?php

namespace App\Model\Cms;

use Illuminate\Database\Eloquent\Model;

class Sosmed extends Model
{
    public $table = 'cms_social_medias';
}
