<?php

namespace App\Model\Cms;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    public $table = 'cms_sliders';
}
