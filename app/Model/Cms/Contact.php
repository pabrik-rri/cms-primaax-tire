<?php

namespace App\Model\Cms;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    public $table = 'cms_contacts';

    public function getDataWithPaginate($request = null, $limit = null)
    {
        $contact = Contact::orderBy('created_at','DESC')->paginate($limit);

        return $contact;
    }
}
