<?php

namespace App\Model\Cms;

use Illuminate\Database\Eloquent\Model;

class About extends Model
{
    public $table = 'cms_abouts';
}
