<?php

namespace App\Model\Cms;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    public $table = 'cms_galleries';
}
