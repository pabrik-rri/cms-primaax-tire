<?php

namespace App\Model\Setting;

use Illuminate\Database\Eloquent\Model;

class Closed extends Model
{
    protected $table = 'order_closed';
}
