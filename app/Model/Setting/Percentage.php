<?php

namespace App\Model\Setting;

use Illuminate\Database\Eloquent\Model;

class Percentage extends Model
{
    public $table = 'percentages';
}
