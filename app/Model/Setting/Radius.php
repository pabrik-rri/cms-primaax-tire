<?php

namespace App\Model\Setting;

use Illuminate\Database\Eloquent\Model;

class Radius extends Model
{
    public $table = 'radius';
}
