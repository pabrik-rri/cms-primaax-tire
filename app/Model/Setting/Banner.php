<?php

namespace App\Model\Setting;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    public $table = 'banners';

    protected $fillable = [ 'title', 'image' ];
}
