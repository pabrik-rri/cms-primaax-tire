<?php

namespace App\Model\Returns;

use Illuminate\Database\Eloquent\Model;

class Returns extends Model
{
    protected $table = 'returns';

    public function ReturnDetails()
    {
    	return $this->hasMany('\App\Model\Returns\ReturnDetails', 'return_id', 'id');
    }
}