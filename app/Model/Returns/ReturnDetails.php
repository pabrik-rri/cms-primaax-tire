<?php

namespace App\Model\Returns;

use Illuminate\Database\Eloquent\Model;

class ReturnDetails extends Model
{
    protected $table = 'return_details';

    public function Returns()
    {
        return $this->belongsTo(\App\Model\Returns\Returns::class, 'return_id', 'id');
    }

    public function Product()
    {
    	return $this->belongsTo(\App\Model\Product\Product::class, 'product_id', 'id');	
    }
}