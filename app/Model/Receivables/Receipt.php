<?php

namespace App\Model\Receivables;

use Illuminate\Database\Eloquent\Model;

class Receipt extends Model
{
    protected $table = 'receivable_pays';

    public function Receivables()
    {
        return $this->belongsTo(\App\Model\Receivables\Receivables::class, 'receivable_id', 'id');
    }
}