<?php

namespace App\Model\Ticket;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    protected $table = 'tickets';

    public function Customer()
    {
        return $this->belongsTo(\App\Model\User\Customer::class, 'customer_id', 'id');
    }

    public function Mitra()
    {
        return $this->belongsTo(\App\Model\User\Mitra::class, 'mitra_id', 'id');
    }

    public function Store()
    {
        return $this->belongsTo(\App\Model\User\Store::class, 'store_id', 'id');
    }
}
