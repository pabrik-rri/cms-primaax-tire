<?php

namespace App\Model\Adjustment;

use Illuminate\Database\Eloquent\Model;
use \App\User;

class Adjustment extends Model
{
    protected $table = 'adjustment';

    public function User()
    {
        return $this->belongsTo( User::class );
    }
}
