<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

class Village extends Model
{
    public $table = 'villages';

    public function District()
    {
        return $this->belongsTo(\App\Model\Master\District::class, 'district_id', 'id');
    }
}
