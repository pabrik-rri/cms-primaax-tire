<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

class District extends Model
{
    public $table = 'districts';

    public function City()
    {
        return $this->belongsTo(\App\Model\Master\City::class, 'city_id', 'id');
    }
}
