<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    public $table         = 'provinces';
}
