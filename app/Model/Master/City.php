<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    public $table = 'cities';

    public function Province()
    {
        return $this->belongsTo(\App\Model\Master\Province::class, 'province_id', 'id');
    }
}
