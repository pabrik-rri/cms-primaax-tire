<?php
namespace App\Exports;

use Illuminate\Http\Request;
use DB;

use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\Exportable;

use App\Model\Returns\Returns;

class ReturnsReportExport implements FromQuery, WithHeadings
{
    public function forReturnId(string $return_id = null)
    {
        $this->return_id = $return_id;
        return $this;
    }

    public function forStatus(string $status = null)
    {
        $this->status = $status;
        return $this;
    }

    public function forStart(string $start)
    {
        $this->start = $start;

        return $this;
    }

    public function forFinish(string $finish)
    {
        $this->finish = $finish;

        return $this;
    }

    public function headings(): array
    {
        return [
            'Tanggal',
            'No Retur',
            'Qty',
            'Status'
        ];
    }

    use Exportable;

    public function query()
    {
        $start     = date_format(date_create($this->start), 'Y-m-d');
        $finish    = date_format(date_create($this->finish), 'Y-m-d');

        $result = Returns::query()->select(
            'return_date as `Tanggal`',
            'code as `No Retur`',
            'qty as `Qty`',
            DB::raw('IF(status = 0, "Retur", "Kembali") as `Status`')
        )
        ->orderBy('id', 'desc')
        ->whereBetween(\DB::raw('DATE(return_date)'),[$start, $finish]);

        if ($this->return_id)
            $result->where('code', 'like', '%' . $this->return_id . '%');

        if ($this->status != null)
            $result->where('status', $this->status);

        return $result;
    }
}
?>
