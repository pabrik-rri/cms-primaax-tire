<?php
namespace App\Exports;

use Illuminate\Http\Request;

use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\Exportable;

use App\Model\Ticket\Ticket;

class TicketsExport implements FromQuery, WithHeadings
{
    public function forStart(string $start)
    {
        $this->start = $start;

        return $this;
    }

    public function forFinish(string $finish)
    {
        $this->finish = $finish;

        return $this;
    }

    public function headings(): array
    {
        return [
            'Kode',
            'Tanggal',
            'Nama Konsumen',
            'Nama Agen',
            'Nama Toko',
            'Keluhan'
        ];
    }

    use Exportable;

    public function query()
    {
        $start     = date_format(date_create($this->start), 'Y-m-d');
        $finish    = date_format(date_create($this->finish), 'Y-m-d');

        $result = Ticket::query()->select(
                    'tickets.id as Kode',
                    'tickets.created_at as Tanggal',
                    'customer.name as `Nama Konsumen`',
                    'mitra.name as `Nama Agen`',
                    'store.name as `Nama Toko`',
                    'tickets.complaint as Keluhan'
                )
                ->join('mitras as mitra', 'tickets.mitra_id','=','mitra.id')
                ->join('customers as customer', 'tickets.customer_id','=','customer.id')
                ->join('stores as store','tickets.store_id', '=' ,'store.id')
                ->orderBy('tickets.id')
                ->whereBetween(\DB::raw('DATE(tickets.created_at)'),[$start, $finish]);

        return $result;
    }
}
?>
