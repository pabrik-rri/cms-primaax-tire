<?php
namespace App\Exports;

use Illuminate\Http\Request;
use DB;

use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\Exportable;

use App\Model\Debt\Debts;

class DebtPaymentExport implements FromQuery, WithHeadings
{
    public function forCode(string $code = null)
    {
        $this->code = $code;
        return $this;
    }

    public function forStatus(string $status = null)
    {
        $this->status = $status;
        return $this;
    }

    public function forStart(string $start)
    {
        $this->start = $start;

        return $this;
    }

    public function forFinish(string $finish)
    {
        $this->finish = $finish;

        return $this;
    }

    public function headings(): array
    {
        return [
            'Tanggal',
            'No Pembayaran',
            'Nilai Pembayaran',
            'Status'
        ];
    }

    use Exportable;

    public function query()
    {
        $start     = date_format(date_create($this->start), 'Y-m-d');
        $finish    = date_format(date_create($this->finish), 'Y-m-d');

        $result = Debts::query()->select(
            'debt_date as `Tanggal`',
            'code as `No Pembayaran`',
            'value_of_debt as `Nilai Pembayaran`',
            DB::raw('IF(status = 0, "Pembayaran", "Penerimaan") as `Status`')
        )
        ->where('customer_name', \Auth::user()->name)
        ->orderBy('created_at', 'desc')
        ->whereBetween(\DB::raw('DATE(created_at)'),[$start, $finish]);

        if ($this->code)
            $result->where('code', 'like', '%' . $this->code . '%');

        if ($this->status != null) {
            $result->where('status', $this->status);
        }

        return $result;
    }
}
?>
