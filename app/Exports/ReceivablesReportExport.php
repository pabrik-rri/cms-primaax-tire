<?php
namespace App\Exports;

use Illuminate\Http\Request;

use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\Exportable;

use App\Model\Receivables\Receivables;

class ReceivablesBalanceExport implements FromQuery, WithHeadings
{
    public function forCustomerName(string $name = null)
    {
        $this->customer_name = $name;
        return $this;
    }

    public function forStart(string $start)
    {
        $this->start = $start;

        return $this;
    }

    public function forFinish(string $finish)
    {
        $this->finish = $finish;

        return $this;
    }

    public function headings(): array
    {
        return [
            'Nama Customer',
            'Tanggal Invoice',
            'No Pembelian',
            'Nilai Piutang',
            'Saldo Piutang'
        ];
    }

    use Exportable;

    public function query()
    {
        $start     = date('Y-m-d',strtotime($this->start));
        $finish    = date('Y-m-d',strtotime($this->finish));

        $result = Receivables::query()->select(
            'customer_name',
            'invoice_date',
            'code',
            'value_of_receivable',
            'balance_of_receivable'
        )
        ->orderBy('id')
        ->where('supplier_name', $this->supplier_name)
        ->whereBetween(\DB::raw('DATE(receivables.created_at)'),[$start, $finish]);

        if ($this->customer_name)
            $result->where('customer_name', $this->customer_name);

        return $result;
    }
}
?>
