<?php
namespace App\Exports;

use Illuminate\Http\Request;

use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\Exportable;

use App\Model\Order\Order;

class OrdersExport implements FromQuery, WithHeadings
{
    public function forStart(string $start)
    {
        $this->start = $start;

        return $this;
    }

    public function forFinish(string $finish)
    {
        $this->finish = $finish;

        return $this;
    }

    public function forStatus(string $status)
    {
        $this->status = $status;

        return $this;
    }

    public function headings(): array
    {
        return [
            'Kode',
            'Date',
            'Alamat',
            'Status',
            'Mitra',
            'Customer',
            'Referral Code',
            'Nama Produk',
            'QTY',
            'Rupiah'
        ];
    }

    use Exportable;

    public function query()
    {
        $start     = date_format(date_create($this->start), 'Y-m-d');
        $finish    = date_format(date_create($this->finish), 'Y-m-d');

        $params['status'] = $this->status;

        $result = Order::query()->select(
                    'orders.order_code as Code',
                    'orders.created_at as Date',
                    'orders.address as Alamat',
                    \DB::raw('
                      CASE WHEN orders.status = \'0\' THEN "Pending"
                      WHEN orders.status = \'1\' THEN "Diterima"
                      WHEN orders.status = \'2\' THEN "Persiapan Pengiriman"
                      WHEN orders.status = \'3\' THEN "Dalam Pengiriman"
                      WHEN orders.status = \'4\' THEN "Batal"
                      WHEN orders.status = \'5\' THEN "Selesai" ELSE 0 END as Status'),
                      'mitra.name as Mitra',
                      'customer.name as Customer',
                      \DB::raw('
                        CASE WHEN customer.referral_code = null THEN "Kosong"
                        ELSE customer.referral_code END as Referral_Code'),
                      'products.name as Nama Product',
                      'order_details.qty as QTY',
                    \DB::raw('(order_details.price * order_details.qty) - orders.discount as Rupiah')
                )
                ->join('mitras as mitra', 'orders.mitra_id','=','mitra.id')
                ->join('customers as customer', 'orders.user_id','=','customer.id')
                ->join('order_details','orders.id', '=' ,'order_details.order_id')
                ->join('products','order_details.product_id', '=' ,'products.id')
                ->orderBy('orders.id')
                ->where('orders.is_po', 0)
                ->whereBetween(\DB::raw('DATE(orders.created_at)'),[$start, $finish]);

        if($params['status'] != "") {
            $result->where('orders.status', $params['status']);
        }

        return $result;
    }
}
?>
