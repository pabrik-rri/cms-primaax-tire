<?php

namespace App\Http\Controllers\Order;

use App\Exports\OrdersExport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Order\Order;
use App\Model\Order\OrderDetail;

class HistoryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->limit = 50;
    }

    /**
     * Show the application Module.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $params  = $request->all();
        $history = 1;

        $order  = new Order;

        if(isset($params['_type'])) {

            // $order = $order->getExport($params); return $order; exit;

            return (new OrdersExport)->forStart($params['start'])
                ->forFinish($params['finish'])
                ->forStatus($params['status'] == "" ? "" : $params['status'])
                ->download('transaction-'. date('YmdH:i:s') .'.xlsx');
        }

        $order = $order->getAll($this->limit, $params, $history);
        return view('history.index', compact('order'));
    }

    public function show($id)
    {
        $order     = Order::find($id);
        $detail    = OrderDetail::where('order_id', $id)->get();
        $total     = OrderDetail::select(\DB::raw('SUM(products.price_market * order_details.qty) as total'))->where('order_id', $id)
                        ->join('products','order_details.product_id', '=' ,'products.id')
                        ->pluck('total')->first();

        return view('history.detail', compact('order','detail','total'));
    }

    public function cancel(Request $request, $id)
    {
    	$order	= Order::find($id);
        $order->status = 4;

        $delete = $order->save();

        if ($delete) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Order berhasil dibatalkan');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('status', 'Order gagal dibatalkan');
        }

    	return redirect('order_history');
    }

    public function delete(Request $request, $id)
    {
        \DB::beginTransaction();

            $detail = OrderDetail::where('product_id', $id)->delete();

            $delete = Order::findOrFail($id)->delete();
        
        \DB::commit();

        if ($delete) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Order berhasil dihapus');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('status', 'Order gagal dihapus');
        }

    	return redirect('order_history');
    }
}
