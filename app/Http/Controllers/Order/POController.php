<?php

namespace App\Http\Controllers\Order;

use App\Exports\POExport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Order\Order;
use App\Model\Order\OrderDetail;
use App\Model\Product\ProductMitra;
use App\Model\Product\Product;
use App\Model\User\Mitra;
use App\Model\Vendor\Vendor;

class POController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->limit = 50;
    }

    /**
     * Show the application Module.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $params  = $request->all();

        $order  = new Order;

        if(isset($params['_type'])) {

            // $order = $order->getExport($params); return $order; exit;

            return (new POExport)->forStart($params['start'])
                ->forFinish($params['finish'])
                ->forStatus($params['status'] == "" ? "" : $params['status'])
                ->download('transaction-'. date('YmdH:i:s') .'.xlsx');
        }

        $order = $order->getPO($this->limit, $params);
        return view('po.index', compact('order'));
    }

    public function show($id)
    {
        $order     = Order::find($id);
        $detail    = OrderDetail::where('order_id', $id)->get();
        $total     = OrderDetail::select(\DB::raw('SUM(products.price_warehouse * order_details.qty) as total'))->where('order_id', $id)
                        ->join('products','order_details.product_id', '=' ,'products.id')
                        ->pluck('total')->first();

        $user     = Mitra::find($order->mitra_id);
        
        $mitra_id = array();
        $vendors  = array();

        if($user->vendor_id != null) {
            $mitra_id   = explode("," , $user->vendor_id);
            $vendors    = Vendor::whereIn('id', $mitra_id)->get();
        }

        return view('po.detail', compact('order','detail','total', 'vendors'));
    }

    public function accept(Request $request, $id)
    {
        $order	= Order::find($id);
        $order->status = 1;

        $accept = $order->save();

        if ($accept) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'PO berhasil diterima');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('status', 'PO gagal diterima');
        }

    	return back();
    }

    public function reject(Request $request, $id)
    {
        $order	= Order::find($id);
        $order->status = 4;

        $reject = $order->save();

        if ($reject) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'PO berhasil ditolak');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('status', 'PO gagal ditolak');
        }

    	return back();
    }

    public function send(Request $request, $id)
    {
        $order	= Order::find($id);
        $order->status = 3;

        $send = $order->save();

        if ($send) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'PO berhasil dikirimkan');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('status', 'PO gagal dikirimkan');
        }

    	return back();
    }

    public function finish(Request $request, $id)
    {
        \DB::beginTransaction();

        $order	= Order::find($id);
        $order->status = 5;

        $finish = $order->save();

        if ($finish) {

            $detail = OrderDetail::where('order_id', $id)->get();

            foreach ($detail as $key => $value) {
                $duplicate = ProductMitra::where('mitra_id', $order->mitra_id)->where('product_id', $value->product_id)->first();
                
                //update is_send
                $order_detail                   = OrderDetail::find($value->id);

                if($order_detail->is_send != 1)
                {
                    $product_mitras                 = isset($duplicate) ? ProductMitra::where('mitra_id', $order->mitra_id)->where('product_id', $value->product_id)->first() : new ProductMitra;
                    $product_mitras->product_id     = $value->product_id;
                    $product_mitras->qty            = isset($duplicate) ? $duplicate->qty + $value->qty : $value->qty;
                    $product_mitras->min_qty        = 0;
                    $product_mitras->price          = $value->price;
                    $product_mitras->mitra_id       = $order->mitra_id;
                    $product_mitras->save();
                }

                $order_detail->is_send          = 1;
                $order_detail->save();
            }

            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'PO telah selesai');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('status', 'Proses penyelesaian PO terjadi kendala');
        }

        \DB::commit();
   	
    	return back();
    }

    public function finish_item(Request $request, $id)
    {
        \DB::beginTransaction();

        $detail                         = OrderDetail::find($id);
        $detail->is_send                = 1;
        $detail->save();

        $product                        = Product::find($detail->product_id);

        $duplicate                      = ProductMitra::where('mitra_id', $request->mitra_id)->where('product_id', $detail->product_id)->first();

        $product_mitras                 = isset($duplicate) ? ProductMitra::where('mitra_id', $request->mitra_id)->where('product_id', $detail->product_id)->first() : new ProductMitra;
        $product_mitras->product_id     = $detail->product_id;
        $product_mitras->qty            = isset($duplicate) ? $duplicate->qty + $detail->qty : $detail->qty;
        $product_mitras->min_qty        = 0;
        $product_mitras->price          = $detail->price;
        $product_mitras->mitra_id       = $request->mitra_id;

        if($product_mitras->save()) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'PO dengan produk '. $product->name .' telah selesai');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('status', 'Proses penyelesaian PO dengan produk '. $product->name .' terjadi kendala');
        }

        \DB::commit();

    	return back();
    }

    public function edit_item(Request $request, $id)
    {
        \DB::beginTransaction();

        $detail                         = OrderDetail::find($id);
        $detail->qty                    = $request->qty;
        $detail->save();
        
        $product                        = Product::find($detail->product_id);

        if($detail->save()) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'PO dengan produk '. $product->name .' telah diubah qtynya');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('status', 'Proses pengubahan qty PO dengan produk '. $product->name .' terjadi kendala');
        }

        \DB::commit();

    	return back();
    }

    public function cancel(Request $request, $id)
    {
    	$order	= Order::find($id);
        $order->status = 4;

        $delete = $order->save();

        if ($delete) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'PO berhasil dibatalkan');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('status', 'PO gagal dibatalkan');
        }

    	return redirect('pre_order');
    }

    public function delete(Request $request, $id)
    {
        \DB::beginTransaction();

            $detail = OrderDetail::where('product_id', $id)->delete();

            $delete = Order::findOrFail($id)->delete();
        
        \DB::commit();

        if ($delete) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'PO berhasil dihapus');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('status', 'PO gagal dihapus');
        }

    	return redirect('pre_order');
    }
}
