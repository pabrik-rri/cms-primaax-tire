<?php

namespace App\Http\Controllers\Adjustment;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\User\Store;
use App\Model\Product\Unit;
use App\Model\Product\Product;
use App\Model\Product\ProductMitra;
use App\Model\Product\ProductCategory;
use App\Model\Adjustment\Adjustment;

class StoreController extends Controller
{
    /**
     * Show the application Module.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $mitras = Store::orderBy('name','ASC')->where('is_courier',0)->get();

        try {
            $mitra          = isset($request->mitra_id) ? Store::where('is_courier', 0)->where('id', $request->mitra_id)->orderBy('name','ASC')->get() : Store::where('is_courier', 0)->orderBy('name','ASC')->get();
            $category       = ProductCategory::all();
    		$data = array();
            
            foreach ($mitra as $k_mitra => $v_mitra) {
                $data[$k_mitra]['mitras']['id'] = $v_mitra->id;
                $data[$k_mitra]['mitras']['name'] = $v_mitra->name;
                foreach ($category as $key => $value) {
                    $data[$k_mitra]['product'][$key]['categories'] = $value->name;

                    $product_mitras  = ProductMitra::select([
                                                'product_mitras.id as id',
                                                'products.name as name',
                                                'product_mitras.price as price',
                                                'product_mitras.qty as qty',
                                                'product_mitras.min_qty as min_qty',
                                                'products.product_category_id as product_category_id'
                                            ])
                                            ->join('products','product_mitras.product_id', '=' ,'products.id')
                                            ->join('product_categories','products.product_category_id', '=' ,'product_categories.id')
                                            ->orderBy('products.product_category_id','ASC')
                                            ->where('product_mitras.mitra_id', $v_mitra->id)
                                            ->where('products.product_category_id', $value->id)
                                            ->get();

                    if(sizeof($product_mitras) > 0) {
                        foreach ($product_mitras as $key2 => $product) {
                            $data[$k_mitra]['product'][$key]['products'][$key2]['id']       	= $product->id;
                            $data[$k_mitra]['product'][$key]['products'][$key2]['name']     	= $product->name;
                            $data[$k_mitra]['product'][$key]['products'][$key2]['price']    	= $product->price;
                            $data[$k_mitra]['product'][$key]['products'][$key2]['qty']    		= $product->qty;
                            $data[$k_mitra]['product'][$key]['products'][$key2]['min_qty']    	= $product->min_qty;
                        }
                    } else {
                        $data[$k_mitra]['product'][$key]['products'] = array();
                    }
                }
            }
            // return response()->json( array( 'message' => 'Berhasil mengambil data produk mitra', 'data' => $data ), 200 );
            
            return view('adjustment.store', compact('data','mitras'));
    		
        } catch(\Exception $e) {
            return response()->json( array( 'message' => $e->getMessage().", line : ". $e->getLine()) );
        }
    }

    public function update(Request $request, $id)
    {
        $adjustment                     = new Adjustment;
        $adjustment->product_mitra_id   = $id;
        $adjustment->qty_old            = $request->qty_old;
        $adjustment->qty                = $request->qty;
        $adjustment->information        = $request->information;
        $adjustment->user_id            = \Auth::user()->id;

        if($adjustment->save()){

            $product_mitras = ProductMitra::find($id);
            $product_mitras->qty = $adjustment->qty;
            $product_mitras->save();

            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Adjustment berhasil ditambahkan');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('status', 'Adjustment gagal ditambahkan');
        }

    	return back();
    }

    public function history($id)
    {
        $html    = "";
        $history = Adjustment::where('product_mitra_id',$id)->get();

        if(sizeof($history) > 0) {
            foreach ($history as $key => $value) {
                $html .= "
                    <tr>
                        <td>". ($key+1) ."</td>
                        <td>". $value->qty_old ."</td>
                        <td>". $value->qty ."</td>
                        <td>". $value->information ."</td>
                        <td>". $value->user->name ."</td>
                        <td>". date('d M Y H:i:s', strtotime($value->created_at)) ."</td>
                    </tr>
                ";
            }
        } else {
            $html = "<tr><td colspan='6'>Produk ini tidak memiliki riwayat adjustment</td></tr>";
        }

        return $html;
    }

    public function getQty($id)
    {
        $product_mitras = ProductMitra::find($id);
        return $product_mitras;
    }
}
