<?php

namespace App\Http\Controllers\Returns;

use DB;
use PDF;
use App\Exports\ReturnsReportExport;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Controller;

use App\User;
use App\Model\Returns\Returns;
use App\Model\Returns\ReturnDetails;
use App\Model\Product\Product;

class ReturnsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        $this->limit = 50;
    }

    /**
     * Show the application Module.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data  = Returns::orderBy('id', 'desc');

        if (!empty($request['start'] && !empty($request['finish']))) {
            $start = date_format(date_create($request['start']), 'Y-m-d 00:00:00');
            $finish = date_format(date_create($request['finish']), 'Y-m-d 23:59:59');
            $data->whereBetween('return_date', [$start, $finish]);
        }

        if ($request['status'] != '') {
            $data->where('status', $request['status']);
        }

        if (!empty($request['return_id'])) {
            $data->where('code', 'like', '%' . $request['return_id'] . '%');
        }

        $data    = $data->paginate($this->limit);
        $data->appends($request->all());

        return view('returns.returns.index', compact('data'));
    }

    public function show($id)
    {
        $return = Returns::findOrFail($id);
        return view('returns.returns.detail', compact('return'));
    }

    public function store(Request $request)
    {
        $params = $request->all();
        $transaction = DB::transaction(function($param) use ($params) {
            $qty = 0;
            for ($i = 0; $i < count($params['name']); $i++)
                $qty+= $params['qty'][$i];

            $last_returns = Returns::where('code', 'like', '%/'.date('n').'/'.date('Y'))->orderBy('code', 'desc')->first();
            if (!$last_returns) {
                $code = '00001/RB/' . date('n') . '/' . date('Y');
            } else {
                $ex = explode('/', $last_returns->code);
                $increment = $ex[0];
                if (date('n') == $ex[2] && date('Y') == $ex[3])
                    $code = str_pad($increment + 1, 5, '0', STR_PAD_LEFT) . '/RB/' . date('n') . '/' . date('Y');
                else
                    $code = '00001/RB/' . date('n') . '/' . date('Y');
            }

            DB::table('returns')->insert([
                'code' => $code,
                'return_date' => date('Y-m-d', strtotime($params['date'])),
                'status' => 0,
                'qty' => $qty,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ]);
            $return_id = DB::getPdo()->lastInsertId();

            for ($i = 0; $i < count($params['name']); $i++) {
                $insert[] = [
                    'product_id' => $params['name'][$i],
                    'return_id' => $return_id,
                    'qty' => $params['qty'][$i],
                    'information' => $params['ket'][$i],
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ];
            }

            DB::table('return_details')->insert($insert);

        });

        $request->session()->flash('status', '200');
        $request->session()->flash('msg', 'Retur Barang berhasil ditambahkan');
        // $request->session()->flash('status', 'err');
        // $request->session()->flash('msg', 'Retur Barang gagal ditambahkan');
        return redirect('returns/returns');
    }

    public function update(Request $request, $id)
    {
        $params = $request->all();
        $transaction = DB::transaction(function() use ($params, $id) {
            $qty = 0;
            for ($i = 0; $i < count($params['name']); $i++)
                $qty+= $params['qty'][$i];

            DB::table('returns')
                ->where('id', $id)
                ->update([
                    'return_date' => date('Y-m-d', strtotime($params['date'])),
                    'status' => 0,
                    'qty' => $qty,
                    'updated_at' => date('Y-m-d H:i:s'),
                ]);

            for ($i = 0; $i < count($params['name']); $i++) {
                $insert[] = [
                    'product_id' => $params['name'][$i],
                    'return_id' => $id,
                    'qty' => $params['qty'][$i],
                    'information' => $params['ket'][$i],
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ];
            }

            DB::table('return_details')->where('return_id', $id)->delete();
            DB::table('return_details')->insert($insert);

        });

        $request->session()->flash('status', '200');
        $request->session()->flash('msg', 'Retur Barang berhasil ditambahkan');
        // $request->session()->flash('status', 'err');
        // $request->session()->flash('msg', 'Retur Barang gagal ditambahkan');
        return redirect('returns/returns');
    }

    public function create(Request $request)
    {
        $products = Product::orderBy('name', 'desc')->get();
        return view('returns.returns.create', compact('products'));
    }

    public function edit($id)
    {
        $return = Returns::find($id);
        $products = Product::orderBy('name', 'desc')->get();
        return view('returns.returns.edit', compact('return', 'products'));   
    }

    public function delete(Request $request, $id)
    {
        $return = Returns::findOrFail($id);

        ReturnDetails::where('return_id', $id)->delete();
        $delete = $return->delete();

        if ($delete) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Retur Barang berhasil dihapus');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('status', 'Retur Barang gagal dihapus');
        }

        return redirect('returns/returns');
    }

    public function export(Request $request)
    {
        return (new ReturnsReportExport)->forReturnId($request['return-id-export'])
            ->forStatus($request['status-export'])
            ->forStart($request['start-export'])
            ->forFinish($request['finish-export'])
            ->download('returns_report-'. date('YmdH:i:s') .'.xlsx');
    }

    public function export_pdf(Request $request, $id)
    {
        $return = Returns::findOrFail($id);
        $pdf = PDF::loadView('pdf/returns/returns', compact('return'));
        return $pdf->download('return_items-'. date('YmdH:i:s') .'.pdf');
        // return $pdf->stream();
    }
}