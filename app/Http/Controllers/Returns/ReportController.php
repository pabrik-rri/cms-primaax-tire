<?php

namespace App\Http\Controllers\Returns;

use App\Exports\ReturnsReportExport;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Controller;

use App\Model\Returns\Returns;

class ReportController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        $this->limit = 50;
    }

    /**
     * Show the application Module.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data  = Returns::orderBy('id', 'desc');

        if (!empty($request['start'] && !empty($request['finish']))) {
            $start = date_format(date_create($request['start']), 'Y-m-d 00:00:00');
            $finish = date_format(date_create($request['finish']), 'Y-m-d 23:59:59');
            $data->whereBetween('return_date', [$start, $finish]);
        }

        if ($request['status'] != '') {
            $data->where('status', $request['status']);
        }

        if (!empty($request['return_id'])) {
        	$data->where('code', 'like', '%' . $request['return_id'] . '%');
        }

        $data    = $data->paginate($this->limit);
        $data->appends($request->all());

        return view('returns.report.index', compact('data'));
    }

    public function export(Request $request)
    {
        return (new ReturnsReportExport)->forReturnId($request['return-id-export'])
        	->forStatus($request['status-export'])
            ->forStart($request['start-export'])
            ->forFinish($request['finish-export'])
            ->download('returns_report-'. date('YmdH:i:s') .'.xlsx');
    }
}
