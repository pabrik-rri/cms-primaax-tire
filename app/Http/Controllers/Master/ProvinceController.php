<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Master\Province;

class ProvinceController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        $this->limit = 50;
    }

    /**
     * Show the application Module.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $province = Province::paginate($this->limit);
        return view('master.province.index', compact('province'));
    }

    public function store(Request $request)
    {
    	$province 			    = new Province;
    	$province->province 	= $request->province;

        $insert = $province->save();

        if ($insert) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Propinsi berhasil ditambahkan');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('status', 'Propinsi gagal ditambahkan');
        }

    	return redirect('province');
    }

    public function create()
    {
        return view('master.province.create');
    }

    public function edit($id)
    {
    	$province = Province::find($id);
        return view('master.province.edit', compact('province'));
    }

    public function update(Request $request, $id)
    {
        $province 			    = Province::find($id);
    	$province->province 	= $request->province;

        $update = $province->save();

        if ($update) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Propinsi berhasil diubah');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('status', 'Propinsi gagal diubah');
        }

    	return redirect('province');
    }

    public function delete(Request $request, $id)
    {
    	$delete	= Province::findOrFail($id)->delete();

        if ($delete) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Propinsi berhasil dihapus');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('status', 'Propinsi gagal dihapus');
        }

    	return redirect('province');
    }
}
