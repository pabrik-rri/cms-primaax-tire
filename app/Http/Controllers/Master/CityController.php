<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Master\City;
use App\Model\Master\Province;

class CityController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        $this->limit = 50;
    }

    /**
     * Show the application Module.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $city = City::paginate($this->limit);

        return view('master.city.index', compact('city'));
    }

    public function store(Request $request)
    {
    	$city 			    = new City;
        $city->city 	    = $request->city;
    	$city->province_id 	= $request->province_id;

        $insert = $city->save();

        if ($insert) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Kota berhasil ditambahkan');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('status', 'Kota gagal ditambahkan');
        }

    	return redirect('city');
    }

    public function create()
    {
        $province = Province::orderBy('province','ASC')->get();

        return view('master.city.create', compact('province'));
    }

    public function edit($id)
    {
    	$city     = City::find($id);
        $province = Province::orderBy('province','ASC')->get(['id','province']);

        return view('master.city.edit', compact('city','province'));
    }

    public function update(Request $request, $id)
    {
        $city 			    = City::find($id);
        $city->city 	    = $request->city;
    	$city->province_id 	= $request->province_id;

        $update = $city->save();

        if ($update) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Kota berhasil diubah');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('status', 'Kota gagal diubah');
        }

    	return redirect('city');
    }

    public function delete(Request $request, $id)
    {
    	$delete	= City::findOrFail($id)->delete();

        if ($delete) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Kota berhasil dihapus');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('status', 'Kota gagal dihapus');
        }

    	return redirect('city');
    }
}
