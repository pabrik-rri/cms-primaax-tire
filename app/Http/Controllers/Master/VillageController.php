<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Master\Village;
use App\Model\Master\District;

class VillageController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        $this->limit = 50;
    }

    /**
     * Show the application Module.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $village = Village::paginate($this->limit);

        return view('master.village.index', compact('village'));
    }

    public function store(Request $request)
    {
    	$village 			    = new Village;
        $village->village 	    = $request->village;
    	$village->district_id 	= $request->district_id;

        $insert = $village->save();

        if ($insert) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Kelurahan berhasil ditambahkan');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('status', 'Kelurahan gagal ditambahkan');
        }

    	return redirect('village');
    }

    public function create()
    {
        $district  = District::orderBy('district','ASC')->get(['id','district']);

        return view('master.village.create', compact('district'));
    }

    public function edit($id)
    {
    	$village     = Village::find($id);
        $district    = District::orderBy('district','ASC')->get(['id','district']);

        return view('master.village.edit', compact('village','district'));
    }

    public function update(Request $request, $id)
    {
        $village 			    = Village::find($id);
        $village->village 	    = $request->village;
    	$village->district_id 	= $request->district_id;

        $update = $village->save();

        if ($update) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Kelurahan berhasil diubah');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('status', 'Kelurahan gagal diubah');
        }

    	return redirect('village');
    }

    public function delete(Request $request, $id)
    {
    	$delete	= Village::findOrFail($id)->delete();

        if ($delete) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Kelurahan berhasil dihapus');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('status', 'Kelurahan gagal dihapus');
        }

    	return redirect('village');
    }
}
