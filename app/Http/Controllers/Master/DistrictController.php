<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Master\District;
use App\Model\Master\City;

class DistrictController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        $this->limit = 50;
    }

    /**
     * Show the application Module.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $district = District::paginate($this->limit);

        return view('master.district.index', compact('district'));
    }

    public function store(Request $request)
    {
    	$district 			    = new District;
        $district->district 	= $request->district;
    	$district->city_id 	    = $request->city_id;

        $insert = $district->save();

        if ($insert) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Kecamatan berhasil ditambahkan');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('status', 'Kecamatan gagal ditambahkan');
        }

    	return redirect('district');
    }

    public function create()
    {
        $city  = City::orderBy('city','ASC')->get(['id','city']);

        return view('master.district.create', compact('city'));
    }

    public function edit($id)
    {
    	$district     = District::find($id);
        $city         = City::orderBy('city','ASC')->get(['id','city']);

        return view('master.district.edit', compact('district','city'));
    }

    public function update(Request $request, $id)
    {
        $district 			    = District::find($id);
        $district->district 	= $request->district;
    	$district->city_id 	    = $request->city_id;

        $update = $district->save();

        if ($update) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Kecamatan berhasil diubah');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('status', 'Kecamatan gagal diubah');
        }

    	return redirect('district');
    }

    public function delete(Request $request, $id)
    {
    	$delete	= District::findOrFail($id)->delete();

        if ($delete) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Kecamatan berhasil dihapus');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('status', 'Kecamatan gagal dihapus');
        }

    	return redirect('district');
    }
}
