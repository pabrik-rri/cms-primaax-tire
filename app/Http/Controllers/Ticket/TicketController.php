<?php

namespace App\Http\Controllers\Ticket;

use App\Exports\TicketsExport;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Controller;

use App\Model\Ticket\Ticket;

class TicketController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        $this->limit = 50;
    }

    /**
     * Show the application Module.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $ticket  = Ticket::orderBy('id', 'desc');

        if (!empty($request['start'] && !empty($request['finish']))) {
            $start = date_format(date_create($request['start']), 'Y-m-d');
            $finish = date_format(date_create($request['finish']), 'Y-m-d');

            $ticket->whereBetween('created_at', [$start, $finish]);
        }

        $ticket    = $ticket->paginate($this->limit);
        $ticket->appends($request->all());

        return view('ticket.ticket.index', compact('ticket'));
    }

    public function export(Request $request) {
        return (new TicketsExport)->forStart($request['start-export'])
            ->forFinish($request['finish-export'])
            ->download('ticket-'. date('YmdH:i:s') .'.xlsx');
    }
}
