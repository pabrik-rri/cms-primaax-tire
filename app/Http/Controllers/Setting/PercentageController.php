<?php

namespace App\Http\Controllers\Setting;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Setting\Percentage;

class PercentageController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->limit = 50;
    }

    /**
     * Show the application Module.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $percentage = Percentage::paginate($this->limit);
        return view('setting.percentage.index', compact('percentage'));
    }

    public function edit($id)
    {
    	$percentage   = Percentage::find($id);

        return view('setting.percentage.edit', compact('percentage'));
    }

    public function update(Request $request, $id)
    {
        $percentage 			    = Percentage::find($id);
    	$percentage->percentage 	= $request->percentage;

        $update = $percentage->save();

        if ($update) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Persentase berhasil diubah');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('status', 'Persentase gagal diubah');
        }

    	return redirect('percentage');
    }
}
