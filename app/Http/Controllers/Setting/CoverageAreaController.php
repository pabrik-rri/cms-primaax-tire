<?php

namespace App\Http\Controllers\Setting;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Master\City;
use App\Model\Master\Province;
use App\Model\User\Mitra;
use App\Model\User\Store;

class CoverageAreaController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application Module.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $mitra = new Mitra;
        $store = new Store;

        if (!empty($request['store'])) {
            $mitra = null;
            $store = $store->where('id', $request['store']);
        } else if (!empty($request['mitra'])) {
            $mitra = $mitra->where('id', $request['mitra']);
            $store = $store->where('mitra_id', $request['mitra']);
        } else if (!empty($request['city'])) {
            $mitra = $mitra->where('city_id', $request['city']);
            $store = $store->where('city_id', $request['city']);
        } else if (!empty($request['province'])) {
            $arr_filter_cities = [];
            $filter_cities = City::select('id')->where('province_id', $request['province'])->get();
            foreach ($filter_cities as $value)
                $arr_filter_cities[] = $value['id'];

            $mitra = $mitra->whereIn('city_id', $arr_filter_cities);
            $store = $store->whereIn('city_id', $arr_filter_cities);
        }

        $provinces  = json_encode(Province::get());
        $cities     = json_encode(City::get());
        $mitras     = $mitra ? json_encode($mitra->get()) : json_encode([]);
        $stores     = $store ? json_encode($store->get()) : json_encode([]);

        $select_mitras = json_encode(Mitra::get());
        $select_stores = json_encode(Store::get());

        return view('setting.coverage_area.index', compact('provinces', 'cities', 'mitras', 'stores', 'select_mitras', 'select_stores'));
    }
}
