<?php

namespace App\Http\Controllers\Setting;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Setting\Banner;

class BannerController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        $this->cdn        = "http://cdn.rajarumahku.com/";
        $this->cdnBanner  = "../../cdn/ads/";
        $this->getBanner  = $this->cdn."/ads/";

        $this->limit = 50;
    }

    /**
     * Show the application Module.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $banner = Banner::paginate($this->limit);
        return view('setting.banner.index', compact('banner'));
    }

    public function store(Request $request)
    {
    	$banner 			    = new Banner;
    	$banner->title 	        = $request->title;

        if($request->hasFile('image')){
            $path           = $this->cdnBanner;
            $pathGet        = $this->getBanner;

            $avatar         = $_FILES['image'];

            $filename       = $this->storeFile($avatar, 0, '', $path, $pathGet);

            if($filename['return'] == true) {
                $banner->image  = $filename['message'];
            }
        }

        $insert = $banner->save();

        if ($insert) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Banner berhasil ditambahkan');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('status', 'Banner gagal ditambahkan');
        }

    	return redirect('banner');
    }

    public function create()
    {
        return view('setting.banner.create');
    }

    public function edit($id)
    {
    	$banner = Banner::find($id);
        $path     = $this->getBanner;

        return view('setting.banner.edit', compact('banner','path'));
    }

    public function update(Request $request, $id)
    {
        $banner 			    = Banner::find($id);
    	$banner->title 	        = $request->title;

        if($request->hasFile('image')){
            $path           = $this->cdnBanner;
            $pathGet        = $this->getBanner;

            $avatar         = $_FILES['image'];

            $filename       = $this->storeFile($avatar, 1, $banner, $path, $pathGet);

            if($filename['return'] == true) {
                $banner->image  = $filename['message'];
            }
        }

        $update = $banner->save();

        if ($update) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Banner berhasil diubah');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('status', 'Banner gagal diubah');
        }

    	return redirect('banner');
    }

    public function delete(Request $request, $id)
    {
    	$banner	= Banner::findOrFail($id);

        // if(isset($banner->image)) {
        //     unlink($this->getBanner.$banner->image);
        // }

        $delete = $banner->delete();

        if ($delete) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Banner berhasil dihapus');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('status', 'Banner gagal dihapus');
        }

    	return redirect('banner');
    }

    public function storeFile($avatar, $old = '', $user = '', $path = '', $pathGet = '')
    {
        // if($old == 1 && $user->image != "") {
        //     $pathImage = $pathGet."/".$user->image;
        //     unlink($pathImage);
        // }

        $userfile       = explode(".", strtolower($avatar['name']));

        $filename       = $userfile[0];
        $fileext        = $userfile[1];

        $file           = md5($filename).".".$fileext;

        $target_dir     = $path;
        $target_file    = $target_dir . basename($file);
        $uploadOk       = 1;
        $imageFileType  = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

        // Check if image file is a actual image or fake image
        $check = getimagesize($_FILES["image"]["tmp_name"]);
        if($check !== false) {
            $data['return'] 	= false;
            $data['message'] 	= "File is an image - " . $check["mime"] . ".";

            $uploadOk = 1;
        } else {
            $data['return'] 	= false;
            $data['message'] 	= "File is not an image.";

            $uploadOk = 0;
        }

        // Check file size
        if ($_FILES["image"]["size"] > 500000) {
            $data['return'] 	= false;
            $data['message'] 	= "Sorry, your file is too large.";

            $uploadOk = 0;
        }
        // Allow certain file formats
        if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
        && $imageFileType != "gif" ) {
            $data['return'] 	= false;
            $data['message'] 	= "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";

            $uploadOk = 0;
        }
        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            $data['return'] 	= false;
            $data['message'] 	= "Sorry, your file was not uploaded.";

        // if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($_FILES["image"]["tmp_name"], $target_file)) {

            	$data['return']  = true;
                $data['message'] = $file;

            } else {
                $data['return'] 	= false;
                $data['message'] 	= "Sorry, there was an error uploading your file.";
            }
        }

        return $data;
    }
}
