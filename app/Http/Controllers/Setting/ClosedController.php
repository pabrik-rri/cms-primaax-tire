<?php

namespace App\Http\Controllers\Setting;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Setting\Closed;

class ClosedController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->limit = 50;
    }

    /**
     * Show the application Module.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $closed = Closed::paginate($this->limit);
        return view('setting.closed.index', compact('closed'));
    }

    public function edit($id)
    {
    	$closed   = Closed::find($id);

        return view('setting.closed.edit', compact('closed'));
    }

    public function update(Request $request, $id)
    {
        $closed 			= closed::find($id);
    	$closed->time 	    = $request->time;

        $update = $closed->save();

        if ($update) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Batas waktu otomatis penghapusan order berhasil diubah');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('status', 'Batas waktu otomatis penghapusan order gagal diubah');
        }

    	return redirect('closed');
    }
}
