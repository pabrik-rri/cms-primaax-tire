<?php

namespace App\Http\Controllers\Setting;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Setting\Radius;

class RadiusController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->limit = 50;
    }

    /**
     * Show the application Module.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $radius = Radius::paginate($this->limit);
        return view('setting.radius.index', compact('radius'));
    }

    public function edit($id)
    {
    	$radius   = Radius::find($id);

        return view('setting.radius.edit', compact('radius'));
    }

    public function update(Request $request, $id)
    {
        $radius 			    = Radius::find($id);
    	$radius->radius 	    = $request->radius;

        $update = $radius->save();

        if ($update) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Radius order berhasil diubah');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('status', 'Radius order gagal diubah');
        }

    	return redirect('radius');
    }
}
