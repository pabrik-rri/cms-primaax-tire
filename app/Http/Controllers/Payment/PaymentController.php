<?php

namespace App\Http\Controllers\Payment;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Master\Payment;

class PaymentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        $this->cdn          = config('app.cdn');
        $this->cdnPayment   = "../../cdn/payment_methods/";
        $this->getPayment   = $this->cdn."/payment_methods/";

        $this->limit = 50;
    }

    /**
     * Show the application Module.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $payment  = Payment::orderBy('id');

        if( isset($request->key) && isset($request->value) )
        {
            $key    = $request->key;
            $value  = $request->value;

            $payment->where($key,'like','%'. $value .'%');
        }

        $payment    = $payment->paginate($this->limit);
        $payment->appends($request->all());
        $path       = $this->getPayment;

        return view('master.payment.index', compact('payment', 'path'));
    }

    public function store(Request $request)
    {
    	$payment            = new Payment;
        $payment->payment 	= $request->payment;
        $payment->status    = $request->status;

    	if($request->hasFile('image')){
            $path           = $this->cdnPayment;
            $pathGet        = $this->getPayment;

            $avatar         = $_FILES['image'];

            $filename       = $this->storeFile($avatar, 0, '', $path, $pathGet);

            if($filename['return'] == true) {
                $payment->image  = $filename['message'];
            }
        }

        $insert = $payment->save();

        if ($insert) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Payment berhasil ditambahkan');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('status', 'Payment gagal ditambahkan');
        }

        return redirect('payment');
    }

    public function create()
    {
        return view('master.payment.create');
    }

    public function edit($id)
    {
        $payment    = Payment::find($id);
        $path       = $this->getPayment;

        return view('master.payment.edit', compact('payment','path'));
    }

    public function update(Request $request, $id)
    {
        $payment            = Payment::find($id);
        $payment->payment   = $request->payment;
        $payment->status    = $request->status;

        if($request->hasFile('image')){
            $path           = $this->cdnPayment;
            $pathGet        = $this->getPayment;

            $avatar         = $_FILES['image'];

            $filename       = $this->storeFile($avatar, 1, $payment, $path, $path);

            if($filename['return'] == true) {
                $payment->image  = $filename['message'];
            }
        }

        $update = $payment->save();

        if ($update) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Payment berhasil diubah');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('status', 'Payment gagal diubah');
        }

        return redirect('payment');
    }

    public function delete(Request $request, $id)
    {
        $payment   = Payment::findOrFail($id);

        if ($payment->image != "")
            unlink($this->cdnPayment.$payment->image);

        $delete = $payment->delete();

        if ($delete) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Payment berhasil dihapus');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('status', 'Payment gagal dihapus');
        }

        return redirect('payment');
    }

    public function storeFile($avatar, $old = '', $user = '', $path = '', $pathGet = '')
    {
        if($old == 1 && $user->image != "") {
            $pathImage = $pathGet.$user->image;
            unlink($pathImage);
        }

        $userfile       = explode(".", strtolower($avatar['name']));

        $filename       = $userfile[0];
        $fileext        = $userfile[1];

        $file           = md5($filename).".".$fileext;

        $target_dir     = $path;
        $target_file    = $target_dir . basename($file);
        $uploadOk       = 1;
        $imageFileType  = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

        // Check if image file is a actual image or fake image
        $check = getimagesize($_FILES["image"]["tmp_name"]);
        if($check !== false) {
            $data['return']     = false;
            $data['message']    = "File is an image - " . $check["mime"] . ".";

            $uploadOk = 1;
        } else {
            $data['return']     = false;
            $data['message']    = "File is not an image.";

            $uploadOk = 0;
        }

        // Check file size
        if ($_FILES["image"]["size"] > 500000) {
            $data['return']     = false;
            $data['message']    = "Sorry, your file is too large.";

            $uploadOk = 0;
        }
        // Allow certain file formats
        if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
        && $imageFileType != "gif" ) {
            $data['return']     = false;
            $data['message']    = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";

            $uploadOk = 0;
        }
        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            $data['return']     = false;
            $data['message']    = "Sorry, your file was not uploaded.";

        // if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($_FILES["image"]["tmp_name"], $target_file)) {

                $data['return']  = true;
                $data['message'] = $file;

            } else {
                $data['return']     = false;
                $data['message']    = "Sorry, there was an error uploading your file.";
            }
        }

        return $data;
    }
}
