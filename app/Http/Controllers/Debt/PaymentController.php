<?php

namespace App\Http\Controllers\Debt;

use DB;
use PDF;

use App\Exports\DebtPaymentExport;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Controller;

use App\Model\User\Mitra;
use App\Model\User\Store;
use App\User;
use App\Model\Receivables\Receivables;
use App\Model\Receivables\Receipt;
use App\Model\Debt\Debts;
use App\Model\Debt\DebtDetails;

class PaymentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        $this->limit = 50;
    }

    /**
     * Show the application Module.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $debts = Debts::where('customer_name', \Auth::user()->name)->orderBy('debt_date', 'desc');

        $level = \Auth::user()->level_id;

        if (!empty($request['start'] && !empty($request['finish']))) {
            $start = date_format(date_create($request['start']), 'Y-m-d 00:00:00');
            $finish = date_format(date_create($request['finish']), 'Y-m-d 23:59:59');
            $debts->whereBetween('created_at', [$start, $finish]);
        }

        if ($request['status'] != '') {
            $debts->where('status', $request['status']);
        }

        if (!empty($request['code'])) {
        	$debts->where('code', 'like', '%' . $request['code'] . '%');
        }

        $debts    = $debts->paginate($this->limit);
        $debts->appends($request->all());

        return view('debt.payment.index', compact('debts'));
    }

    public function show($id)
    {
        $level = \Auth::user()->level_id;
        $mitra_id = \Auth::user()->mitra_id;

        $upline = $this->check_upline($level, $mitra_id);

        $debt = Debts::findOrFail($id);
        return view('debt.payment.detail', compact('debt', 'upline'));
    }

    public function create(Request $request)
    {        
        $level = \Auth::user()->level_id;
        $mitra_id = \Auth::user()->mitra_id;

        $upline = $this->check_upline($level, $mitra_id);

        $receivables = Receivables::
            select('receivables.*', DB::raw('receivables.balance_of_receivable - COALESCE((select sum(balance) from debt_details join debts on debts.id = debt_details.debt_id where receivable_id = receivables.id and debts.status = 0), 0) as balance_of_receivable'))
            ->where('customer_name', \Auth::user()->name)
            ->having('balance_of_receivable', '>', 0)
            ->orderBy('id', 'asc')
            ->get();

        return view('debt.payment.create', compact('receivables', 'upline'));
    }

    // public function edit(Request $request)
    // {        
    //     $level = \Auth::user()->level_id;
    //     $mitra_id = \Auth::user()->mitra_id;

    //     $upline = $this->check_upline($level, $mitra_id);

    //     $receivables = Receivables::
    //         where('customer_name', \Auth::user()->name)
    //         ->where('balance_of_receivable', '>', 0)
    //         ->orderBy('id', 'asc')->get();

    //     return view('debt.payment.create', compact('receivables', 'upline'));
    // }

    public function store(Request $request)
    {
        $params = $request->all();

        $transaction = DB::transaction(function() use ($params, $request) {

            $last_returns = Debts::where('code', 'like', '%/'.date('n').'/'.date('Y'))->orderBy('code', 'desc')->first();
            if (!$last_returns) {
                $code = '00001/PH/' . date('n') . '/' . date('Y');
            } else {
                $ex = explode('/', $last_returns->code);
                $increment = $ex[0];
                if (date('n') == $ex[2] && date('Y') == $ex[3])
                    $code = str_pad($increment + 1, 5, '0', STR_PAD_LEFT) . '/PH/' . date('n') . '/' . date('Y');
                else
                    $code = '00001/PH/' . date('n') . '/' . date('Y');
            }

            $payment = $this->priceclean($params['payment']);
            DB::table('debts')->insert([
                'code' => $code,
                'debt_date' => date('Y-m-d', strtotime($params['now'])),
                'supplier_id' => $params['supplier_id'],
                'customer_name' => \Auth::user()->name,
                'value_of_debt' => $payment,
                'status' => 0,
                'payment_method' => $params['payment_method'],
                'is_factory' => \Auth::user()->level_id,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ]);

            $debt_id = DB::getPdo()->lastInsertId();

            $receivables = Receivables::
            select('receivables.*', DB::raw('receivables.balance_of_receivable - COALESCE((select sum(balance) from debt_details join debts on debts.id = debt_details.debt_id where receivable_id = receivables.id and debts.status = 0), 0) as balance_of_receivable'))
            ->where('customer_name', \Auth::user()->name)
            ->where('balance_of_receivable', '>', 0)
            ->orderBy('id', 'asc')->get();

            $insert = [];
            foreach ($receivables as $key => $value) {
                if ($payment <= 0)
                    break;
                if ($value->balance_of_receivable - $payment <= 0) {
                    $saldo_hutang = $value->balance_of_receivable;
                    $payment -= $saldo_hutang;
                    // $saldo_piutang = 0;
                } else {
                    $saldo_hutang = $payment;
                    $payment -= $saldo_hutang;
                    // $saldo_piutang = $value->balance_of_receivable - $saldo_hutang;
                }

                // Update Saldo Piutang dengan saldo piutang = 0
                // Receivables::where('id', $value->id)
                //     ->update(['balance_of_receivable' => $saldo_piutang]);

                $insert[] = [
                    'receivable_id' => $value->id,
                    'debt_id' => $debt_id,
                    'balance' => $saldo_hutang,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ];
            }

            DB::table('debt_details')->insert($insert);
            $request->session()->flash('print', $debt_id);
        });
        
        $request->session()->flash('status', '200');
        $request->session()->flash('msg', 'Pembayaran Hutang berhasil ditambahkan');
        return redirect('debt/payment/create');
    }

    public function delete(Request $request, $id)
    {
        $transaction = DB::transaction(function() use ($request, $id) {
            $debt = Debts::findOrFail($id);

            // foreach ($debt->debtDetails as $key => $value) {
            //     Receivables::where('id', $value->receivable_id)
            //         ->update(['balance_of_receivable' => DB::raw($value->balance . '+' . $value->receivables->balance_of_receivable)]);
            // }

            DebtDetails::where('debt_id', $id)->delete();
            $delete = $debt->delete();

            if ($delete) {
                $request->session()->flash('status', '200');
                $request->session()->flash('msg', 'Pembayaran Hutang berhasil dihapus');
            } else {
                $request->session()->flash('status', 'err');
                $request->session()->flash('status', 'Pembayaran Hutang gagal dihapus');
            }
        });
        return redirect('debt/payment');
    }

    public function export(Request $request)
    {
        return (new DebtPaymentExport)->forCode($request['code-export'])
            ->forStatus($request['status-export'])
            ->forStart($request['start-export'])
            ->forFinish($request['finish-export'])
            ->download('debt_payments-'. date('YmdH:i:s') .'.xlsx');
    }

    public function export_pdf(Request $request, $id)
    {
        $level = \Auth::user()->level_id;
        $mitra = \Auth::user()->mitra_id;

        $upline = $this->check_upline($level, $mitra);

        $debt = Debts::findOrFail($id);

        $pdf = PDF::loadView('pdf/debt/payment', compact('debt', 'upline'));
        return $pdf->download('debt_payments-'. date('YmdH:i:s') .'.pdf');
        // return $pdf->stream();
    }

    private function check_upline($level, $mitra)
    {
        switch ($level) {
            case 3:
                $profile = Mitra::where('id', $mitra)->first();
                // Pabrik
                $upline = User::where('id', $profile->vendor_id)->first();
                break;
            case 4:
                // Agen
                $profile = Store::where('id', $mitra)->first();
                $upline = Mitra::where('id', $profile->vendor_id)->first();
                break;
        }

        return $upline;
    }

    private function priceclean($angka)
    {
        $angka = str_replace("Rp.","",$angka);
        $angka = str_replace(".","",$angka);
        $angka = str_replace("%","",$angka);
        $angka = str_replace(",",".",$angka);
        return $angka;
    }
}
