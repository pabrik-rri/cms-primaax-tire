<?php

namespace App\Http\Controllers\Debt;

use DB;
use PDF;

use App\Exports\DebtReportExport;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Controller;

use App\Model\User\Mitra;
use App\Model\User\Store;
use App\User;
use App\Model\Receivables\Receivables;
use App\Model\Receivables\Receipt;
use App\Model\Debt\Debts;
use App\Model\Debt\DebtDetails;

class ReportController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        $this->limit = 50;
    }

    public function index(Request $request)
    {        
        $receivables = Receivables::
            select('receivables.*')
            ->where('customer_name', \Auth::user()->name)
            ->orderBy('id', 'asc');
        
        if (!empty($request['code'])) {
            $receivables->where('code', $request['code']);
        }

        if (!empty($request['start'] && !empty($request['finish']))) {
            $start = date('Y-m-d 00:00:00',strtotime($request['start']));
            $finish = date('Y-m-d 23:59:59',strtotime($request['finish']));
            $receivables->whereBetween('receivables.created_at', [$start, $finish]);
        }

        $receivables = $receivables->get();

        // return $receivables; exit;

        return view('debt.report.index', compact('receivables'));
    }

    public function export(Request $request)
    {
        return (new DebtReportExport)->forCode($request['code-export'])
            ->forCustomer(\Auth::user()->name)
            ->forStart($request['start-export'])
            ->forFinish($request['finish-export'])
            ->download('report_debt-'. date('YmdH:i:s') .'.xlsx');
    }
}
