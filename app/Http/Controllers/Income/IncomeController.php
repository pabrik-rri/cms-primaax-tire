<?php

namespace App\Http\Controllers\Income;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Exports\OrdersExport;
use Maatwebsite\Excel\Facades\Excel;

use App\Model\Order\Order;
use App\Model\Order\OrderDetail;
use App\Model\Setting\Percentage;
use App\Model\User\Mitra;

class IncomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->limit = 50;
    }

    /**
     * Show the application Module.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $params   = $request->all();

        $obj      = new Order;
        $data     = $obj->getIncomeMitra($this->limit, $params);

        $income   = $data['incomes'];
        $total    = $data['total'];      

        $percentage = Percentage::first();
        $mitra      = Mitra::where('is_courier', 0)->orderBy('name','ASC')->get();

        return view('income.index', compact('income','percentage','mitra','total'));
    }
}
