<?php

namespace App\Http\Controllers\Receivables;

use App\Exports\ReceivablesBalanceExport;
use App\Imports\ReceivablesBalanceImport;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Controller;

use App\Model\User\Mitra;
use App\Model\User\Store;
use App\User;
use App\Model\Receivables\Receivables;

class BalanceController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        $this->limit = 50;
    }

    /**
     * Show the application Module.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $balance  = Receivables::where('supplier_name', \Auth::user()->name)->orderBy('id', 'desc');

        $level = \Auth::user()->level_id;
    	switch ($level) {
            case 1:
                // Pabrik
                $customer = User::where('level_id', 2)->orderBy('name', 'asc')->get();
                break;
            case 2:
                // Agent
                $customer = Mitra::orderBy('name', 'asc')->get();
                break;
            case 3:
                // Toko
                $customer = Store::orderBy('name')->get();
                break;
            default:
                $customer = null;
                break;
    	}

        if (!empty($request['customer'])) {
        	$balance->where('customer_name', $request['customer']);
        }

        $balance->where('supplier_name', \Auth::user()->name);
        $balance->where('order_id', null);

        $balance    = $balance->paginate($this->limit);
        $balance->appends($request->all());

        return view('receivables.balance.index', compact('balance', 'customer'));
    }

    public function store(Request $request)
    {
    	$last_receivables = Receivables::where('code', 'like', '%/'.date('n').'/'.date('Y'))->orderBy('code', 'desc')->first();
        if (!$last_receivables) {
            $code = '00001/FB/' . date('n') . '/' . date('Y');
        } else {
            $ex = explode('/', $last_receivables->code);
            $increment = $ex[0];
            if (date('n') == $ex[2] && date('Y') == $ex[3])
                $code = str_pad($increment + 1, 5, '0', STR_PAD_LEFT) . '/FB/' . date('n') . '/' . date('Y');
            else
                $code = '00001/FB/' . date('n') . '/' . date('Y');
        }
		$balance            			= new Receivables;
		$balance->code 					= $code;
		$balance->invoice_date    		= date('Y-m-d', strtotime($request['date']));
		$balance->supplier_name    		= \Auth::user()->name;
		$balance->customer_name    		= $request['customer'];
		$balance->value_of_receivable 	= str_replace(".","",$request['receivables']);
		$balance->balance_of_receivable	= str_replace(".","",$request['balance']);
		$balance->is_factory    		= \Auth::user()->level_id;

        $insert = $balance->save();

        if ($insert) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Saldo Awal Piutang berhasil ditambahkan');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Saldo Awal Piutang gagal ditambahkan');
        }

        return redirect('receivables/balance');
    }

    public function create()
    {
        $level = \Auth::user()->level_id;
        switch ($level) {
            case 1:
                // Pabrik
                $customer = User::where('level_id', 2)->orderBy('name', 'asc')->get();
                break;
            case 2:
                $customer = Mitra::orderBy('name', 'asc')->get();
                break;
            case 3:
                $customer = Store::orderBy('name')->get();
                break;
            default:
                $customer = null;
                break;
        }

        return view('receivables.balance.create', compact('customer'));
    }

    public function delete(Request $request, $id)
    {
        $balance   	= Receivables::findOrFail($id);
        $delete 	= $balance->delete();

        if ($delete) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Saldo Awal Piutang berhasil dihapus');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Saldo Awal Piutang gagal dihapus');
        }

        return redirect('receivables/balance');
    }

    public function multiple_delete(Request $request)
    {
        $balance = Receivables::whereIn('id', explode(',', $request['deleted-id']));
        if ($balance->delete()) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Saldo Awal Piutang berhasil dihapus');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Saldo Awal Piutang gagal dihapus');
        }

        return redirect('receivables/balance');
    }

    public function export(Request $request)
    {
        return (new ReceivablesBalanceExport)->forCustomerName($request['customer-export'])
        	->forSupplierName(\Auth::user()->name)
            ->download('initial_balance_of_receivables-'. date('YmdH:i:s') .'.xlsx');
    }

    public function import(Request $request)
    {
	    $this->validate($request, [
			'import_file' => 'mimes:xls,xlsx',
		],
		$messages = [
			'mimes' => 'File harus Excel'
		]);

		if ($request->hasFile('import_file')) {
			$request->session()->flash('status', '200');
	    	$request->session()->flash('msg', 'File berhasil diimport');

			$import = new ReceivablesBalanceImport;
			$import->import(request()->file('import_file'));

			return redirect('receivables/balance');
		} else {
			$request->session()->flash('status', 'err');
		    $request->session()->flash('msg', 'File harus Excel');
		    return redirect('receivables/balance');
		}
    }
}
