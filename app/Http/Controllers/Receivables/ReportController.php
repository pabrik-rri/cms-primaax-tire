<?php

namespace App\Http\Controllers\Receivables;

use App\Exports\ReceivablesReceiptExport;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\User\Mitra;
use App\Model\User\Store;
use App\User;
use App\Model\Receivables\Receivables;

class ReportController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        $this->limit = 50;
    }

    /**
     * Show the application Module.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $balance  = Receivables::where('supplier_name', \Auth::user()->name)->orderBy('id', 'desc');

        $level = \Auth::user()->level_id;
    	switch ($level) {
            case 1:
                // Pabrik
                $customer = User::where('level_id', 2)->orderBy('name', 'asc')->get();
                break;
            case 2:
                // Agent
                $customer = Mitra::orderBy('name', 'asc')->get();
                break;
            case 3:
                // Toko
                $customer = Store::orderBy('name')->get();
                break;
            default:
                $customer = null;
                break;
    	}

        if (!empty($request['name'])) {
        	$balance->where('customer_name', $request['name']);
        }

        if (!empty($request['start'] && !empty($request['finish']))) {
            $start = date_format(date_create($request['start']), 'Y-m-d 00:00:00');
            $finish = date_format(date_create($request['finish']), 'Y-m-d 23:59:59');
            $balance->whereBetween('receivables.created_at', [$start, $finish]);
        }

        $balance->where('supplier_name', \Auth::user()->name);
        $balance->where('order_id', null);

        $balance    = $balance->paginate($this->limit);
        $balance->appends($request->all());

        return view('receivables.report.index', compact('balance', 'customer'));
    }

    public function export(Request $request)
    {
        return (new ReceivablesReceiptExport)->forCustomerName($request['customer-export'])
        	->forStart($request['start-export'])
            ->forFinish($request['finish-export'])
            ->download('report_of_receivables-'. date('YmdH:i:s') .'.xlsx');
    }
}
