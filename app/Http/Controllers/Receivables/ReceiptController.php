<?php

namespace App\Http\Controllers\Receivables;

use DB;

use App\Exports\ReceivablesReceiptExport;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Controller;

use App\Model\Receivables\Receipt;
use App\Model\Debt\Debts;
use App\Model\Receivables\Receivables;

class ReceiptController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        $this->limit = 50;
    }

    /**
     * Show the application Module.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $receipts  = Debts::select('debts.*', 'receivable_pays.no_receipt')
        ->leftJoin('receivable_pays', 'receivable_pays.code', '=', 'debts.code')
        ->where('debts.supplier_id', \Auth::user()->id)
        ->orderBy('debts.status', 'asc')
        ->orderBy('debts.id', 'desc');


        if (!empty($request['start'] && !empty($request['finish']))) {
            $start = date_format(date_create($request['start']), 'Y-m-d 00:00:00');
            $finish = date_format(date_create($request['finish']), 'Y-m-d 23:59:59');
            $receipts->whereBetween('debts.created_at', [$start, $finish]);
        }

        if (!empty($request['name'])) {
        	$receipts->where('debts.customer_name', 'like', '%' . $request['name'] . '%');
        }

        $receipts    = $receipts->paginate($this->limit);
        $receipts->appends($request->all());

        return view('receivables.receipt.index', compact('receipts'));
    }

    public function store(Request $request)
    {
    	$last_receipt = Receipt::where('code', 'like', '%/'.date('n').'/'.date('Y'))->orderBy('code', 'desc')->first();
    	if (!$last_receipt) {
    		$code = '00001/PH/' . date('n') . '/' . date('Y');
    	} else {
    		$ex = explode('/', $last_receipt->code);
    		$increment = $ex[0];
    		if (date('n') == $ex[2] && date('Y') == $ex[3])
    			$code = str_pad($increment + 1, 5, '0', STR_PAD_LEFT) . '/PH/' . date('n') . '/' . date('Y');
    		else
    			$code = '00001/PH/' . date('n') . '/' . date('Y');
    	}

    	$receivables  = Receivables::where('id', $request['receivables'])->first();

		$balance            			= new Receipt;
		$balance->code 					= $code;
		$balance->customer_name    		= $receivables['customer_name'];
		$balance->receivable_id		 	= $request['receivables'];

        $insert = $balance->save();

        if ($insert) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Penerimaan Piutang berhasil ditambahkan');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Penerimaan Piutang gagal ditambahkan');
        }

        return redirect('receivables/receipt');
    }

    public function update(Request $request, $id)
    {
    	$receipt = Receipt::findOrFail($id);
		$receivables  = Receivables::where('id', $request['receivables'])->first();

		$receipt->customer_name    		= $receivables['customer_name'];
		$receipt->receivable_id		 	= $request['receivables'];

        if ($receipt->save()) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Penerimaan Piutang berhasil diedit');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Penerimaan Piutang gagal diedit');
        }

        return redirect('receivables/receipt');
    }

    public function create()
    {
    	$arr_receipt = [];

    	$receivables = Receivables::whereNotIn('id', $arr_receipt)
    	->where('supplier_name', \Auth::user()->name)
        ->where('balance_of_receivable', '!=', 0)->orderBy('customer_name', 'asc')->get();

        return view('receivables.receipt.create', compact('receivables'));
    }

    public function edit($id)
    {
    	$arr_receipt = [];

    	$receipt = Receipt::findOrFail($id);

    	$receivables = Receivables::whereNotIn('id', $arr_receipt)
    	->where('supplier_name', \Auth::user()->name)->orderBy('customer_name', 'asc')->get();

        return view('receivables.receipt.edit', compact('receipt', 'receivables'));
    }

    public function repayment(Request $request, $id)
    {
        $transaction = DB::transaction(function() use ($request, $id) {
            $balance   	= Debts::findOrFail($id);
            if ($balance->value_of_debt != $this->priceclean($request['receipt_value'])) {
    			$request->session()->flash('status', 'err');
    			$request->session()->flash('msg', 'Nilai penerimaan tidak sesuai dengan uang yang dibayar ! Harap isi nilai dengan benar atau hubungi pelanggan anda');
    			return redirect('receivables/receipt');
            }

            $last_receipt = Receipt::where('no_receipt', 'like', '%/'.date('n').'/'.date('Y'))->orderBy('no_receipt', 'desc')->first();
        	if (!$last_receipt) {
        		$code = '00001/PP/' . date('n') . '/' . date('Y');
        	} else {
        		$ex = explode('/', $last_receipt->no_receipt);
        		$increment = $ex[0];
        		if (date('n') == $ex[2] && date('Y') == $ex[3])
        			$code = str_pad($increment + 1, 5, '0', STR_PAD_LEFT) . '/PP/' . date('n') . '/' . date('Y');
        		else
        			$code = '00001/PP/' . date('n') . '/' . date('Y');
        	}

            $receipt                        = new Receipt;
            $receipt->code                  = $balance->code;
            $receipt->customer_name         = $balance->customer_name;
            $receipt->no_receipt            = $code;

            // Update Receivable's Balance
            foreach ($balance->DebtDetails as $value) {
                $receivable = Receivables::findOrFail($value->receivable_id);
                $receivable->balance_of_receivable = $receivable->balance_of_receivable - $value->balance;
                if ($receivable->balance_of_receivable == 0) {
                    $receivable->status = 1;
                }
                $receivable->save();
            }

            $balance->status                = 1;

            if ($balance->save() && $receipt->save()) {
                $request->session()->flash('status', '200');
                $request->session()->flash('msg', 'Konfirmasi pembayaran berhasil');
            } else {
                $request->session()->flash('status', 'err');
                $request->session()->flash('msg', 'Konfirmasi pembayaran gagal');
            }
        });

        return redirect('receivables/receipt');
    }

    public function export(Request $request)
    {
        return (new ReceivablesReceiptExport)->forCustomerName($request['customer-export'])
        	->forStart($request['start-export'])
            ->forFinish($request['finish-export'])
            ->download('receipt_of_receivables-'. date('YmdH:i:s') .'.xlsx');
    }

    private function priceclean($angka)
    {
        $angka = str_replace("Rp.","",$angka);
        $angka = str_replace(".","",$angka);
        $angka = str_replace("%","",$angka);
        $angka = str_replace(",",".",$angka);
        return $angka;
    }
}
