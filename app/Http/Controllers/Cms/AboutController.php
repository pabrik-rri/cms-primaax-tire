<?php

namespace App\Http\Controllers\Cms;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Cms\About;

class AboutController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        $this->cdn       = config('app.cdn');
        $this->cdnAbout  = "../../cdn/about/";
        $this->getAbout  = $this->cdn."/about/";
        $this->limit     = 50;
    }

    /**
     * Show the application Module.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $about = About::paginate($this->limit);
        return view('cms.about.index', compact('about'));
    }

    public function store(Request $request)
    {
    	$about 			      = new About;
    	$about->title 	      = $request->title;
    	$about->description    = $request->description;
    	$about->status         = 1;

        if($request->hasFile('image')){
            $path           = $this->cdnAbout;
            $pathGet        = $this->getAbout;

            $avatar         = $_FILES['image'];

            $filename       = $this->storeFile($avatar, 0, '', $path, $pathGet);

            if($filename['return'] == true) {
                $about->image  = $filename['message'];
            }
        }

        $insert = $about->save();

        if ($insert) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Tentang berhasil ditambahkan');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Tentang gagal ditambahkan');
        }

    	return redirect('about');
    }

    public function create()
    {
        return view('cms.about.create');
    }

    public function edit($id)
    {
    	$about   = About::find($id);
        $path   = $this->getAbout;

        return view('cms.about.edit', compact('about','path'));
    }

    public function update(Request $request, $id)
    {
        $about 			      = About::find($id);
    	$about->title 	      = $request->title;
    	$about->description    = $request->description;

        if($request->hasFile('image')){
            $path           = $this->cdnAbout;
            $pathGet        = $this->getAbout;

            $avatar         = $_FILES['image'];

            $filename       = $this->storeFile($avatar, 1, $about, $path, $pathGet);

            if($filename['return'] == true) {
                $about->image  = $filename['message'];
            }
        }

        $update = $about->save();

        if ($update) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Tentang berhasil diubah');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Tentang gagal diubah');
        }

    	return redirect('about');
    }

    public function delete(Request $request, $id)
    {
    	$about	= About::findOrFail($id);

        $delete = $about->delete();

        if ($delete) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Tentang berhasil dihapus');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Tentang gagal dihapus');
        }

    	return redirect('about');
    }

    public function storeFile($avatar, $old = '', $user = '', $path = '', $pathGet = '')
    {
        $userfile       = explode(".", strtolower($avatar['name']));

        $filename       = $userfile[0];
        $fileext        = $userfile[1];

        $file           = md5($filename).".".$fileext;

        $target_dir     = $path;
        $target_file    = $target_dir . basename($file);
        $uploadOk       = 1;
        $imageFileType  = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

        // Check if image file is a actual image or fake image
        $check = getimagesize($_FILES["image"]["tmp_name"]);
        if($check !== false) {
            $data['return'] 	= false;
            $data['message'] 	= "File is an image - " . $check["mime"] . ".";

            $uploadOk = 1;
        } else {
            $data['return'] 	= false;
            $data['message'] 	= "File is not an image.";

            $uploadOk = 0;
        }

        // Check file size
        if ($_FILES["image"]["size"] > 500000) {
            $data['return'] 	= false;
            $data['message'] 	= "Sorry, your file is too large.";

            $uploadOk = 0;
        }
        // Allow certain file formats
        if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
        && $imageFileType != "gif" ) {
            $data['return'] 	= false;
            $data['message'] 	= "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";

            $uploadOk = 0;
        }
        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            $data['return'] 	= false;
            $data['message'] 	= "Sorry, your file was not uploaded.";

        // if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($_FILES["image"]["tmp_name"], $target_file)) {

            	$data['return']  = true;
                $data['message'] = $file;

            } else {
                $data['return'] 	= false;
                $data['message'] 	= "Sorry, there was an error uploading your file.";
            }
        }

        return $data;
    }
}
