<?php

namespace App\Http\Controllers\Cms;

use Illuminate\Http\Request;
use App\Model\Cms\Contact;
use App\Http\Controllers\Controller;

class ContactController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        $this->modelContacts    = new Contact;
        $this->limit            = 50;
    }

    /**
     * Show the application Module.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $contacts = $this->modelContacts->getDataWithPaginate($request, $this->limit);

        return view('cms.contact.index', compact('contacts'));
    }

    public function show($id)
    {
        $contact = Contact::find($id);

        return view('cms.contact.show', compact('contact'));
    }

    public function delete(Request $request, $id)
    {
    	$about	= Contact::findOrFail($id);

        $delete = $about->delete();

        if ($delete) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Kontak kami berhasil dihapus');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Kontak kami gagal dihapus');
        }

    	return redirect('contact');
    }
}
