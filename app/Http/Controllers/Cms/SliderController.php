<?php

namespace App\Http\Controllers\Cms;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Cms\Slider;

class SliderController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        $this->cdn        = config('app.cdn');
        $this->cdnSlider  = "../../cdn/sliders/";
        $this->getSlider  = $this->cdn."/sliders/";

        $this->limit       = 50;
    }

    /**
     * Show the application Module.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $slider = Slider::paginate($this->limit);
        return view('cms.slider.index', compact('slider'));
    }

    public function store(Request $request)
    {
    	$slider 			    = new Slider;
    	$slider->title 	        = $request->title;
    	$slider->description    = $request->description;
    	$slider->status         = 1;

        if($request->hasFile('image')){
            $path           = $this->cdnSlider;
            $pathGet        = $this->getSlider;

            $avatar         = $_FILES['image'];

            $filename       = $this->storeFile($avatar, 0, '', $path, $pathGet);

            if($filename['return'] == true) {
                $slider->image  = $filename['message'];
            }else{
                $request->session()->flash('status', 'err');
                $request->session()->flash('msg', $filename['message']);

                return redirect()->back();
            }
        }

        $insert = $slider->save();

        if ($insert) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Slider berhasil ditambahkan');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Slider gagal ditambahkan');
        }

    	return redirect('slider');
    }

    public function create()
    {
        return view('cms.slider.create');
    }

    public function edit($id)
    {
    	$slider   = Slider::find($id);
        $path     = $this->getSlider;

        return view('cms.slider.edit', compact('slider','path'));
    }

    public function update(Request $request, $id)
    {
        $slider 			    = Slider::find($id);
    	$slider->title 	        = $request->title;
    	$slider->description    = $request->description;

        if($request->hasFile('image')){
            $path           = $this->cdnSlider;
            $pathGet        = $this->getSlider;

            $avatar         = $_FILES['image'];

            $filename       = $this->storeFile($avatar, 1, $slider, $path, $pathGet);

            if($filename['return'] == true) {
                $slider->image  = $filename['message'];
            }else{
                $request->session()->flash('status', 'err');
                $request->session()->flash('msg', $filename['message']);

                return redirect()->back();
            }
        }

        $update = $slider->save();

        if ($update) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Slider berhasil diubah');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Slider gagal diubah');
        }

    	return redirect('slider');
    }

    public function delete(Request $request, $id)
    {
    	$slider	= Slider::findOrFail($id);

        $delete = $slider->delete();

        if ($delete) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Slider berhasil dihapus');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Slider gagal dihapus');
        }

    	return redirect('slider');
    }

    public function storeFile($avatar, $old = '', $user = '', $path = '', $pathGet = '')
    {
        $userfile       = explode(".", strtolower($avatar['name']));

        $filename       = $userfile[0];
        $fileext        = $userfile[1];

        $file           = md5($filename).".".$fileext;

        $target_dir     = $path;
        $target_file    = $target_dir . basename($file);
        $uploadOk       = 1;
        $imageFileType  = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

        // Check if image file is a actual image or fake image
        $check = getimagesize($_FILES["image"]["tmp_name"]);
        if($check !== false) {
            $data['return'] 	= false;
            $data['message'] 	= "File is an image - " . $check["mime"] . ".";

            $uploadOk = 1;
        } else {
            $data['return'] 	= false;
            $data['message'] 	= "File is not an image.";

            $uploadOk = 0;
        }

        // Check file size
        if ($_FILES["image"]["size"] >= 1500000) {
            $data['return'] 	= false;
            $data['message'] 	= "Sorry, your file is too large.";

            $uploadOk = 0;
        }
        // Allow certain file formats
        if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
        && $imageFileType != "gif" ) {
            $data['return'] 	= false;
            $data['message'] 	= "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";

            $uploadOk = 0;
        }
        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            $data['return'] 	= false;
            $data['message'] 	= "Sorry, your file was not uploaded.";

        // if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($_FILES["image"]["tmp_name"], $target_file)) {

            	$data['return']  = true;
                $data['message'] = $file;

            } else {
                $data['return'] 	= false;
                $data['message'] 	= "Sorry, there was an error uploading your file.";
            }
        }

        return $data;
    }
}
