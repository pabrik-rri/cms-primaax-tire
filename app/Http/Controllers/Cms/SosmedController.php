<?php

namespace App\Http\Controllers\Cms;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Cms\Sosmed;

class SosmedController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        $this->cdn          = config('app.cdn');
        $this->cdnSosmed    = "../../cdn/sosmed/";
        $this->getSosmed    = $this->cdn."/sosmed/";
        $this->limit        = 50;
    }

    /**
     * Show the application Module.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sosmed = Sosmed::paginate($this->limit);
        return view('cms.sosmed.index', compact('sosmed'));
    }

    public function store(Request $request)
    {
    	$sosmed 			    = new Sosmed;
    	$sosmed->name 	        = $request->name;
    	$sosmed->url            = $request->url;
    	$sosmed->status         = 1;

        if($request->hasFile('image')){
            $path           = $this->cdnSosmed;
            $pathGet        = $this->getSosmed;

            $avatar         = $_FILES['image'];

            $filename       = $this->storeFile($avatar, 0, '', $path, $pathGet);

            if($filename['return'] == true) {
                $sosmed->image  = $filename['message'];
            }
        }

        $insert = $sosmed->save();

        if ($insert) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Sosial Media berhasil ditambahkan');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Sosial Media gagal ditambahkan');
        }

    	return redirect('sosmed');
    }

    public function create()
    {
        return view('cms.sosmed.create');
    }

    public function edit($id)
    {
    	$sosmed   = Sosmed::find($id);
        $path     = $this->getSosmed;

        return view('cms.sosmed.edit', compact('sosmed','path'));
    }

    public function update(Request $request, $id)
    {
        $sosmed 			    = Sosmed::find($id);
    	$sosmed->name 	        = $request->name;
    	$sosmed->url            = $request->url;

        if($request->hasFile('image')){
            $path           = $this->cdnSosmed;
            $pathGet        = $this->getSosmed;

            $avatar         = $_FILES['image'];

            $filename       = $this->storeFile($avatar, 1, $sosmed, $path, $pathGet);

            if($filename['return'] == true) {
                $sosmed->image  = $filename['message'];
            }
        }

        $update = $sosmed->save();

        if ($update) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Sosial Media berhasil diubah');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Sosial Media gagal diubah');
        }

    	return redirect('sosmed');
    }

    public function delete(Request $request, $id)
    {
    	$sosmed	= Sosmed::findOrFail($id);

        $delete = $sosmed->delete();

        if ($delete) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Sosial Media berhasil dihapus');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Sosial Media gagal dihapus');
        }

    	return redirect('sosmed');
    }

    public function storeFile($avatar, $old = '', $user = '', $path = '', $pathGet = '')
    {
        $userfile       = explode(".", strtolower($avatar['name']));

        $filename       = $userfile[0];
        $fileext        = $userfile[1];

        $file           = md5($filename).".".$fileext;

        $target_dir     = $path;
        $target_file    = $target_dir . basename($file);
        $uploadOk       = 1;
        $imageFileType  = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

        // Check if image file is a actual image or fake image
        $check = getimagesize($_FILES["image"]["tmp_name"]);
        if($check !== false) {
            $data['return'] 	= false;
            $data['message'] 	= "File is an image - " . $check["mime"] . ".";

            $uploadOk = 1;
        } else {
            $data['return'] 	= false;
            $data['message'] 	= "File is not an image.";

            $uploadOk = 0;
        }

        // Check file size
        if ($_FILES["image"]["size"] > 500000) {
            $data['return'] 	= false;
            $data['message'] 	= "Sorry, your file is too large.";

            $uploadOk = 0;
        }
        // Allow certain file formats
        if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
        && $imageFileType != "gif" ) {
            $data['return'] 	= false;
            $data['message'] 	= "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";

            $uploadOk = 0;
        }
        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            $data['return'] 	= false;
            $data['message'] 	= "Sorry, your file was not uploaded.";

        // if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($_FILES["image"]["tmp_name"], $target_file)) {

            	$data['return']  = true;
                $data['message'] = $file;

            } else {
                $data['return'] 	= false;
                $data['message'] 	= "Sorry, there was an error uploading your file.";
            }
        }

        return $data;
    }
}
