<?php

namespace App\Http\Controllers\Cms;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Cms\News;
use Auth;

class NewsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        $this->cdn        = config('app.cdn');
        $this->cdnNews  = "../../cdn/news/";
        $this->getNews  = $this->cdn."/news/";
        $this->limit       = 50;
    }

    /**
     * Show the application Module.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news = News::paginate($this->limit);
        return view('cms.news.index', compact('news'));
    }

    public function store(Request $request)
    {
    	$news 			      = new News;
    	$news->title 	      = $request->title;
    	$news->description    = $request->description;
    	$news->status         = 1;
    	$news->user_created   = Auth::user()->id;

        if($request->hasFile('image')){
            $path           = $this->cdnNews;
            $pathGet        = $this->getNews;

            $avatar         = $_FILES['image'];

            $filename       = $this->storeFile($avatar, 0, '', $path, $pathGet);

            if($filename['return'] == true) {
                $news->image  = $filename['message'];
            }
        }

        $insert = $news->save();

        if ($insert) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Berita berhasil ditambahkan');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Berita gagal ditambahkan');
        }

    	return redirect('news');
    }

    public function create()
    {
        return view('cms.news.create');
    }

    public function edit($id)
    {
    	$news   = News::find($id);
        $path   = $this->getNews;

        return view('cms.news.edit', compact('news','path'));
    }

    public function update(Request $request, $id)
    {
        $news 			      = News::find($id);
    	$news->title 	      = $request->title;
    	$news->description    = $request->description;
    	$news->user_updated   = Auth::user()->id;

        if($request->hasFile('image')){
            $path           = $this->cdnNews;
            $pathGet        = $this->getNews;

            $avatar         = $_FILES['image'];

            $filename       = $this->storeFile($avatar, 1, $news, $path, $pathGet);

            if($filename['return'] == true) {
                $news->image  = $filename['message'];
            }
        }

        $update = $news->save();

        if ($update) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Berita berhasil diubah');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Berita gagal diubah');
        }

    	return redirect('news');
    }

    public function delete(Request $request, $id)
    {
    	$news	= News::findOrFail($id);

        $delete = $news->delete();

        if ($delete) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Berita berhasil dihapus');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Berita gagal dihapus');
        }

    	return redirect('news');
    }

    public function storeFile($avatar, $old = '', $user = '', $path = '', $pathGet = '')
    {
        $userfile       = explode(".", strtolower($avatar['name']));

        $filename       = $userfile[0];
        $fileext        = $userfile[1];

        $file           = md5($filename).".".$fileext;

        $target_dir     = $path;
        $target_file    = $target_dir . basename($file);
        $uploadOk       = 1;
        $imageFileType  = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

        // Check if image file is a actual image or fake image
        $check = getimagesize($_FILES["image"]["tmp_name"]);
        if($check !== false) {
            $data['return'] 	= false;
            $data['message'] 	= "File is an image - " . $check["mime"] . ".";

            $uploadOk = 1;
        } else {
            $data['return'] 	= false;
            $data['message'] 	= "File is not an image.";

            $uploadOk = 0;
        }

        // Check file size
        if ($_FILES["image"]["size"] > 500000) {
            $data['return'] 	= false;
            $data['message'] 	= "Sorry, your file is too large.";

            $uploadOk = 0;
        }
        // Allow certain file formats
        if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
        && $imageFileType != "gif" ) {
            $data['return'] 	= false;
            $data['message'] 	= "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";

            $uploadOk = 0;
        }
        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            $data['return'] 	= false;
            $data['message'] 	= "Sorry, your file was not uploaded.";

        // if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($_FILES["image"]["tmp_name"], $target_file)) {

            	$data['return']  = true;
                $data['message'] = $file;

            } else {
                $data['return'] 	= false;
                $data['message'] 	= "Sorry, there was an error uploading your file.";
            }
        }

        return $data;
    }
}
