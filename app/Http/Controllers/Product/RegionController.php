<?php

namespace App\Http\Controllers\Product;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Product\Region;
use App\Model\Product\Product;
use App\Model\Product\ProductCategory;
use App\Model\Master\Province;
use App\Model\Master\City;

class RegionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        $this->limit = 50;
    }

    /**
     * Show the application Module.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $region = Region::orderBy('city_id','ASC');

        if(isset($request->city_id)) {
            $region->where('city_id', $request->city_id);
        }

        $region   = $region->paginate($this->limit);

        $provinces = Province::all(['id','province']);
        $cities = City::all(['id','city']);

        return view('product.region.index', compact('region','provinces','cities'));
    }

    public function store(Request $request)
    {
    	try {
            // for ($i=0; $i < sizeof($request->product_id); $i++) {
            //     $region 			    = new Region;
            //     $region->product_id 	= $request->product_id[$i];
            //     $region->city_id 	    = $request->city_id;
            // 	$region->price 	        = $request->price;
            // }

            $region 			        = new Region;
            $region->product_id 	    = $request->product_id;
            $region->city_id 	        = $request->city_id;
            $region->price 	            = $request->price;
            $region->price_warehouse 	= $request->price_warehouse;
            $region->save();

            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Harga Produk / Region berhasil ditambahkan');

        } catch(\Exception $e) {
            $request->session()->flash('status', 'err');
            $request->session()->flash('status', $e->getMessage()." - ". $e->getLine());
        }

    	return redirect('product_region');
    }

    public function create()
    {
        $province = Province::all();
        $product_category = ProductCategory::all();

        return view('product.region.create', compact('province','product_category'));
    }

    public function edit($id)
    {
    	$region = Region::find($id);
        $province = Province::all();
        $city = City::all();
        $product = Product::all();
        $product_category = ProductCategory::all();

        return view('product.region.edit', compact('region','province','city','product','product_category'));
    }

    public function update(Request $request, $id)
    {
        $region 			        = Region::find($id);
    	$region->product_id 	    = $request->product_id;
        $region->city_id 	        = $request->city_id;
        $region->price 	            = $request->price;
        $region->price_warehouse 	= $request->price_warehouse;

        $update = $region->save();

        if ($update) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Harga Produk / Region berhasil diubah');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('status', 'Harga Produk / Region gagal diubah');
        }

    	return redirect('product_region');
    }

    public function delete(Request $request, $id)
    {
    	$delete	= Region::findOrFail($id)->delete();

        if ($delete) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Harga Produk / Region berhasil dihapus');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('status', 'Harga Produk / Region gagal dihapus');
        }

    	return redirect('product_region');
    }
}
