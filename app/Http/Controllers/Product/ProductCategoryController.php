<?php

namespace App\Http\Controllers\Product;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Product\Category;
use App\Model\Product\ProductCategory;

class ProductCategoryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        $this->cdn          = config('app.cdn');
        $this->cdnCategory  = "../../cdn/banners/";
        $this->getCategory  = $this->cdn."/banners/";

        $this->limit = 50;
    }

    /**
     * Show the application Module.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $product_category = ProductCategory::paginate($this->limit);
        return view('product.product_category.index', compact('product_category'));
    }

    public function store(Request $request)
    {
    	$product_category 			        = new ProductCategory;
    	$product_category->name 	        = $request->name;
    	$product_category->category_id 	    = $request->category_id;

        if($request->hasFile('image')){
            $path           = $this->cdnCategory;
            $pathGet        = $this->getCategory;

            $avatar         = $_FILES['image'];

            $filename       = $this->storeFile($avatar, 0, '', $path, $pathGet);

            if($filename['return'] == true) {
                $product_category->image  = $filename['message'];
            }
        }

        $insert = $product_category->save();

        if ($insert) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Produk Kategori berhasil ditambahkan');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('status', 'Produk Kategori gagal ditambahkan');
        }

    	return redirect('product_category');
    }

    public function create()
    {
        $category = Category::all(['id','name']);
        return view('product.product_category.create', compact('category'));
    }

    public function edit($id)
    {
        $category = Category::all(['id','name']);
    	$product_category = ProductCategory::find($id);
        $path     = $this->getCategory;

        return view('product.product_category.edit', compact('product_category','category','path'));
    }

    public function update(Request $request, $id)
    {
        $product_category 			        = ProductCategory::find($id);
        $product_category->name 	        = $request->name;
    	$product_category->category_id 	    = $request->category_id;

        if($request->hasFile('image')){
            $path           = $this->cdnCategory;
            $pathGet        = $this->getCategory;

            $avatar         = $_FILES['image'];

            $filename       = $this->storeFile($avatar, 1, $product_category, $path, $pathGet);

            if($filename['return'] == true) {
                $product_category->image  = $filename['message'];
            }
        }

        $update = $product_category->save();

        if ($update) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Produk Kategori berhasil diubah');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('status', 'Produk Kategori gagal diubah');
        }

    	return redirect('product_category');
    }

    public function delete(Request $request, $id)
    {
    	$product_category	= ProductCategory::findOrFail($id);

        // unlink($this->getCategory.$product_category->image);

        $delete = $product_category->delete();

        if ($delete) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Produk Kategori berhasil dihapus');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('status', 'Produk Kategori gagal dihapus');
        }

    	return redirect('product_category');
    }

    public function storeFile($avatar, $old = '', $user = '', $path = '', $pathGet = '')
    {
        if($old == 1 && $user->avatar != "") {
            $pathImage = $pathGet."/".$user->image;
            // unlink($pathImage);
        }

        $userfile       = explode(".", strtolower($avatar['name']));

        $filename       = $userfile[0];
        $fileext        = $userfile[1];

        $file           = md5($filename).".".$fileext;

        $target_dir     = $path;
        $target_file    = $target_dir . basename($file);
        $uploadOk       = 1;
        $imageFileType  = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

        // Check if image file is a actual image or fake image
        $check = getimagesize($_FILES["image"]["tmp_name"]);
        if($check !== false) {
            $data['return'] 	= false;
            $data['message'] 	= "File is an image - " . $check["mime"] . ".";

            $uploadOk = 1;
        } else {
            $data['return'] 	= false;
            $data['message'] 	= "File is not an image.";

            $uploadOk = 0;
        }

        // Check file size
        if ($_FILES["image"]["size"] > 500000) {
            $data['return'] 	= false;
            $data['message'] 	= "Sorry, your file is too large.";

            $uploadOk = 0;
        }
        // Allow certain file formats
        if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
        && $imageFileType != "gif" ) {
            $data['return'] 	= false;
            $data['message'] 	= "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";

            $uploadOk = 0;
        }
        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            $data['return'] 	= false;
            $data['message'] 	= "Sorry, your file was not uploaded.";

        // if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($_FILES["image"]["tmp_name"], $target_file)) {

            	$data['return']  = true;
                $data['message'] = $file;

            } else {
                $data['return'] 	= false;
                $data['message'] 	= "Sorry, there was an error uploading your file.";
            }
        }

        return $data;
    }
}
