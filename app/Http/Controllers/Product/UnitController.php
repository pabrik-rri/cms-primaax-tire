<?php

namespace App\Http\Controllers\Product;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Product\Unit;

class UnitController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        $this->limit = 50;
    }

    /**
     * Show the application Module.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $unit = Unit::paginate($this->limit);
        return view('product.unit.index', compact('unit'));
    }

    public function store(Request $request)
    {
    	$unit 			    = new Unit;
        $unit->code 	    = $request->code;
    	$unit->name 	    = $request->name;

        $insert = $unit->save();

        if ($insert) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Unit berhasil ditambahkan');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('status', 'Unit gagal ditambahkan');
        }

    	return redirect('unit');
    }

    public function create()
    {
        return view('product.unit.create');
    }

    public function edit($id)
    {
    	$unit = Unit::find($id);

        return view('product.unit.edit', compact('unit'));
    }

    public function update(Request $request, $id)
    {
        $unit 			    = Unit::find($id);
    	$unit->name 	    = $request->name;
        $unit->code 	    = $request->code;

        $update = $unit->save();

        if ($update) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Unit berhasil diubah');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('status', 'Unit gagal diubah');
        }

    	return redirect('unit');
    }

    public function delete(Request $request, $id)
    {
    	$delete	= Unit::findOrFail($id)->delete();

        if ($delete) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Unit berhasil dihapus');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('status', 'Unit gagal dihapus');
        }

    	return redirect('unit');
    }
}
