<?php

namespace App\Http\Controllers\Product;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Product\Category;

class CategoryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        $this->cdn          = config('app.cdn');
        $this->cdnCategory  = "../../cdn/categories/";
        $this->getCategory  = $this->cdn."/categories/";

        $this->limit = 50;
    }

    /**
     * Show the application Module.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $category = Category::paginate($this->limit);
        return view('product.category.index', compact('category'));
    }

    public function store(Request $request)
    {
    	$category 			    = new Category;
    	$category->name 	    = $request->name;

        if($request->hasFile('banner')){
            $path           = $this->cdnCategory;
            $pathGet        = $this->getCategory;

            $avatar         = $_FILES['banner'];

            $filename       = $this->storeFile($avatar, 0, '', $path, $pathGet);

            if($filename['return'] == true) {
                $category->banner  = $filename['message'];
            }
        }

        $insert = $category->save();

        if ($insert) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Kategori berhasil ditambahkan');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('status', 'Kategori gagal ditambahkan');
        }

    	return redirect('category');
    }

    public function create()
    {
        return view('product.category.create');
    }

    public function edit($id)
    {
    	$category = Category::find($id);
        $path     = $this->getCategory;

        return view('product.category.edit', compact('category','path'));
    }

    public function update(Request $request, $id)
    {
        $category 			    = Category::find($id);
    	$category->name 	    = $request->name;

        if($request->hasFile('banner')){
            $path           = $this->cdnCategory;
            $pathGet        = $this->getCategory;

            $avatar         = $_FILES['banner'];

            $filename       = $this->storeFile($avatar, 1, $category, $path, $pathGet);

            if($filename['return'] == true) {
                $category->banner  = $filename['message'];
            }
        }

        $update = $category->save();

        if ($update) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Kategori berhasil diubah');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('status', 'Kategori gagal diubah');
        }

    	return redirect('category');
    }

    public function delete(Request $request, $id)
    {
    	$category	= Category::findOrFail($id);

        // unlink($this->getCategory.$category->banner);

        $delete = $category->delete();

        if ($delete) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Kategori berhasil dihapus');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('status', 'Kategori gagal dihapus');
        }

    	return redirect('category');
    }

    public function storeFile($avatar, $old = '', $user = '', $path = '', $pathGet = '')
    {
        if($old == 1 && $user->avatar != "") {
            $pathImage = $pathGet."/".$user->banner;
            // unlink($pathImage);
        }

        $userfile       = explode(".", strtolower($avatar['name']));

        $filename       = $userfile[0];
        $fileext        = $userfile[1];

        $file           = md5($filename).".".$fileext;

        $target_dir     = $path;
        $target_file    = $target_dir . basename($file);
        $uploadOk       = 1;
        $imageFileType  = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

        // Check if image file is a actual image or fake image
        $check = getimagesize($_FILES["banner"]["tmp_name"]);
        if($check !== false) {
            $data['return'] 	= false;
            $data['message'] 	= "File is an image - " . $check["mime"] . ".";

            $uploadOk = 1;
        } else {
            $data['return'] 	= false;
            $data['message'] 	= "File is not an image.";

            $uploadOk = 0;
        }

        // Check file size
        if ($_FILES["banner"]["size"] > 500000) {
            $data['return'] 	= false;
            $data['message'] 	= "Sorry, your file is too large.";

            $uploadOk = 0;
        }
        // Allow certain file formats
        if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
        && $imageFileType != "gif" ) {
            $data['return'] 	= false;
            $data['message'] 	= "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";

            $uploadOk = 0;
        }
        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            $data['return'] 	= false;
            $data['message'] 	= "Sorry, your file was not uploaded.";

        // if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($_FILES["banner"]["tmp_name"], $target_file)) {

            	$data['return']  = true;
                $data['message'] = $file;

            } else {
                $data['return'] 	= false;
                $data['message'] 	= "Sorry, there was an error uploading your file.";
            }
        }

        return $data;
    }
}
