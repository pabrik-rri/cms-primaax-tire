<?php

namespace App\Http\Controllers\Product;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Product\Unit;
use App\Model\Product\Product;
use App\Model\Product\ProductCategory;

class ProductController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        $this->cdn            = config('app.cdn');
        $this->cdnProduct     = "../../cdn/products/";
        $this->getProduct     = $this->cdn."/products/";

        $this->limit = 50;
    }

    /**
     * Show the application Module.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $product = Product::orderBy('product_category_id','ASC')->paginate($this->limit);
        return view('product.index', compact('product'));
    }

    public function store(Request $request)
    {
    	$product 			                = new Product;
        $product->code 	                    = $request->code;
        $product->name 	                    = $request->name;
        $product->description 	            = $request->description;
        $product->price_market 	            = $request->price_market;
        $product->price_warehouse 	        = $request->price_warehouse;
        $product->qty 	                    = $request->qty;
        $product->min_qty 	                = $request->min_qty;
        $product->product_category_id 	    = $request->product_category_id;
    	$product->unit_id 	                = $request->unit_id;
    	$product->is_new 	                = $request->is_new;

        if($request->hasFile('image')){
            $path           = $this->cdnProduct;
            $pathGet        = $this->getProduct;

            $avatar         = $_FILES['image'];

            $filename       = $this->storeFile($avatar, 0, '', $path, $pathGet);

            if($filename['return'] == true) {
                $product->image  = $filename['message'];
            }
        }

        $insert = $product->save();

        if ($insert) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Produk berhasil ditambahkan');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('status', 'Produk gagal ditambahkan');
        }

    	return redirect('product');
    }

    public function create()
    {
        $unit                 = Unit::all();
        $product_category     = ProductCategory::all();

        return view('product.create', compact('unit','product_category'));
    }

    public function edit($id)
    {
    	$product              = Product::find($id);
        $unit                 = Unit::all();
        $product_category     = ProductCategory::all();

        $path                 = $this->getProduct;

        return view('product.edit', compact('product','unit','product_category','path'));
    }

    public function update(Request $request, $id)
    {
        $product 			                = Product::find($id);
        $product->code 	                    = $request->code;
        $product->name 	                    = $request->name;
        $product->description 	            = $request->description;
        $product->price_market 	            = $request->price_market;
        $product->price_warehouse 	        = $request->price_warehouse;
        $product->qty 	                    = $request->qty;
        $product->min_qty 	                = $request->min_qty;
        $product->product_category_id 	    = $request->product_category_id;
    	$product->unit_id 	                = $request->unit_id;
    	$product->is_new 	                = $request->is_new;

        if($request->hasFile('image')){
            $path           = $this->cdnProduct;
            $pathGet        = $this->getProduct;

            $avatar         = $_FILES['image'];

            $filename       = $this->storeFile($avatar, 1, $product, $path, $pathGet);

            if($filename['return'] == true) {
                $product->image  = $filename['message'];
            }
        }

        $update = $product->save();

        if ($update) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Produk berhasil diubah');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('status', 'Produk gagal diubah');
        }

    	return redirect('product');
    }

    public function delete(Request $request, $id)
    {
    	$product	= Product::findOrFail($id);

        // unlink($this->getProduct.$product->image);

        $delete = $product->delete();

        if ($delete) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Produk berhasil dihapus');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('status', 'Produk gagal dihapus');
        }

    	return redirect('product');
    }

    public function storeFile($avatar, $old = '', $user = '', $path = '', $pathGet = '')
    {
        if($old == 1 && $user->avatar != "") {
            $pathImage = $pathGet."/".$user->image;
            // unlink($pathImage);
        }

        $userfile       = explode(".", strtolower($avatar['name']));

        $filename       = $userfile[0];
        $fileext        = $userfile[1];

        $file           = md5($filename).".".$fileext;

        $target_dir     = $path;
        $target_file    = $target_dir . basename($file);
        $uploadOk       = 1;
        $imageFileType  = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

        // Check if image file is a actual image or fake image
        $check = getimagesize($_FILES["image"]["tmp_name"]);
        if($check !== false) {
            $data['return'] 	= false;
            $data['message'] 	= "File is an image - " . $check["mime"] . ".";

            $uploadOk = 1;
        } else {
            $data['return'] 	= false;
            $data['message'] 	= "File is not an image.";

            $uploadOk = 0;
        }

        // Check file size
        if ($_FILES["image"]["size"] > 500000) {
            $data['return'] 	= false;
            $data['message'] 	= "Sorry, your file is too large.";

            $uploadOk = 0;
        }
        // Allow certain file formats
        if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
        && $imageFileType != "gif" ) {
            $data['return'] 	= false;
            $data['message'] 	= "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";

            $uploadOk = 0;
        }
        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            $data['return'] 	= false;
            $data['message'] 	= "Sorry, your file was not uploaded.";

        // if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($_FILES["image"]["tmp_name"], $target_file)) {

            	$data['return']  = true;
                $data['message'] = $file;

            } else {
                $data['return'] 	= false;
                $data['message'] 	= "Sorry, there was an error uploading your file.";
            }
        }

        return $data;
    }
}
