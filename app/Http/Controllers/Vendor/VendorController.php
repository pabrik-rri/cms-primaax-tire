<?php

namespace App\Http\Controllers\Vendor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Vendor\Vendor;

class VendorController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        $this->limit = 50;
    }

    /**
     * Show the application Module.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vendor = Vendor::paginate($this->limit);
        return view('vendor.index', compact('vendor'));
    }

    public function store(Request $request)
    {
    	$vendor 			    = new Vendor;
        $vendor->name 	        = $request->name;
        $vendor->address 	    = $request->address;
        $vendor->phone 	        = $request->phone;
        $vendor->fax 	        = $request->fax;

        $insert = $vendor->save();

        if ($insert) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Vendor berhasil ditambahkan');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('status', 'Vendor gagal ditambahkan');
        }

    	return redirect('vendor');
    }

    public function create()
    {
        return view('vendor.create');
    }

    public function edit($id)
    {
    	$vendor = Vendor::find($id);

        return view('vendor.edit', compact('vendor'));
    }

    public function update(Request $request, $id)
    {
        $vendor 			    = Vendor::find($id);
        $vendor->name 	        = $request->name;
        $vendor->address 	    = $request->address;
        $vendor->phone 	        = $request->phone;
        $vendor->fax 	        = $request->fax;

        $update = $vendor->save();

        if ($update) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Vendor berhasil diubah');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('status', 'Vendor gagal diubah');
        }

    	return redirect('vendor');
    }

    public function delete(Request $request, $id)
    {
    	$delete	= Vendor::findOrFail($id)->delete();

        if ($delete) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Vendor berhasil dihapus');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('status', 'Vendor gagal dihapus');
        }

    	return redirect('vendor');
    }
}
