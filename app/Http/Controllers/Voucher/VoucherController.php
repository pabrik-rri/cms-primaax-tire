<?php

namespace App\Http\Controllers\Voucher;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Voucher\Voucher;

class VoucherController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        $this->limit = 50;
    }

    /**
     * Show the application Module.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $voucher = Voucher::paginate($this->limit);
        return view('voucher.index', compact('voucher'));
    }

    public function store(Request $request)
    {
    	$voucher 			    = new Voucher;
        $voucher->code 	        = $request->code;
        $voucher->value 	    = $request->value;
        $voucher->start 	    = date_format(date_create($request->start),'Y-m-d H:i:s');
    	$voucher->finish 	    = date_format(date_create($request->finish),'Y-m-d H:i:s');

        $insert = $voucher->save();

        if ($insert) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Voucher berhasil ditambahkan');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('status', 'Voucher gagal ditambahkan');
        }

    	return redirect('voucher');
    }

    public function create()
    {
        return view('voucher.create');
    }

    public function edit($id)
    {
    	$voucher = Voucher::find($id);

        return view('voucher.edit', compact('voucher'));
    }

    public function update(Request $request, $id)
    {
        $voucher 			    = Voucher::find($id);
        $voucher->code 	        = $request->code;
        $voucher->value 	    = $request->value;
        $voucher->start 	    = date_format(date_create($request->start),'Y-m-d H:i:s');
    	$voucher->finish 	    = date_format(date_create($request->finish),'Y-m-d H:i:s');

        $update = $voucher->save();

        if ($update) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Voucher berhasil diubah');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('status', 'Voucher gagal diubah');
        }

    	return redirect('voucher');
    }

    public function delete(Request $request, $id)
    {
    	$delete	= Voucher::findOrFail($id)->delete();

        if ($delete) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Voucher berhasil dihapus');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('status', 'Voucher gagal dihapus');
        }

    	return redirect('voucher');
    }
}
