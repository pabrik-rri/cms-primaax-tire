<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\User\StoreUser;
use App\Http\Requests\UpdateUser;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->module = "User";
        $this->limit = 25;
    }

    /**
     * Show the application level.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
    	$this->authorize('index', [ \App\User::class, $this->module ]);

        $user              = \App\User::whereNotIn('level_id', [1])->orderBy('id');

        if (\Auth::user()->level_id == 2)
            $user->where('level_id','!=',1);

        if( isset($request->key) && isset($request->value) )
        {
            $key    = $request->key;
            $value  = $request->value;

            $user->where($key,'like','%'. $value .'%');
        }

        $user    = $user->paginate($this->limit);
        $user->appends($request->all());

    	return view('user.index', compact('user'));
    }

    public function store(StoreUser $request)
    {
    	$user 			    = new \App\User;
    	$user->name 		= $request->name;
    	$user->email 		= $request->email;
    	$user->username	    = $request->username;
    	$user->level_id	    = $request->level_id;
    	$user->password	    = bcrypt($request->password);

        $insert = $user->save();

        if ($insert) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'User baru berhasil ditambahkan');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('status', 'User baru gagal ditambahkan');
        }

    	return redirect('user');

    }

    public function create()
    {
        $levels = \App\Model\Level::whereNotIn('level_id',[3,4])->get(['level_id','level']);
        
    	return view('user.create', compact('levels'));
    }

    public function edit($id)
    {
    	$user = \App\User::find($id);
        $levels = \App\Model\Level::all(['level_id','level']);

    	return view('user.edit', compact('user', 'levels'));
    }

    public function update(Request $request, $id)
    {
    	$this->authorize('edit', [ \App\User::class, $this->module ]);

    	$data = [
            'name' => $request->name,
            'email' => $request->email,
            'username' => $request->username,
            'level_id' => $request->level_id
        ];

    	if ($request->password != "" || $request->password != NULL) {
    		$data['password'] = bcrypt($request->password);
    	}

    	$update = \App\User::where('id',$id)->update($data);

        if ($update) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'User berhasil diubah');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('status', 'User gagal diubah');
        }

        if ($request->type == "profile") {
            return redirect('profile');
        }

        return redirect('user');
    }

    public function delete(Request $request, $id)
    {
    	$this->authorize('delete', [ \App\User::class, $this->module ]);

    	$delete	= \App\User::where('id',$id)->delete();

        if ($delete) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'User berhasil dihapus');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('status', 'User gagal diubah');
        }

    	return redirect('user');
    }

    public function profile()
    {
        $id                 = \Auth::user()->id;
        // $data['path']       = $this->cdn."/".\Auth::user()->photo;
        $profile    = \App\User::where('id', $id)->first();

        return view('user.profile', compact('profile'));
    }

    public function profileUpdate(Request $request)
    {
        $this->authorize('update', [ \App\User::class, $this->module ]);

        $id = \Auth::user()->id;

        $data = [
            'name' => $request->name,
            'email' => $request->email,
            'username' => $request->username,
            'phone' => $request->phone,
            'address' => $request->address,
        ];

        if ($request->password != "" || $request->password != NULL) {
            $data['password'] = bcrypt($request->password);
        }

        // return $data; exit;

        $update = \App\User::where('id',$id)->update($data);

        if ($update) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'User berhasil diubah');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('status', 'User gagal diubah');
        }

        return redirect('profile');
    }
}
