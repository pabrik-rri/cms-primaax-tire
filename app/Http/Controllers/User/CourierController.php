<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\User\Mitra;
use App\Model\Master\Province;
use App\Model\Master\City;

class CourierController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->module = "Courier";

        $this->limit = 50;

        $this->cdn          = config('app.cdn');
        $this->cdnMitra     = "../../cdn/avatars/mitra/";
        $this->cdnCourier   = "../../cdn/avatars/courier/";
        $this->getMitra     = $this->cdn."/avatars/mitra/";
        $this->getCourier   = $this->cdn."/avatars/courier/";
    }

    /**
     * Show the application level.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
    	// $this->authorize('index', [ \App\User::class, $this->module ]);

        $courier  = Mitra::where('is_courier', 1)->orderBy('id');

        if( isset($request->key) && isset($request->value) )
        {
            $key    = $request->key;
            $value  = $request->value;

            $courier->where($key,'like','%'. $value .'%');
        }

        $courier = $courier->paginate($this->limit);

    	return view('user.courier.index', compact('courier'));
    }

    public function store(Request $request)
    {
        // $this->authorize('create', [ \App\User::class, $this->module ]);

    	$mitra 			    = new Mitra;
    	$mitra->name 		= $request->name;
    	$mitra->email 		= $request->email;
    	$mitra->no_id_card	= $request->no_id_card;
    	$mitra->mobile	    = $request->mobile;
    	$mitra->name_store	= $request->name_store;
    	$mitra->password	= bcrypt($request->password);
    	$mitra->gender	    = $request->gender;
    	$mitra->birth_date	= date_format(date_create($request->birth_date),'Y-m-d');
    	$mitra->address	    = $request->address;
    	$mitra->latitude	= $request->latitude;
    	$mitra->longitude	= $request->longitude;
        $mitra->duta_code	= $request->duta_code;
        $mitra->city_id	    = $request->city_id;
        $mitra->zip_code	= $request->zip_code;
        $mitra->is_courier	= 1;
        $mitra->status	    = 1;

        if($request->hasFile('avatar')){
            $path           = $this->cdnCourier;
            $pathGet        = $this->getCourier;

            $avatar         = $_FILES['avatar'];

            $filename       = $this->storeFile($avatar, 0, '', $path, $pathGet);

            if($filename['return'] == true) {
                $mitra->avatar  = $filename['message'];
            }
        }

        $insert = $mitra->save();

        if ($insert) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Courier baru berhasil ditambahkan');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('status', 'Courier baru gagal ditambahkan');
        }

    	return redirect('courier');

    }

    public function create()
    {
        $provinces = Province::all(['id','province']);
        $mitras = Mitra::where('is_courier', 0)->get(['id','name']);

    	return view('user.courier.create', compact('provinces','mitras'));
    }

    public function edit($id)
    {
    	$user = Mitra::find($id);
        $provinces = Province::all(['id','province']);
        $cities = City::all(['id','city']);
        $mitras = Mitra::where('is_courier', 0)->get(['id','name']);

        $path = $this->getCourier;

    	return view('user.courier.edit', compact('user', 'provinces','cities','mitras','path'));
    }

    public function show($id)
    {
    	$user = Mitra::find($id);
        $path = $this->getCourier;

    	return view('user.courier.show', compact('user'));
    }

    public function update(Request $request, $id)
    {
    	// $this->authorize('edit', [ \App\User::class, $this->module ]);

    	$mitra 			    = Mitra::find($id);
    	$mitra->name 		= $request->name;
    	$mitra->email 		= $request->email;
    	$mitra->no_id_card	= $request->no_id_card;
    	$mitra->mobile	    = $request->mobile;
    	$mitra->name_store	= $request->name_store;
    	$mitra->gender	    = $request->gender;
    	$mitra->birth_date	= date_format(date_create($request->birth_date),'Y-m-d');
    	$mitra->address	    = $request->address;
    	$mitra->latitude	= $request->latitude;
    	$mitra->longitude	= $request->longitude;
        $mitra->city_id	    = $request->city_id;
        $mitra->zip_code	= $request->zip_code;
        $mitra->mitra_id	= $request->mitra_id;
        $mitra->is_courier	= 1;
        $mitra->status	    = 1;

        if($request->hasFile('avatar')){
            $path           = $this->cdnCourier;
            $pathGet        = $this->getCourier;

            $avatar         = $_FILES['avatar'];

            $filename       = $this->storeFile($avatar, 1, $mitra, $path, $pathGet);

            if($filename['return'] == true) {
                $mitra->avatar  = $filename['message'];
            }
        }

    	if ($request->password != "" || $request->password != NULL) {
    		$mitra->password = bcrypt($request->password);
    	}

    	$update = $mitra->save();

        if ($update) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Courier berhasil diubah');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('status', 'Courier gagal diubah');
        }

        return redirect('courier');
    }

    public function delete(Request $request, $id)
    {
    	// $this->authorize('delete', [ \App\User::class, $this->module ]);

    	$delete	= Mitra::where('id',$id)->delete();

        if ($delete) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Courier berhasil dihapus');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('status', 'Courier gagal diubah');
        }

    	return back();
    }

    public function storeFile($avatar, $old = '', $user = '', $path = '', $pathGet = '')
    {
        if($old == 1 && $user->avatar != "") {
            $pathImage = $pathGet."/".$user->avatar;
            unlink($pathImage);
        }

        $userfile       = explode(".", strtolower($avatar['name']));

        $filename       = $userfile[0];
        $fileext        = $userfile[1];

        $file           = md5($filename).".".$fileext;

        $target_dir     = $path;
        $target_file    = $target_dir . basename($file);
        $uploadOk       = 1;
        $imageFileType  = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

        // Check if image file is a actual image or fake image
        $check = getimagesize($_FILES["avatar"]["tmp_name"]);
        if($check !== false) {
            $data['return'] 	= false;
            $data['message'] 	= "File is an image - " . $check["mime"] . ".";

            $uploadOk = 1;
        } else {
            $data['return'] 	= false;
            $data['message'] 	= "File is not an image.";

            $uploadOk = 0;
        }

        // Check file size
        if ($_FILES["avatar"]["size"] > 500000) {
            $data['return'] 	= false;
            $data['message'] 	= "Sorry, your file is too large.";

            $uploadOk = 0;
        }
        // Allow certain file formats
        if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
        && $imageFileType != "gif" ) {
            $data['return'] 	= false;
            $data['message'] 	= "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";

            $uploadOk = 0;
        }
        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            $data['return'] 	= false;
            $data['message'] 	= "Sorry, your file was not uploaded.";

        // if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($_FILES["avatar"]["tmp_name"], $target_file)) {

            	$data['return']  = true;
                $data['message'] = $file;

            } else {
                $data['return'] 	= false;
                $data['message'] 	= "Sorry, there was an error uploading your file.";
            }
        }

        return $data;
    }
}
