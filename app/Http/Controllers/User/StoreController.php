<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\User\Store;
use App\Model\User\Mitra;
use App\Model\Master\Province;
use App\Model\Master\City;
use App\Model\Vendor\Vendor;
use App\User;

class StoreController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->module = "Store";

        $this->limit = 50;

        $this->cdn         = config('app.cdn');
        $this->cdnStore     = "../../cdn/avatars/stores/";
        $this->cdnSales   = "../../cdn/avatars/sales/";
        $this->getStore     = $this->cdn."/avatars/stores/";
        $this->getSales   = $this->cdn."/avatars/sales/";
    }

    /**
     * Show the application level.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
    	// $this->authorize('index', [ \App\User::class, $this->module ]);

        $mitra  = Store::where('is_courier', 0)->orderBy('id');

        if( isset($request->key) && isset($request->value) )
        {
            $key    = $request->key;
            $value  = $request->value;

            $mitra->where($key,'like','%'. $value .'%');
        }

        if( \Auth::user()->level_id == 3 ) {
            $mitra->where('mitra_id', \Auth::user()->mitra_id);
        }

        if( \Auth::user()->level_id == 4 ) {
            $mitra->where('id', \Auth::user()->mitra_id);
        }

        $mitra = $mitra->paginate($this->limit);

    	return view('user.store.index', compact('mitra'));
    }

    public function store(Request $request)
    {
        // $this->authorize('create', [ \App\User::class, $this->module ]);

        \DB::beginTransaction();

    	$mitra 			    = new Store;
    	$mitra->name 		= $request->name;
    	$mitra->email 		= $request->email;
    	$mitra->no_id_card	= $request->no_id_card;
    	$mitra->mobile	    = $request->mobile;
    	$mitra->name_store	= $request->name_store;
    	$mitra->password	= bcrypt($request->password);
    	$mitra->gender	    = $request->gender;
    	$mitra->birth_date	= date_format(date_create($request->birth_date),'Y-m-d');
    	$mitra->address	    = $request->address;
    	$mitra->latitude	= $request->latitude;
    	$mitra->longitude	= $request->longitude;
        $mitra->limit	= $request->limit;
        $mitra->city_id	    = $request->city_id;
        $mitra->zip_code	= $request->zip_code;
        $mitra->status	    = 1;

        if( \Auth::user()->level_id == 3 ) {
            $mitra->mitra_id = \Auth::user()->mitra_id;
        } else{
            $mitra->mitra_id = $request->mitra_id;
        }

        if(isset($request->vendor_id))
        {
            $mitra->vendor_id = implode(",", $request->vendor_id);
        }

        if($request->hasFile('avatar')){
            $path           = $this->cdnStore;
            $pathGet        = $this->getStore;

            $avatar         = $_FILES['avatar'];

            $filename       = $this->storeFile($avatar, 0, '', $path, $pathGet);

            if($filename['return'] == true) {
                $mitra->avatar  = $filename['message'];
            }
        }

        $mitra->save();

        //insert to user
        $user 			    = new \App\User;
    	$user->name 		= $request->name;
    	$user->email 		= $request->email;
    	$user->username	    = $request->mobile;
    	$user->level_id	    = 4;
        $user->password	    = bcrypt($request->password);
        $user->mitra_id     = $mitra->id;

        $insert = $user->save();

        if ($insert) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Toko baru berhasil ditambahkan');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Toko baru gagal ditambahkan');
        }

        \DB::commit();

    	return redirect('store');

    }

    public function create()
    {
        $provinces = Province::all(['id','province']);
        $vendors   = Vendor::all(['id','name']);
        $mitras    = Mitra::where('is_courier',0)->get(['id','name']);

    	return view('user.store.create', compact('provinces','mitras','vendors'));
    }

    public function edit($id)
    {
    	$user         = Store::find($id);
        $provinces    = Province::all(['id','province']);
        $cities       = City::all(['id','city']);
        $vendors      = Vendor::all(['id','name']);
        $mitras       = Mitra::where('is_courier',0)->get(['id','name']);

        $path = $this->getStore;

    	return view('user.store.edit', compact('user', 'provinces','cities','path','vendors','mitras'));
    }

    public function show($id)
    {
    	$user = Store::find($id);
        $path = $this->getStore;
        
        $mitra_id = array();
        $vendors  = array();

        if($user->vendor_id != null) {
            $mitra_id   = explode("," , $user->vendor_id);
            $vendors    = Vendor::whereIn('id', $mitra_id)->get();
        }

    	return view('user.store.show', compact('user','path', 'vendors'));
    }

    public function update(Request $request, $id)
    {
    	// $this->authorize('edit', [ \App\User::class, $this->module ]);

    	$mitra 			    = Store::find($id);
    	$mitra->name 		= $request->name;
    	$mitra->email 		= $request->email;
    	$mitra->no_id_card	= $request->no_id_card;
    	$mitra->mobile	    = $request->mobile;
    	$mitra->name_store	= $request->name_store;
    	$mitra->gender	    = $request->gender;
    	$mitra->birth_date	= date_format(date_create($request->birth_date),'Y-m-d');
    	$mitra->address	    = $request->address;
    	$mitra->latitude	= $request->latitude;
    	$mitra->longitude	= $request->longitude;
        $mitra->limit	    = $request->limit;
        $mitra->city_id	    = $request->city_id;
        $mitra->zip_code	= $request->zip_code;
        $mitra->status	    = 1;

        if(isset($request->vendor_id))
        {
            $mitra->vendor_id = implode(",", $request->vendor_id);
        }

        if($request->hasFile('avatar')){
            $path           = $this->cdnStore;
            $pathGet        = $this->getStore;

            $avatar         = $_FILES['avatar'];

            $filename       = $this->storeFile($avatar, 1, $mitra, $path, $pathGet);

            if($filename['return'] == true) {
                $mitra->avatar  = $filename['message'];
            }
        }

    	if ($request->password != "" || $request->password != NULL) {
    		$mitra->password = bcrypt($request->password);
    	}

    	$update = $mitra->update();
        
        //update to user
    	$data['name'] 		= $request->name;
    	$data['email'] 		= $request->email;
        $data['username']	= $request->mobile;

        if ($request->password != "" || $request->password != NULL) {
    		$data['password'] = bcrypt($request->password);
        }
        
        \DB::table('users')
            ->where('mitra_id', $mitra->id)
            ->where('level_id',4)
            ->update($data);

        if ($update) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Toko berhasil diubah');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Toko gagal diubah');
        }

        return redirect('store');
    }

    public function delete(Request $request, $id)
    {
    	// $this->authorize('delete', [ \App\User::class, $this->module ]);

    	$delete	= Store::where('id',$id)->delete();

        if ($delete) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Toko berhasil dihapus');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Toko gagal diubah');
        }

    	return back();
    }

    public function storeFile($avatar, $old = '', $user = '', $path = '', $pathGet = '')
    {
        if($old == 1 && $user->avatar != "") {
            $pathImage = $pathGet."/".$user->avatar;
            unlink($pathImage);
        }

        $userfile       = explode(".", strtolower($avatar['name']));

        $filename       = $userfile[0];
        $fileext        = $userfile[1];

        $file           = md5($filename).".".$fileext;

        $target_dir     = $path;
        $target_file    = $target_dir . basename($file);
        $uploadOk       = 1;
        $imageFileType  = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

        // Check if image file is a actual image or fake image
        $check = getimagesize($_FILES["avatar"]["tmp_name"]);
        if($check !== false) {
            $data['return'] 	= false;
            $data['message'] 	= "File is an image - " . $check["mime"] . ".";

            $uploadOk = 1;
        } else {
            $data['return'] 	= false;
            $data['message'] 	= "File is not an image.";

            $uploadOk = 0;
        }

        // Check file size
        if ($_FILES["avatar"]["size"] > 500000) {
            $data['return'] 	= false;
            $data['message'] 	= "Sorry, your file is too large.";

            $uploadOk = 0;
        }
        // Allow certain file formats
        if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
        && $imageFileType != "gif" ) {
            $data['return'] 	= false;
            $data['message'] 	= "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";

            $uploadOk = 0;
        }
        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            $data['return'] 	= false;
            $data['message'] 	= "Sorry, your file was not uploaded.";

        // if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($_FILES["avatar"]["tmp_name"], $target_file)) {

            	$data['return']  = true;
                $data['message'] = $file;

            } else {
                $data['return'] 	= false;
                $data['message'] 	= "Sorry, there was an error uploading your file.";
            }
        }

        return $data;
    }
}
