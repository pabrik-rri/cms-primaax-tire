<?php

use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () { return redirect('login'); });
Route::get('/home', 'HomeController@index')->name('home');
Auth::routes();

Route::get('getCity', function(Request $request) {
    if($request->ajax()) {
        $city = \App\Model\Master\City::where('province_id', $request->province)->get();

        $html = "";

        foreach ($city as $key => $value) {
            $html .= "<option value='". $value->id ."'>". $value->city ."</option>";
        }

        return $html;
    }
});

Route::get('getProduct', function(Request $request) {
    if($request->ajax()) {
        $product = \App\Model\Product\Product::where('product_category_id', $request->product_category)->get();

        $html = "";

        foreach ($product as $key => $value) {
            $html .= "<option value='". $value->id ."'>". $value->code ." - ". $value->name ."</option>";
        }

        return $html;
    }
});

Route::group(['namespace' => 'User'], function(){
    // Routing Group Menu Users
    Route::group(['prefix' => 'user'], function(){
        Route::get('/', 'UserController@index');
        Route::post('/', 'UserController@store');
        Route::get('/create', 'UserController@create');
        Route::get('/edit/{id}', 'UserController@edit');
        Route::put('/update/{id}', 'UserController@update');
        Route::delete('/delete/{id}', 'UserController@delete');
    });

    // Routing Group Menu Level
    Route::group(['prefix' => 'level'], function(){
        Route::get('/', 'LevelController@index');
        Route::post('/', 'LevelController@store');
        Route::get('/create', 'LevelController@create');
        Route::get('/edit/{id}', 'LevelController@edit');
        Route::put('/update/{id}', 'LevelController@update');
        Route::delete('/delete/{id}', 'LevelController@delete');
    });

    // Routing Group Menu Mitra
    Route::group(['prefix' => 'agent'], function(){
        Route::get('/', 'MitraController@index');
        Route::post('/', 'MitraController@store');
        Route::get('/create', 'MitraController@create');
        Route::get('/edit/{id}', 'MitraController@edit');
        Route::get('/show/{id}', 'MitraController@show');
        Route::put('/update/{id}', 'MitraController@update');
        Route::delete('/delete/{id}', 'MitraController@delete');
    });

    // Routing Group Menu Courier
    Route::group(['prefix' => 'courier'], function(){
        Route::get('/', 'CourierController@index');
        Route::post('/', 'CourierController@store');
        Route::get('/create', 'CourierController@create');
        Route::get('/edit/{id}', 'CourierController@edit');
        Route::get('/show/{id}', 'CourierController@show');
        Route::put('/update/{id}', 'CourierController@update');
        Route::delete('/delete/{id}', 'CourierController@delete');
    });

    // Routing Group Menu Store
    Route::group(['prefix' => 'store'], function(){
        Route::get('/', 'StoreController@index');
        Route::post('/', 'StoreController@store');
        Route::get('/create', 'StoreController@create');
        Route::get('/edit/{id}', 'StoreController@edit');
        Route::get('/show/{id}', 'StoreController@show');
        Route::put('/update/{id}', 'StoreController@update');
        Route::delete('/delete/{id}', 'StoreController@delete');
    });

    // Routing Group Menu Sales
    Route::group(['prefix' => 'sales'], function(){
        Route::get('/', 'SalesController@index');
        Route::post('/', 'SalesController@store');
        Route::get('/create', 'SalesController@create');
        Route::get('/edit/{id}', 'SalesController@edit');
        Route::get('/show/{id}', 'SalesController@show');
        Route::put('/update/{id}', 'SalesController@update');
        Route::delete('/delete/{id}', 'SalesController@delete');
    });

    // Routing Group Menu Customer
    Route::group(['prefix' => 'customer'], function(){
        Route::get('/', 'CustomerController@index');
        Route::post('/', 'CustomerController@store');
        Route::get('/create', 'CustomerController@create');
        Route::get('/edit/{id}', 'CustomerController@edit');
        Route::get('/show/{id}', 'CustomerController@show');
        Route::put('/update/{id}', 'CustomerController@update');
        Route::delete('/delete/{id}', 'CustomerController@delete');
    });
});

// Routing Group Menu System
Route::group(['prefix' => 'system', 'namespace' => 'System'], function(){

    // Routing Group Menu Module
    Route::group(['prefix' => 'module'], function(){
        Route::get('/', 'ModuleController@index');
        Route::post('/', 'ModuleController@store');
        Route::get('/create', 'ModuleController@create');
        Route::get('/edit/{id}', 'ModuleController@edit');
        Route::put('/update/{id}', 'ModuleController@update');
        Route::delete('/delete/{id}', 'ModuleController@delete');
    });

    // Routing Group Menu Task
    Route::group(['prefix' => 'task'], function(){
        Route::get('/', 'TaskController@index');
        Route::post('/', 'TaskController@store');
        Route::get('/create', 'TaskController@create');
        Route::get('/edit/{id}', 'TaskController@edit');
        Route::put('/update/{id}', 'TaskController@update');
        Route::delete('/delete/{id}', 'TaskController@delete');
    });

    // Routing Group Menu
    Route::group(['prefix' => 'menu'], function(){
        Route::get('/', 'MenuController@index');
        Route::post('/', 'MenuController@store');
        Route::get('/create', 'MenuController@create');
        Route::get('/edit/{id}', 'MenuController@edit');
        Route::put('/update/{id}', 'MenuController@update');
        Route::delete('/delete/{id}', 'MenuController@delete');
    });

    // Routing Group Menu Role
    Route::group(['prefix' => 'role'], function(){
        Route::get('/', 'RoleController@index');
        Route::get('/edit/{id}', 'RoleController@edit');
        Route::post('/update', 'RoleController@update');
    });
});

// Routing Group Menu Master
Route::group(['namespace' => 'Master'], function(){
    // Routing Group Menu Province
    Route::group(['prefix' => 'province'], function(){
        Route::get('/', 'ProvinceController@index');
        Route::post('/', 'ProvinceController@store');
        Route::get('/create', 'ProvinceController@create');
        Route::get('/edit/{id}', 'ProvinceController@edit');
        Route::put('/update/{id}', 'ProvinceController@update');
        Route::delete('/delete/{id}', 'ProvinceController@delete');
    });
    // Routing Group Menu City
    Route::group(['prefix' => 'city'], function(){
        Route::get('/', 'CityController@index');
        Route::post('/', 'CityController@store');
        Route::get('/create', 'CityController@create');
        Route::get('/edit/{id}', 'CityController@edit');
        Route::put('/update/{id}', 'CityController@update');
        Route::delete('/delete/{id}', 'CityController@delete');
    });
    // Routing Group Menu District
    Route::group(['prefix' => 'district'], function(){
        Route::get('/', 'DistrictController@index');
        Route::post('/', 'DistrictController@store');
        Route::get('/create', 'DistrictController@create');
        Route::get('/edit/{id}', 'DistrictController@edit');
        Route::put('/update/{id}', 'DistrictController@update');
        Route::delete('/delete/{id}', 'DistrictController@delete');
    });
    // Routing Group Menu Village
    Route::group(['prefix' => 'village'], function(){
        Route::get('/', 'VillageController@index');
        Route::post('/', 'VillageController@store');
        Route::get('/create', 'VillageController@create');
        Route::get('/edit/{id}', 'VillageController@edit');
        Route::put('/update/{id}', 'VillageController@update');
        Route::delete('/delete/{id}', 'VillageController@delete');
    });
});

// Routing Group Menu Debt
Route::group(['namespace' => 'Debt'], function(){
    // Routing Balance
    Route::group(['prefix' => 'debt/payment'], function(){
        Route::get('/', 'PaymentController@index');
        Route::post('/', 'PaymentController@store');
        Route::get('/detail/{id}', 'PaymentController@show');
        Route::get('/create', 'PaymentController@create');
        // Route::get('/edit/{id}', 'PaymentController@edit');
        // Route::put('/update/{id}', 'ReturnsController@update');
        Route::delete('/delete/{id}', 'PaymentController@delete');
        Route::get('/export', 'PaymentController@export');
        Route::get('/export-pdf/{id}', 'PaymentController@export_pdf');
    });

    // Routing Balance
    Route::group(['prefix' => 'debtreport'], function(){
        Route::get('/', 'ReportController@index');
        Route::get('/export', 'ReportController@export');
    });
});

// Routing Group Menu Receivables
Route::group(['namespace' => 'Receivables'], function(){
    // Routing Balance
    Route::group(['prefix' => 'receivables/balance'], function(){
        Route::get('/', 'BalanceController@index');
        Route::post('/', 'BalanceController@store');
        Route::get('/create', 'BalanceController@create');
        Route::delete('/delete/{id}', 'BalanceController@delete');
        Route::delete('/multiple-delete', 'BalanceController@multiple_delete');
        Route::get('/export', 'BalanceController@export');
        Route::post('/import', 'BalanceController@import');
    });

    // Routing Receipt of Receivables
    Route::group(['prefix' => 'receivables/receipt'], function(){
        Route::get('/', 'ReceiptController@index');
        // Route::post('/', 'ReceiptController@store');
        // Route::get('/create', 'ReceiptController@create');
        // Route::get('/edit/{id}', 'ReceiptController@edit');
        // Route::put('/update/{id}', 'ReceiptController@update');
        Route::get('/repayment/{id}', 'ReceiptController@repayment');
        Route::get('/export', 'ReceiptController@export');
    });

    // Routing Balance
    Route::group(['prefix' => 'receivables/report'], function(){
        Route::get('/', 'ReportController@index');
        Route::get('/export', 'ReportController@export');
    });
});

// Routing Group Menu Returns
Route::group(['namespace' => 'Returns'], function(){
    // Routing Balance
    Route::group(['prefix' => 'returns/returns'], function(){
        Route::get('/', 'ReturnsController@index');
        Route::get('/detail/{id}', 'ReturnsController@show');
        Route::post('/', 'ReturnsController@store');
        Route::get('/create', 'ReturnsController@create');
        Route::get('/edit/{id}', 'ReturnsController@edit');
        Route::put('/update/{id}', 'ReturnsController@update');
        Route::delete('/delete/{id}', 'ReturnsController@delete');
        Route::delete('/multiple-delete', 'ReturnsController@multiple_delete');
        Route::get('/export', 'ReturnsController@export');
        Route::get('/export-pdf/{id}', 'ReturnsController@export_pdf');
    });

    // Routing Receipt of Receivables
    Route::group(['prefix' => 'returns/report'], function(){
        Route::get('/', 'ReportController@index');
        Route::get('/export', 'ReportController@export');
    });
});

// Routing Group Menu Analysis
Route::group(['namespace' => 'Product'], function(){
    // Routing Group Menu Product
    Route::group(['prefix' => 'product'], function(){
        Route::get('/', 'ProductController@index');
        Route::post('/', 'ProductController@store');
        Route::get('/create', 'ProductController@create');
        Route::get('/edit/{id}', 'ProductController@edit');
        Route::get('/detail/{id}', 'ProductController@show');
        Route::put('/update/{id}', 'ProductController@update');
        Route::delete('/delete/{id}', 'ProductController@delete');
    });

    // Routing Group Menu Category
    Route::group(['prefix' => 'category'], function(){
        Route::get('/', 'CategoryController@index');
        Route::post('/', 'CategoryController@store');
        Route::get('/create', 'CategoryController@create');
        Route::get('/edit/{id}', 'CategoryController@edit');
        Route::get('/detail/{id}', 'CategoryController@show');
        Route::put('/update/{id}', 'CategoryController@update');
        Route::delete('/delete/{id}', 'CategoryController@delete');
    });

    // Routing Group Menu Product Category
    Route::group(['prefix' => 'product_category'], function(){
        Route::get('/', 'ProductCategoryController@index');
        Route::post('/', 'ProductCategoryController@store');
        Route::get('/create', 'ProductCategoryController@create');
        Route::get('/edit/{id}', 'ProductCategoryController@edit');
        Route::get('/detail/{id}', 'ProductCategoryController@show');
        Route::put('/update/{id}', 'ProductCategoryController@update');
        Route::delete('/delete/{id}', 'ProductCategoryController@delete');
    });

    // Routing Group Menu Unit
    Route::group(['prefix' => 'unit'], function(){
        Route::get('/', 'UnitController@index');
        Route::post('/', 'UnitController@store');
        Route::get('/create', 'UnitController@create');
        Route::get('/edit/{id}', 'UnitController@edit');
        Route::get('/detail/{id}', 'UnitController@show');
        Route::put('/update/{id}', 'UnitController@update');
        Route::delete('/delete/{id}', 'UnitController@delete');
    });

    // Routing Group Menu Product Region
    Route::group(['prefix' => 'product_region'], function(){
        Route::get('/', 'RegionController@index');
        Route::post('/', 'RegionController@store');
        Route::get('/create', 'RegionController@create');
        Route::get('/edit/{id}', 'RegionController@edit');
        Route::get('/detail/{id}', 'RegionController@show');
        Route::put('/update/{id}', 'RegionController@update');
        Route::delete('/delete/{id}', 'RegionController@delete');
    });
});

// Routing Group Menu Voucher
Route::group(['namespace' => 'Voucher'], function(){
    Route::group(['prefix' => 'voucher'], function(){
        Route::get('/', 'VoucherController@index');
        Route::post('/', 'VoucherController@store');
        Route::get('/create', 'VoucherController@create');
        Route::get('/edit/{id}', 'VoucherController@edit');
        Route::get('/detail/{id}', 'VoucherController@show');
        Route::put('/update/{id}', 'VoucherController@update');
        Route::delete('/delete/{id}', 'VoucherController@delete');
    });
});

// Routing Group Menu Vendor
Route::group(['namespace' => 'Vendor'], function(){
    Route::group(['prefix' => 'vendor'], function(){
        Route::get('/', 'VendorController@index');
        Route::post('/', 'VendorController@store');
        Route::get('/create', 'VendorController@create');
        Route::get('/edit/{id}', 'VendorController@edit');
        Route::get('/detail/{id}', 'VendorController@show');
        Route::put('/update/{id}', 'VendorController@update');
        Route::delete('/delete/{id}', 'VendorController@delete');
    });
});

// Routing Group Menu Payment
Route::group(['namespace' => 'Payment'], function() {
    Route::group(['prefix' => 'payment'], function(){
        Route::get('/', 'PaymentController@index');
        Route::post('/', 'PaymentController@store');
        Route::get('/create', 'PaymentController@create');
        Route::get('/edit/{id}', 'PaymentController@edit');
        Route::put('/update/{id}', 'PaymentController@update');
        Route::delete('/delete/{id}', 'PaymentController@delete');
    });
});

Route::group(['namespace' => 'Order'], function(){
    // Routing Group Menu Order
    Route::group(['prefix' => 'order'], function(){
        Route::get('/', 'OrderController@index');
        Route::post('/', 'OrderController@store');
        Route::get('/create', 'OrderController@create');
        Route::get('/edit/{id}', 'OrderController@edit');
        Route::get('/show/{id}', 'OrderController@show');
        Route::put('/update/{id}', 'OrderController@update');
        Route::delete('/cancel/{id}', 'OrderController@cancel');
        Route::delete('/delete/{id}', 'OrderController@delete');
    });

    // Routing Group Menu Order History
    Route::group(['prefix' => 'order_history'], function(){
        Route::get('/', 'HistoryController@index');
        Route::post('/', 'HistoryController@store');
        Route::get('/create', 'HistoryController@create');
        Route::get('/edit/{id}', 'HistoryController@edit');
        Route::get('/show/{id}', 'HistoryController@show');
        Route::put('/update/{id}', 'HistoryController@update');
        Route::delete('/cancel/{id}', 'HistoryController@cancel');
        Route::delete('/delete/{id}', 'HistoryController@delete');
    });

    // Routing Group Menu PO
    Route::group(['prefix' => 'pre_order'], function(){
        Route::get('/', 'POController@index');
        Route::get('/show/{id}', 'POController@show');
        Route::get('/accept/{id}', 'POController@accept');
        Route::get('/reject/{id}', 'POController@reject');
        Route::get('/send/{id}', 'POController@send');
        Route::get('/finish/{id}', 'POController@finish');
        Route::get('/finish_item/{id}', 'POController@finish_item');
        Route::get('/edit_item/{id}', 'POController@edit_item');
        Route::delete('/cancel/{id}', 'POController@cancel');
        Route::delete('/delete/{id}', 'POController@delete');
    });
});

// Routing Group Menu Vendor
Route::group(['namespace' => 'Setting'], function(){

    //Banner
    Route::group(['prefix' => 'banner'], function(){
        Route::get('/', 'BannerController@index');
        Route::post('/', 'BannerController@store');
        Route::get('/create', 'BannerController@create');
        Route::get('/edit/{id}', 'BannerController@edit');
        Route::get('/detail/{id}', 'BannerController@show');
        Route::put('/update/{id}', 'BannerController@update');
        Route::delete('/delete/{id}', 'BannerController@delete');
    });

    //Radius
    Route::group(['prefix' => 'radius'], function(){
        Route::get('/', 'RadiusController@index');
        Route::get('/edit/{id}', 'RadiusController@edit');
        Route::put('/update/{id}', 'RadiusController@update');
    });

    //Percentage Income
    Route::group(['prefix' => 'percentage'], function(){
        Route::get('/', 'PercentageController@index');
        Route::get('/edit/{id}', 'PercentageController@edit');
        Route::put('/update/{id}', 'PercentageController@update');
    });

    //Automatic Closing Order
    Route::group(['prefix' => 'closed'], function(){
        Route::get('/', 'ClosedController@index');
        Route::get('/edit/{id}', 'ClosedController@edit');
        Route::put('/update/{id}', 'ClosedController@update');
    });

    //Coverage Area
    Route::group(['prefix' => 'coverage_area'], function(){
        Route::get('/', 'CoverageAreaController@index');
    });
});

// Routing Group Menu Vendor
Route::group(['namespace' => 'Cms'], function(){

    //Slider
    Route::group(['prefix' => 'slider'], function(){
        Route::get('/', 'SliderController@index');
        Route::post('/', 'SliderController@store');
        Route::get('/create', 'SliderController@create');
        Route::get('/edit/{id}', 'SliderController@edit');
        Route::get('/detail/{id}', 'SliderController@show');
        Route::put('/update/{id}', 'SliderController@update');
        Route::delete('/delete/{id}', 'SliderController@delete');
    });

    //News
    Route::group(['prefix' => 'news'], function(){
        Route::get('/', 'NewsController@index');
        Route::post('/', 'NewsController@store');
        Route::get('/create', 'NewsController@create');
        Route::get('/edit/{id}', 'NewsController@edit');
        Route::get('/detail/{id}', 'NewsController@show');
        Route::put('/update/{id}', 'NewsController@update');
        Route::delete('/delete/{id}', 'NewsController@delete');
    });

    //About
    Route::group(['prefix' => 'about'], function(){
        Route::get('/', 'AboutController@index');
        Route::post('/', 'AboutController@store');
        Route::get('/create', 'AboutController@create');
        Route::get('/edit/{id}', 'AboutController@edit');
        Route::get('/detail/{id}', 'AboutController@show');
        Route::put('/update/{id}', 'AboutController@update');
        Route::delete('/delete/{id}', 'AboutController@delete');
    });

    //Social
    Route::group(['prefix' => 'sosmed'], function(){
        Route::get('/', 'SosmedController@index');
        Route::post('/', 'SosmedController@store');
        Route::get('/create', 'SosmedController@create');
        Route::get('/edit/{id}', 'SosmedController@edit');
        Route::get('/detail/{id}', 'SosmedController@show');
        Route::put('/update/{id}', 'SosmedController@update');
        Route::delete('/delete/{id}', 'SosmedController@delete');
    });

    //Gallery
    Route::group(['prefix' => 'gallery'], function(){
        Route::get('/', 'GalleryController@index');
        Route::post('/', 'GalleryController@store');
        Route::get('/create', 'GalleryController@create');
        Route::get('/edit/{id}', 'GalleryController@edit');
        Route::get('/detail/{id}', 'GalleryController@show');
        Route::put('/update/{id}', 'GalleryController@update');
        Route::delete('/delete/{id}', 'GalleryController@delete');
    });

    //Contact
    Route::group(['prefix' => 'contact'], function(){
        Route::get('/', 'ContactController@index');
        Route::get('/detail/{id}', 'ContactController@show');
        Route::delete('/delete/{id}', 'ContactController@delete');
    });
});

//Ticket
Route::group(['namespace' => 'Ticket'], function(){
    Route::group(['prefix' => 'ticket'], function(){
        Route::get('/', 'TicketController@index');
        Route::get('/export', 'TicketController@export');
    });
});

//Adjustment
Route::group(['namespace' => 'Adjustment'], function(){
    Route::group(['prefix' => 'adjustment'], function(){
        Route::get('/', 'AdjustmentController@index');
        Route::post('/update/{id}', 'AdjustmentController@update');
        Route::get('/getQty/{id}', 'AdjustmentController@getQty');
        Route::get('/history/{id}', 'AdjustmentController@history');
    });
    Route::group(['prefix' => 'adjustment_store'], function(){
        Route::get('/', 'StoreController@index');
        Route::post('/update/{id}', 'StoreController@update');
        Route::get('/getQty/{id}', 'StoreController@getQty');
        Route::get('/history/{id}', 'StoreController@history');
    });
});

//Income
Route::group(['namespace' => 'Income'], function(){
    // Routing Group Menu Income
    Route::group(['prefix' => 'income'], function(){
        Route::get('/', 'IncomeController@index');
    });
});
